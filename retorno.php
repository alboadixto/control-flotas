<?php

   session_start();
   include "config/config.php";

   if (!isset($_SESSION['user_id'])&& $_SESSION['user_id']==null) {
      header("location: config/index.php");
   }

   include "head.php";

?>

   <!-- MODAL -->
   <?php

      include "modal/retorno/modal_retorno.php";
   
   ?>
   <!-- END MODAL -->

   <!-- CUERPO -->
   <section class="p-t-20">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                     <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> Retorno</h3>
                        
                     <div id="retornos" class="table-responsive m-b-40">
                        <?php

                           include "dt/retorno/view/dt_retorno.php"

                        ?>
                     </div>

               </div>
            </div>
         </div>
   </section>
   <!-- END CUERPO -->

<?php 

   include "footer.php" 

?>
 
<script type="text/javascript" src="js/retorno.js"></script>
<script type="text/javascript" src="dt/retorno/dt.js"></script> 

