<?php

   session_start();
   include "config/config.php";

   if (!isset($_SESSION['user_id'])&& $_SESSION['user_id']==null) {
      header("location: config/index.php");
   }

   include "head.php";

?>

   <!-- MODAL -->
   <?php   
      
      //include "modal/flotas_index/modal_carga_datos.php";
      //include "modal/flotas_index/modal_asignacion_recorrido.php";
      //include "modal/flotas_index/modal_asignaciones.php";
   
   ?>
   <!-- END MODAL -->

   <!-- DASHBOARD-->
   <?php 

      include "dashboard.php";
   
   ?>
   <!-- END DASHBOARD-->

   <!-- CUERPO -->
   <section class="p-t-20">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> Asignaciones</h3>
               <div class="table-data__tool">

                  <div class="table-data__tool-left">
                     <button id="btn_nueva_asignacion" class="btn btn-success">
                           <i class="fas fa-truck"></i> ASIGNACIÓN
                     </button>
                     <!-- <button class="btn btn-danger" onclick="siniestro_pass();">
                           <i class="fas fa-car-crash"></i> SINIESTRO
                     </button> -->
                  </div>

                  <div class="table-data__tool-right">
                     <button id="btn_abrir_modal" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal_carga_datos" ><i class="fas fa-file-import"></i> Planilla de Carga</button>
                  </div>

               </div>
                  
               <div id="asignaciones" class="table-responsive m-b-40">
                  <?php

                     include "dt/index_flotas/view/dt_asignaciones.php"

                  ?>
               </div>

               <!-- Formulario Asignacion -->
               <div id="Asignacion" class="card border border-success" style="display:none">
                  <div class="card-header bg-success">
                     <strong class="card-title text-light"><i class="fas fa-truck"></i> Asignación
                           <small>
                              <span id="loading_gif_asignacion" class="float-right mt-1"></span>
                           </small>
                     </strong>
                  </div>

                  <div class="card-body card-block">

                     <form id="frmAsignacion" >
                           <div class="custom-tab">

                              <nav>
                                 <div class="nav nav-tabs" id="nav-tab" role="tablist">

                                       <a class="nav-item nav-link active show" id="nav-volumen-tab" data-toggle="tab" href="#nav-volumen" role="tab" aria-controls="nav-volumen" aria-selected="true"><strong>Volumen Cargar</strong></a>

                                       <a class="nav-item nav-link" id="nav-asignacion_recorrido-tab" data-toggle="tab" href="#nav-asignacion_recorrido" role="tab" aria-controls="nav-asignacion_recorrido" aria-selected="false"><strong>Asignación Recorrido</strong></a>
                                 
                                       <!-- <a class="nav-item nav-link disabled" id="asignacion-nav-vehiculo-tab" data-toggle="tab" href="#asignacion-nav-vehiculo" role="tab" aria-controls="asignacion-nav-vehiculo" aria-selected="false"><strong>Vehículo</strong></a>

                                       <a class="nav-item nav-link disabled" id="asignacion-nav-tripulacion-tab" data-toggle="tab" href="#asignacion-nav-tripulacion" role="tab" aria-controls="asignacion-nav-tripulacion" aria-selected="false"><strong>Tripulación</strong></a> -->

                                 </div>
                              </nav>

                              <div class="tab-content pl-3 pt-2" id="nav-tabContent">

                                 <div class="tab-pane fade active show" id="nav-volumen" role="tabpanel" aria-labelledby="nav-volumen-tab">
                                    <div class="table-responsive m-b-40">    
                                       <?php

                                          //include "dt/index_flotas/view/dt_volumen.php";
                                       
                                       ?>
                                    </div>
                                 </div>

                                 <div class="tab-pane fade" id="nav-asignacion_recorrido" role="tabpanel" aria-labelledby="nav-asignacion_recorrido-tab">
                                    <div class="table-responsive m-b-40">   
                                       <?php 

                                       //include "dt/index_flotas/view/dt_asignacion_recorrido.php";
                                       
                                       ?>
                                    </div>
                                 </div>

                              </div>

                           </div>
                     </form>

                  </div>

                  <div class="card-footer">

                     <button id="btn_cancelar_asignacion" type="submit" class="btn btn-secondary btn-sm"><i class="fas fa-undo"></i> VOLVER
                     </button>

                  </div>
               </div>
               <!-- Fin Formulario Asignacion -->

               <!-- Formulario Siniestro -->
               <div id="frmSiniestro" class="card border border-danger" style="display:none">
                  <div class="card-header bg-danger">
                     <strong class="card-title text-light"><i class="fas fa-car-crash"></i> Siniestro
                           <small>
                              <span id="loading_gif_siniestro" class="float-right mt-1"></span>
                           </small>
                     </strong>
                  </div>

                  <div class="card-body card-block">

                     <form>

                           <div class="row form-group">
                              <div class="col col-md-2">
                                 <label for="ppu_origen" class=" form-control-label">PPU</label>
                              </div>
                              <div class="input-group col-12 col-md-12 col-lg-4">
                                 <div class="input-group-addon">
                                    <i class="fas fa-car-crash"></i>
                                 </div>
                                 <input type="text" id="ppu_origen" name="ppu_origen" placeholder="" class="form-control" readonly>
                              </div>
                              <div class="input-group col-12 col-md-12 col-lg-6">
                                 <div class="input-group-addon">
                                    <i class="fas fa-truck"></i>
                                 </div>
                                 <select id="ppu_destino" name="ppu_destino" class="select2">
                                          
                                          <?php foreach ($ppu_nueva as $p) { ?>
                                          <option value="<?php echo $p["idvehiculos"] ?>">- <?php echo $p["patente"] ?></option>
                                          <?php } ?>
                                 </select>
                              </div>
                           </div>

                           <div class="row form-group">
                              <div class="col col-md-2">
                                 <label for="conductor_origen" class=" form-control-label">CONDUCTOR</label>
                              </div>
                              <div class="input-group col-12 col-md-12 col-lg-4">
                                 <div class="input-group-addon">
                                    <i class="fas fa-user-alt-slash"></i>
                                 </div>
                                 <input type="text" id="conductor_origen" name="conductor_origen" placeholder="" class="form-control" readonly>
                              </div>

                              <div class="input-group col-12 col-md-12 col-lg-6">
                                 <div class="input-group-addon">
                                    <i class="fas fa-user-check"></i>
                                 </div>
                                 <select id="conductor_destino" name="conductor_destino" class="select2">
                                       <?php foreach ($conductor as $c) { ?>
                                       <option value="<?php echo $c["id"] ?>">- <?php echo $c["nombre"] ?></option>
                                       <?php } ?>
                                 </select>
                              </div>
                           </div>

                     </form>

                  </div>
                  <div class="card-footer">

                     <button id="btn_cancelar_siniestro" type="submit" class="btn btn-secondary btn-sm"><i class="fas fa-undo"></i> VOLVER
                     </button>

                     <div class="pull-right">

                           <button type="submit" class="btn btn-primary btn-sm">
                              <i class="fas fa-check"></i> MODIFICAR
                           </button>
                           
                     </div>
                  </div>
               </div>
               <!-- Fin Formulario Siniestro -->

            </div>
         </div>
      </div>
   </section>
   <!-- END CUERPO -->

<?php 

   include "footer.php" 

?>

<!-- <script type="text/javascript" src="js/flotas_index.js"></script>
<script type="text/javascript" src="dt/index_flotas/dt.js"></script>
<script type="text/javascript" src="js/dashboard.js"></script>
<script type="text/javascript" src="dt/dashboard/dt.js"></script> -->
