<!-- DASHBOARD FLOTAS INDEX-->
<section class="statistic statistic2">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
         <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> Dashboard - $<span id="datepicker_total" name="datepicker_total"></span></h3>
            
            <div class="table-data__tool">

               <div class="table-data__tool-left"> 
               
                  <button id="btn_export_excel" type="button" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Excel</button>
                                
               </div>

               <div class="table-data__tool-right">

                  <input id="date" type="date" class="form-control" onchange="filtro_fecha()">
               
               </div>

            </div>

            <div class="table-responsive m-b-40">
               <?php

                  //include "dt/dashboard/view/dt_dashboard.php"

               ?>
            </div>

         </div>
      </div>
   </div>
</section>
<!-- END DASHBOARD FLOTAS INDEX-->