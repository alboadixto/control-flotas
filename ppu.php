<?php

   session_start();
   include "config/config.php";

   if (!isset($_SESSION['user_id'])&& $_SESSION['user_id']==null) {
      header("location: config/index.php");
   }

   include "head.php";

?>

   <!-- MODAL -->
   <?php 

      include "modal/ppu/modal_asignacion_ppu.php";
   
   ?>
   <!-- END MODAL -->

   <!-- CUERPO -->
   <section class="p-t-20">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                     <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> PPU</h3>
                     
                     <form id="frmFiltros" >
                        <div class="table-data__tool">
                           <div class="table-data__tool-left">
                              <div class="form-row">
                                                         
                                 <div class="form-group col-md-4">
                                    <select id="ppu_ambiente" name="ppu_ambiente" class="form-control" onchange="filtro()" >
                                    </select>
                                 </div>

                                 <div class="form-group col-md-4">
                                    <select id="ppu_kilos" name="ppu_kilos" class="form-control" onchange="filtro()">
                                    </select>
                                 </div>
                                 
                                 <div class="form-group col-md-4">
                                    <select id="ppu_metros" name="ppu_metros" class="form-control" onchange="filtro()">
                                    </select>
                                 </div>
                                             
                              </div>
                           </div>
                        </div>
                     </form>
                        
                     <div id="xxx" class="table-responsive m-b-40">
                        <?php

                           include "dt/ppu/view/dt_ppu.php"

                        ?>
                     </div>

               </div>
            </div>
         </div>
   </section>
   <!-- END CUERPO -->

<?php 

   include "footer.php" 

?>
 
<script type="text/javascript" src="js/ppu.js"></script>
<script type="text/javascript" src="js/flotas_index.js"></script>
<script type="text/javascript" src="dt/ppu/dt.js"></script> 

