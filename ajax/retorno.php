<?php

include "../variables.php";

// SELECT UNIDAD------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_unidad") {

   $id_vehiculo=$_GET['id_vehiculo'];
   $fecha=$_GET['fecha'];
   $estado_retorno=$_GET['estado_retorno'];

   $sql_select = "SELECT 
                  UL.id_unidad_limpia id, 
                  UL.descripcion_unidad_limpia nombre,           
                  ER.id_unidad_limpia asignado
                  FROM UNIDAD_LIMPIA_PPU AS UL
                  LEFT JOIN evaluacion_retorno ER
                  ON UL.id_unidad_limpia = ER.id_unidad_limpia AND ER.id_vehiculo = $id_vehiculo AND ER.fecha = $fecha AND ER.estado_retorno = $estado_retorno
                  ORDER BY ER.id_unidad_limpia DESC ,UL.id_unidad_limpia  DESC";
   //print_r($sql_select);

   $query_select = mysqli_query($con,$sql_select);
   $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

   if ($query_select){
       $messages[] = $res_query_select;
   } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
                   $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN SELECT UNIDAD------------------------------------------------------------------


// SELECT COMBUSTIBLE------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_combustible") {

    $id_vehiculo=$_GET['id_vehiculo'];
    $fecha=$_GET['fecha'];
    $estado_retorno=$_GET['estado_retorno'];

    $sql_select = "SELECT 
                    NC.id_nivel_combustible id, 
                    NC.descripcion_nivel_combustible nombre, 
                    ER.id_nivel_combustible asignado
                    FROM NIVEL_COMBUSTIBLE_PPU AS NC
                    LEFT JOIN evaluacion_retorno ER
                    ON NC.id_nivel_combustible = ER.id_nivel_combustible AND ER.id_vehiculo = $id_vehiculo AND ER.fecha = $fecha AND ER.estado_retorno = $estado_retorno
                    ORDER BY ER.id_nivel_combustible DESC ,NC.id_nivel_combustible  DESC ";
    //print_r($sql_select);

    $query_select = mysqli_query($con,$sql_select);
    $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

    if ($query_select){
        $messages[] = $res_query_select;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN SELECT COMBUSTIBLE------------------------------------------------------------------