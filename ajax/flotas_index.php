<?php

include "../variables.php";

// INSERTAR EVALUACION TRIPULACION----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="insertar_evaluacion_trabajador") {

    $id_recorrido=$_GET['btn_asignar_tabla_recorrido_id'];
    $id_conductor=$_GET['conductor_tripulacion_asignacion'];
    $token_asistente=$_GET['token_asistente'];

    $sql_insert_eva_tripulacion = "INSERT INTO evaluacion_trabajador (
                                                                fecha,
                                                                id_recorrido,
                                                                id_trabajador,
                                                                hora)
                                                                VALUES (
                                                                '$fecha', 
                                                                $id_recorrido,
                                                                $id_conductor,
                                                                CURTIME())";

    $query_insert = mysqli_query($con,$sql_insert_eva_tripulacion);
    
    if($token_asistente == 1){
        $id_asistente=$_GET['asistente_tripulacion_asignacion'];

        $sql_insert_eva_tripulacion = "INSERT INTO evaluacion_trabajador (
                                                                    fecha,
                                                                    id_recorrido,
                                                                    id_trabajador,
                                                                    hora)
                                                                    VALUES (
                                                                    '$fecha', 
                                                                    $id_recorrido,
                                                                    $id_asistente,
                                                                    CURTIME())";
    
        $query_insert = mysqli_query($con,$sql_insert_eva_tripulacion);
    }

    if ($query_insert){
        $messages[] = "Evaluación tripulacion agregado exitosamente.";
    } else $errors[]= "Evaluación tripulacion no insertado, contacte con el administrador.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN INSERTAR EVALUACION TRIPULACION------------------------------------------------

// INSERTAR EVALUACION VEHICULO----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="insertar_evaluacion_vehiculo") {

    $id_recorrido=$_GET['btn_asignar_tabla_recorrido_id'];

    $sql_insert_eva_vehiculo = "INSERT INTO evaluacion_vehiculo (
                                                        fecha,
                                                        id_recorrido,
                                                        hora)
                                                VALUES (
                                                        '$fecha', 
                                                        $id_recorrido,
                                                        CURTIME())";

    $query_insert = mysqli_query($con,$sql_insert_eva_vehiculo);

    if ($query_insert){
        $messages[] = "Evaluación vehículo agregado exitosamente.";
    } else $errors[]= "Evaluación vehículo no insertado, contacte con el administrador.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN INSERTAR EVALUACION VEHICULO------------------------------------------------

// CHECK TRABAJADOR------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="check_trabajador") {

    $id_recorrido=$_GET['id_recorrido'];
    $id_tripulacion=$_GET['id_tripulacion'];

    $sql_select = "SELECT 
                        pantalon,
                        camisa,
                        higiene,
                        casco,
                        chaleco,
                        zapatos
                        FROM evaluacion_trabajador
                        WHERE id_recorrido = $id_recorrido AND id_trabajador = $id_tripulacion";

    $query_select = mysqli_query($con,$sql_select);
    $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

    if ($query_select){
        $messages[] = $res_query_select;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN CHECK TRABAJADOR------------------------------------------------------------------

// SELECT HORA LLEGADA TRIPULACION------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_hora_llegada") {

    $id_recorrido=$_GET['id_recorrido'];
    $id_tripulacion=$_GET['id_tripulacion'];

    $sql_select = "SELECT 
                        HR.id_hora_llegada id, 
                        HR.descripcion_hora_llegada nombre, 
                        ET.id_hora_llegada asignado
                        FROM HORA_LLEGADA HR
                        LEFT JOIN evaluacion_trabajador ET
                        ON HR.id_hora_llegada = ET.id_hora_llegada AND ET.id_recorrido = $id_recorrido 
                        AND ET.id_trabajador = $id_tripulacion
                        ORDER BY ET.id_hora_llegada DESC ,HR.id_hora_llegada  ASC";

    $query_select = mysqli_query($con,$sql_select);
    $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

    if ($query_select){
        $messages[] = $res_query_select;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN SELECT HORA LLEGADA TRIPULACION------------------------------------------------------------------

// INPUT NUMBER EVA VEHICULO------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="input_number_eva_vehiculo") {

    $id_recorrido=$_GET['id_recorrido'];

    $sql_input_number = "SELECT 
                            kilometraje, 
                            asignacion_dinero, 
                            litros_cargados
                            FROM evaluacion_vehiculo
                            WHERE id_recorrido = $id_recorrido ";

    $query_input_number = mysqli_query($con,$sql_input_number);
    $res_query_input_number = $query_input_number->fetch_all(MYSQLI_ASSOC); 

    if ($query_input_number){
        $messages[] = $res_query_input_number;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN INPUT NUMBER EVA VEHICULO------------------------------------------------------------------

// SELECT COMBUSTIBLE------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_combustible") {

    $id_recorrido=$_GET['id_recorrido'];

    $sql_select = "SELECT 
                            NC.id_nivel_combustible id, 
                            NC.descripcion_nivel_combustible nombre, 
                            EV.id_nivel_combustible asignado
                            FROM NIVEL_COMBUSTIBLE_PPU AS NC
                            LEFT JOIN evaluacion_vehiculo EV
                            ON NC.id_nivel_combustible = EV.id_nivel_combustible AND EV.id_recorrido = $id_recorrido
                            ORDER BY EV.id_nivel_combustible DESC ,NC.id_nivel_combustible  DESC ";

    $query_select = mysqli_query($con,$sql_select);
    $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

    if ($query_select){
        $messages[] = $res_query_select;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN SELECT COMBUSTIBLE------------------------------------------------------------------

// SELECT ACEITE------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_aceite") {

    $id_recorrido=$_GET['id_recorrido'];

    $sql_select = "SELECT 
                            NA.id_nivel_aceite id, 
                            NA.descripcion_nivel_aceite nombre, 
                            EV.id_nivel_aceite asignado
                            FROM NIVEL_ACEITE AS NA
                            LEFT JOIN evaluacion_vehiculo EV
                            ON NA.id_nivel_aceite = EV.id_nivel_aceite AND EV.id_recorrido = $id_recorrido
                            ORDER BY EV.id_nivel_aceite DESC ,NA.id_nivel_aceite  DESC ";

    $query_select = mysqli_query($con,$sql_select);
    $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

    if ($query_select){
        $messages[] = $res_query_select;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN SELECT ACEITE------------------------------------------------------------------

// SELECT AGUA------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_agua") {

    $id_recorrido=$_GET['id_recorrido'];

    $sql_select = "SELECT 
                            NA.id_nivel_agua id, 
                            NA.descripcion_nivel_agua nombre, 
                            EV.id_nivel_agua asignado
                            FROM NIVEL_AGUA AS NA
                            LEFT JOIN evaluacion_vehiculo EV
                            ON NA.id_nivel_agua = EV.id_nivel_agua AND EV.id_recorrido = $id_recorrido
                            ORDER BY EV.id_nivel_agua DESC ,NA.id_nivel_agua  DESC ";

    $query_select = mysqli_query($con,$sql_select);
    $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

    if ($query_select){
        $messages[] = $res_query_select;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN SELECT AGUA------------------------------------------------------------------

// SELECT PRESION------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_presion") {

    $id_recorrido=$_GET['id_recorrido'];

    $sql_select = "SELECT 
                            PN.id_presion_neumatico id, 
                            PN.descripcion_presion_neumatico nombre, 
                            EV.id_presion_neumatico asignado
                            FROM PRESION_NEUMATICO_PPU AS PN
                            LEFT JOIN evaluacion_vehiculo EV
                            ON PN.id_presion_neumatico = EV.id_presion_neumatico AND EV.id_recorrido = $id_recorrido
                            ORDER BY EV.id_presion_neumatico DESC ,PN.id_presion_neumatico  DESC";

    $query_select = mysqli_query($con,$sql_select);
    $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

    if ($query_select){
        $messages[] = $res_query_select;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN SELECT PRESION------------------------------------------------------------------

// SELECT UNIDAD------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_unidad") {

    $id_recorrido=$_GET['id_recorrido'];

    $sql_select = "SELECT 
                            UL.id_unidad_limpia id, 
                            UL.descripcion_unidad_limpia nombre,           
                            EV.id_unidad_limpia asignado
                            FROM UNIDAD_LIMPIA_PPU AS UL
                            LEFT JOIN evaluacion_vehiculo EV
                            ON UL.id_unidad_limpia = EV.id_unidad_limpia AND EV.id_recorrido = $id_recorrido
                            ORDER BY EV.id_unidad_limpia DESC ,UL.id_unidad_limpia  DESC";

    $query_select = mysqli_query($con,$sql_select);
    $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

    if ($query_select){
        $messages[] = $res_query_select;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN SELECT UNIDAD------------------------------------------------------------------

// ACTUALIZAR VOLUMEN------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="actualizar_volumen") {

    $valor=$_GET['valor'];
    $idrecorrido=$_GET['idrecorrido'];
    $campo=$_GET['campo'];

    $sql_update = "UPDATE 
                    recorrido
                SET 
                    $campo = $valor
                WHERE 
                    idrecorrido = $idrecorrido";

    $query_update = mysqli_query($con,$sql_update);

    if ($query_update){
        $messages[] = "Volumen actualizado.";
    } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;

}
// FIN ACTUALIZAR VOLUMEN------------------------------------------------------------------

// ASIGNAR A RECORRIDO------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="btn_modal_asignar_recorrido") {

    $idrecorrido=$_GET['btn_asignar_tabla_recorrido_id'];
    $id_vehiculo=$_GET['ppu_asignacion'];
    $id_conductor=$_GET['conductor_tripulacion_asignacion'];
    $id_asistente_insert="";
    $id_asistente_value="";
    $token_asistente=$_GET['token_asistente'];
    if($token_asistente == 1){
        $id_asistente_insert=", id_asistente";
        $id_asistente_value=$_GET['asistente_tripulacion_asignacion'];
        $id_asistente_value=", $id_asistente_value";
    }

    $sql_asignacion_recorrido = "INSERT INTO asignacion_recorrido 
                                    (id_recorrido, 
                                    fecha,
                                    id_vehiculo,
                                    id_conductor
                                    $id_asistente_insert,
                                    hora) 
                                    VALUES 
                                    ($idrecorrido, 
                                    '$fecha',
                                    $id_vehiculo,
                                    $id_conductor
                                    $id_asistente_value,
                                    CURTIME())";
    
    //print_r($sql_asignacion_recorrido);

    $query_insertar = mysqli_query($con,$sql_asignacion_recorrido);

    if ($query_insertar){
        $messages[] = "Asignado.";
    } else $errors[]= "Lo siento algo ha salido mal en asignación recorrido, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN ASIGNAR A RECORRIDO------------------------------------------------------------------

// ACTUALIZAR CONDUCTOR------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="actualizar_conductor") {

    $sql_select_conductor = "SELECT 
                            id, 
                            nombre
                        FROM trabajadores 
                        WHERE cargo='CONDUCTOR' AND estado = 0 
                        ORDER BY nombre ASC";

    $query_conductor = mysqli_query($con,$sql_select_conductor);
    $res_query_conductor = $query_conductor->fetch_all(MYSQLI_ASSOC); 

    if ($query_conductor){
        $messages[] = $res_query_conductor;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN ACTUALIZAR CONDUCTOR------------------------------------------------------------------

// ACTUALIZAR ASISTENTE------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="actualizar_asistente") {

    $sql_select_asistente = "SELECT 
                            id, 
                            nombre
                        FROM trabajadores 
                        WHERE cargo='ASISTENTE' AND estado = 0 
                        ORDER BY nombre ASC";

    $query_asistente = mysqli_query($con,$sql_select_asistente);
    $res_query_asistente = $query_asistente->fetch_all(MYSQLI_ASSOC); 

    if ($query_asistente){
        $messages[] = $res_query_asistente;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN ACTUALIZAR ASISTENTE------------------------------------------------------------------

// ACTUALIZAR PPU------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="actualizar_ppu") {

    $sql_select_ppu = "SELECT 
                        idvehiculos, 
                        patente 
                    FROM vehiculos
                    WHERE permiso_circulacion >= NOW() AND estado = 0 
                    ORDER BY patente ASC";

    $query_ppu = mysqli_query($con,$sql_select_ppu);
    $res_query_ppu = $query_ppu->fetch_all(MYSQLI_ASSOC); 

    if ($query_ppu){
        $messages[] = $res_query_ppu;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN ACTUALIZAR PPU------------------------------------------------------------------

// BTN OK MODAL FINALIZAR ASIGNACIÓN------------------------------------------------------------------
/* if (isset($_GET['action']) && $_GET['action']=="btn_ok_modal_finalizar_asignacion") {
    
    $asistente="";
    $id_vehiculo=$_GET['ppu_asignacion'];
    $conductor=$_GET['conductor_tripulacion_asignacion'];
    if ($_GET['token_asistente'] == 1) {
        $asistente=$_GET['asistente_tripulacion_asignacion'];
        $asistente = " , $asistente";
    }


    $sql_update = "UPDATE 
                    evaluacion_vehiculo t1,
                    asignacion_recorrido t2, 
                    evaluacion_trabajador t3,
                    vehiculos t4,
                    trabajadores t5
                SET 
                    t1.finalizado = 1,
                    t2.finalizado = 1,
                    t3.finalizado = 1,
                    t4.estado = 1,
                    t5.estado = 1
                WHERE 
                    (t1.finalizado = 0 AND t1.fecha = CURDATE()) AND 
                    (t2.finalizado = 0 AND t2.id_vehiculo IS NOT NULL AND t2.fecha = CURDATE()) AND 
                    (t3.finalizado = 0 AND t3.fecha = CURDATE()) AND
                    t4.idvehiculos = $id_vehiculo AND
                    (t5.id IN ($conductor $asistente))";

    $query_update = mysqli_query($con,$sql_update);

    if ($query_update){
        $messages[] = "Asignado exitosamente.";
    } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

} */
// FIN BTN OK MODAL FINALIZAR ASIGNACIÓN------------------------------------------------------------------

// BTN RECORRIDO  ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="btn_recorrido" && isset($_GET['ppu_asignacion'])) {

    $sql_verificar_finalizado = "SELECT count(*) AS numrows FROM asignacion_recorrido WHERE finalizado=0 AND fecha=CURDATE()";
    $query_verificar = mysqli_query($con,$sql_verificar_finalizado);
    $row= mysqli_fetch_array($query_verificar);
    $numrows = $row['numrows'];

    if ($numrows>0){

        $id_vehiculo=$_GET['ppu_asignacion'];

        $sql_update_recorrido = "UPDATE asignacion_recorrido SET id_vehiculo = $id_vehiculo WHERE finalizado=0 AND fecha=CURDATE()";

        $query_update = mysqli_query($con,$sql_update_recorrido);

        if ($query_update){
            $messages[] = "Recorrido asignado exitosamente.";
        } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

    } else $errors[]= "Recorridos no asignados.";

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN BTN RECORRIDO ------------------------------------------------------------------

// ELIMINAR RECORRIDO  ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="eliminar_recorrido") {
    $idrecorrido=$_GET['idrecorrido'];

    $sql_eliminar_recorrido = "DELETE T1, T2, T3
                                FROM asignacion_recorrido T1
                                INNER JOIN evaluacion_vehiculo T2 
                                ON T1.id_recorrido = T2.id_recorrido
                                INNER JOIN evaluacion_trabajador T3
                                ON T1.id_recorrido = T3.id_recorrido
                                WHERE T1.id_recorrido=$idrecorrido AND T1.estado_retorno = 0;";

    $query_eliminar = mysqli_query($con,$sql_eliminar_recorrido);

    if ($query_eliminar){
        $messages[] = "Eliminado.";
    } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN ELIMINAR RECORRIDO ------------------------------------------------------------------

// BTN LIMPIAR  ------------------------------------------------------------------
if (isset($_GET['action']) &&  $_GET['action']=="btn_limpiar") {

    $sql_delete_temp = "DELETE FROM asignacion_recorrido WHERE finalizado = 0 ";

    $query_delete = mysqli_query($con,$sql_delete_temp);

    if ($query_delete){
        $messages[] = "Campos editables.";
    } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN BTN LIMPIAR ------------------------------------------------------------------

// SINIESTRO CONTRASEÑA------------------------------------------------------------------
if (isset($_GET['siniestro_pass'])) {

    $password=sha1(md5($_GET['siniestro_pass']));

    //Verificar si la contraseña es correcta para el administrador.
    $sql_verificar_contraseña = "SELECT count(*) AS numrows FROM usuarios WHERE (idusuarios=1 AND password='{$password}')";
    $query_password = mysqli_query($con,$sql_verificar_contraseña);
    $row= mysqli_fetch_array($query_password);
    $numrows = $row['numrows'];

    if ($numrows>0){
        $messages[] = "Contraseña correcta.";

    } else $errors []= "Contraseña incorrecta.";
    

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;
}
// FIN SINIESTRO CONTRASEÑA------------------------------------------------------------------

// PPU DESTINO (obtener estado vehiculo)------------------------------------------------------------------
if (isset($_GET['idvehiculo'])) {

    $idvehiculo=$_GET['idvehiculo'];

    $sql_estado = "SELECT V.descripcion descripcion_vehiculo,EV.descripcion descripcion_estado
                    FROM vehiculos V
                    INNER JOIN estado_vehiculo EV
                    ON V.estado_vehiculo = EV.id_estado_v
                    WHERE V.idvehiculos = $idvehiculo ";

    $query = mysqli_query($con,$sql_estado);
    $descripcion = mysqli_fetch_array($query);

    if ($query){
        $messages[] = array($descripcion['descripcion_vehiculo'],$descripcion['descripcion_estado']);
    } else $errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'descripcion_vehiculo'=>$message[0], 'descripcion_estado'=>$message[1]);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN PPU DESTINO (obtener estado vehiculo)------------------------------------------------------------------

// CONDUCTOR Y ASISTENTE ------------------------------------------------------------------
if (isset($_GET['action']) &&  $_GET['action']=="conductor_asistente") {

    $id=$_GET['id'];

    $sql_trabajadores = "SELECT rut_pasaporte FROM trabajadores WHERE id = $id ";

    $query = mysqli_query($con,$sql_trabajadores);
    $rut = mysqli_fetch_array($query);

    if ($query){
        $messages[] = $rut['rut_pasaporte'];
    } else $errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN CONDUCTOR Y ASISTENTE------------------------------------------------------------------

// EVALUACION VEHICULO MODIFICAR -----------------------------------------------------
if (isset($_GET['action']) && ($_GET['action']=="btn_modal_eva_vehiculo" || $_GET['action']=="actualizar_evaluacion_vehiculo")) {

    if($_GET['action']=="btn_modal_eva_vehiculo"){
        
        $id_vehiculo=$_GET['eva_vehiculo_id_ppu'];

        $sql = "SELECT EV.*
                    FROM asignacion_recorrido AR
                    INNER JOIN evaluacion_vehiculo EV
                    ON AR.id_recorrido = EV.id_recorrido
                    WHERE AR.id_vehiculo = $id_vehiculo AND AR.estado_retorno = 0 ";
    }else{

        $id_recorrido=$_GET['btn_asignar_tabla_recorrido_id'];
        
        $id_vehiculo=$_GET['ppu_asignacion'];

        $sql = "SELECT EV.*
                    FROM asignacion_recorrido AR
                    INNER JOIN evaluacion_vehiculo EV
                    ON AR.id_recorrido = EV.id_recorrido
                    WHERE AR.id_vehiculo = $id_vehiculo AND AR.estado_retorno = 0 
                    LIMIT 1";
    }


    $resultado = mysqli_query($con, $sql);

    if($_GET['action']=="btn_modal_eva_vehiculo"){
        $unidad_ppu_asignacion=$_GET['unidad_ppu_asignacion'];
        $presion_ppu_asignacion=$_GET['presion_ppu_asignacion'];
        $agua_ppu_asignacion=$_GET['agua_ppu_asignacion'];
        $aceite_ppu_asignacion=$_GET['aceite_ppu_asignacion'];
        $combustible_ppu_asignacion=$_GET['combustible_ppu_asignacion'];
        $kilometraje_ppu_asignacion=str_replace ( ".", "", $_GET['kilometraje_ppu_asignacion']);
        $dinero_ppu_asignacion=str_replace ( ".", "", $_GET['dinero_ppu_asignacion']);
        $litros_cargados_ppu_asignacion=str_replace ( ".", "", $_GET['litros_cargados_ppu_asignacion']);
        
        while ($row=mysqli_fetch_array($resultado)){
            
            $id_recorrido=$row["id_recorrido"];

            $sql_update_eva_vehiculo = " UPDATE evaluacion_vehiculo
                                            SET
                                            id_unidad_limpia=$unidad_ppu_asignacion, 
                                            id_presion_neumatico=$presion_ppu_asignacion, 
                                            kilometraje=$kilometraje_ppu_asignacion, 
                                            id_nivel_combustible=$combustible_ppu_asignacion,
                                            asignacion_dinero=$dinero_ppu_asignacion,   
                                            id_nivel_aceite=$aceite_ppu_asignacion, 
                                            id_nivel_agua=$agua_ppu_asignacion, 
                                            litros_cargados=$litros_cargados_ppu_asignacion
                                            WHERE id_recorrido=$id_recorrido";
            
            $query_update = mysqli_query($con, $sql_update_eva_vehiculo);
        }

    }else{

        $res_query_select = $resultado->fetch_all(MYSQLI_ASSOC);

        $unidad_ppu_asignacion = !empty($res_query_select[0]["id_unidad_limpia"]) ? $res_query_select[0]["id_unidad_limpia"] : "NULL";
        $presion_ppu_asignacion = !empty($res_query_select[0]["id_presion_neumatico"]) ? $res_query_select[0]["id_presion_neumatico"] : "NULL";
        $agua_ppu_asignacion = !empty($res_query_select[0]["id_nivel_agua"]) ? $res_query_select[0]["id_nivel_agua"] : "NULL";
        $aceite_ppu_asignacion = !empty($res_query_select[0]["id_nivel_aceite"]) ? $res_query_select[0]["id_nivel_aceite"] : "NULL";
        $combustible_ppu_asignacion = !empty($res_query_select[0]["id_nivel_combustible"]) ? $res_query_select[0]["id_nivel_combustible"] : "NULL";
        $kilometraje_ppu_asignacion = !empty($res_query_select[0]["kilometraje"]) ? $res_query_select[0]["kilometraje"] : "NULL";
        $dinero_ppu_asignacion = !empty($res_query_select[0]["asignacion_dinero"]) ? $res_query_select[0]["asignacion_dinero"] : "NULL";
        $litros_cargados_ppu_asignacion = !empty($res_query_select[0]["litros_cargados"]) ? $res_query_select[0]["litros_cargados"] : "NULL";

        $sql_update_eva_vehiculo = " UPDATE evaluacion_vehiculo
                                            SET
                                            id_unidad_limpia=$unidad_ppu_asignacion, 
                                            id_presion_neumatico=$presion_ppu_asignacion, 
                                            kilometraje=$kilometraje_ppu_asignacion, 
                                            id_nivel_combustible=$combustible_ppu_asignacion,
                                            asignacion_dinero=$dinero_ppu_asignacion,   
                                            id_nivel_aceite=$aceite_ppu_asignacion, 
                                            id_nivel_agua=$agua_ppu_asignacion, 
                                            litros_cargados=$litros_cargados_ppu_asignacion
                                            WHERE id_recorrido=$id_recorrido";
            
            $query_update = mysqli_query($con, $sql_update_eva_vehiculo);

    }

        if ($query_update){
            $messages[] = "Evaluación Vehículo agregado exitosamente.";
        } else $errors []= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN EVALUACION VEHICULO MODIFICAR -------------------------------------------------

// EVALUACION TRIPULACION MODIFICAR----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="btn_modal_eva_tripulacion") {

    $id_recorrido=$_GET['eva_tripulacion_id_recorrido'];
    $id_tripulacion=$_GET['eva_tripulacion_id_tripulacion'];
    $id_hora_llegada=$_GET['hora_tripulacion_asignacion'];
    $pantalon=0;
    $camisa=0;
    $higiene=0;
    $casco=0;
    $chaleco=0;
    $zapatos=0;

    if(isset($_GET['pantalon_tripulacion_asignacion'])){
        $pantalon=$_GET['pantalon_tripulacion_asignacion'];
    }
    if(isset($_GET['camisa_tripulacion_asignacion'])){
        $camisa=$_GET['camisa_tripulacion_asignacion'];
    }
    if(isset($_GET['higiene_tripulacion_asignacion'])){
        $higiene=$_GET['higiene_tripulacion_asignacion'];
    }
    if(isset($_GET['casco_tripulacion_asignacion'])){
        $casco=$_GET['casco_tripulacion_asignacion'];
    }
    if(isset($_GET['chaleco_tripulacion_asignacion'])){
        $chaleco=$_GET['chaleco_tripulacion_asignacion'];
    }
    if(isset($_GET['zapatos_tripulacion_asignacion'])){
        $zapatos=$_GET['zapatos_tripulacion_asignacion'];
    }

    $sql="SELECT ET.*
            FROM asignacion_recorrido AR
            INNER JOIN evaluacion_trabajador ET
            ON AR.id_recorrido = ET.id_recorrido
            WHERE (AR.id_conductor = $id_tripulacion OR AR.id_asistente = $id_tripulacion) AND AR.estado_retorno = 0";
    
    $resultado = mysqli_query($con, $sql);
    
    while ($row=mysqli_fetch_array($resultado)){
        
        $id_recorrido=$row["id_recorrido"];

        $sql_update_eva_conductor = "UPDATE evaluacion_trabajador 
                                SET id_hora_llegada=$id_hora_llegada,
                                    pantalon=$pantalon,
                                    camisa=$camisa,
                                    higiene=$higiene,
                                    casco=$casco,
                                    chaleco=$chaleco,
                                    zapatos=$zapatos
                                WHERE id_trabajador=$id_tripulacion AND id_recorrido=$id_recorrido";
        
        $query_update = mysqli_query($con, $sql_update_eva_conductor);
    }

    if ($query_update){
        $messages[] = "Evaluación Trabajador agregado exitosamente.";
    } else $errors []= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN EVALUACION TRIPULACION MODIFICAR------------------------------------------------

// EVALUACION TRIPULACION MODIFICAR AL INSERTAR----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="actualizar_evaluacion_trabajador") {

    $id_recorrido=$_GET['btn_asignar_tabla_recorrido_id'];
    $id_conductor=$_GET['conductor_tripulacion_asignacion'];
    $token_asistente=$_GET['token_asistente'];

    $sql_update_eva_conductor = "UPDATE evaluacion_trabajador T1
                                INNER JOIN (SELECT ET.*
                                        FROM asignacion_recorrido AR
                                        INNER JOIN evaluacion_trabajador ET
                                        ON AR.id_recorrido = ET.id_recorrido
                                        WHERE ET.id_trabajador = $id_conductor AND AR.estado_retorno = 0 
                                        ORDER BY 1 ASC LIMIT 1) T2
                                on T1.id_trabajador = T2.id_trabajador
                                SET T1.id_hora_llegada=T2.id_hora_llegada
                                ,T1.pantalon=T2.pantalon
                                , T1.camisa=T2.camisa
                                , T1.higiene=T2.higiene
                                , T1.casco=T2.casco
                                , T1.chaleco=T2.chaleco
                                , T1.zapatos=T2.zapatos
                                WHERE T1.id_recorrido IN (SELECT id_recorrido FROM asignacion_recorrido WHERE id_conductor = $id_conductor  AND estado_retorno = 0)";

    //print_r($sql_update_eva_conductor);
    
    $query_update_conductor = mysqli_query($con, $sql_update_eva_conductor); 

    if($token_asistente == 1){
        $id_asistente=$_GET['asistente_tripulacion_asignacion'];

        $sql_update_eva_asistente = "UPDATE evaluacion_trabajador T1
                                    INNER JOIN (SELECT ET.*
                                            FROM asignacion_recorrido AR
                                            INNER JOIN evaluacion_trabajador ET
                                            ON AR.id_recorrido = ET.id_recorrido
                                            WHERE ET.id_trabajador = $id_asistente AND AR.estado_retorno = 0 
                                            ORDER BY 1 ASC LIMIT 1) T2
                                    on T1.id_trabajador = T2.id_trabajador
                                    SET T1.id_hora_llegada=T2.id_hora_llegada
                                    ,T1.pantalon=T2.pantalon
                                    , T1.camisa=T2.camisa
                                    , T1.higiene=T2.higiene
                                    , T1.casco=T2.casco
                                    , T1.chaleco=T2.chaleco
                                    , T1.zapatos=T2.zapatos
                                    WHERE T1.id_recorrido IN (SELECT id_recorrido FROM asignacion_recorrido WHERE id_asistente = $id_asistente  AND estado_retorno = 0)";

        $query_update_asistente = mysqli_query($con, $sql_update_eva_asistente);
    }


    if ($query_update_conductor){
        $messages[] = "Evaluación Trabajador agregado exitosamente.";
    } else $errors []= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    //echo json_encode($datos);
    exit;  

}
// FIN EVALUACION TRIPULACION MODIFICAR AL INSERTAR ------------------------------------------------
