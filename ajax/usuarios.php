<?php
 
 include "../variables.php";

//Función para eliminar usuario
 if (isset($_GET['idusuarios'])) {

    $idusuarios=$_GET['idusuarios'];

    $sql_delete_usuario = " DELETE FROM usuarios WHERE idusuarios=$idusuarios ";

    $query_delete = mysqli_query($con,$sql_delete_usuario);
    if ($query_delete){
        $messages[] = "Usuario eliminado exitosamente.";
    } else $errors []= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                 $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
        foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }

    echo json_encode($datos);
    exit;
}

//Función para agregar usuario
if (isset($_GET['username']) && isset($_GET['rut']) && isset($_GET['email']) && isset($_GET['password']) && isset($_GET['empresa'])) {

    $username=$_GET['username'];
    $rut=$_GET['rut'];
    $rut_formateado = str_replace(".", "", $rut);
    $rut_formateado = str_replace("-", "", $rut_formateado);
    $email=$_GET['email'];
    $password=sha1(md5($_GET['password']));
    $empresa=$_GET['empresa'];

//Verificar si existe el RUT o CORREO, si existe no permita insertar el usuario, de lo contrario lo puede insertar.
    $sql_verificar_usuario = "SELECT count(*) AS numrows FROM usuarios WHERE rut_usuario='{$rut_formateado}' OR correo='{$email}'";
    $query_verificar = mysqli_query($con,$sql_verificar_usuario);
    $row= mysqli_fetch_array($query_verificar);
    $numrows = $row['numrows'];

    if ($numrows<1){

        $sql_insert_usuario = "INSERT INTO usuarios (nombre_completo, rut_usuario, correo, password, empresa) VALUES ('{$username}', '{$rut_formateado}', '{$email}', '{$password}', '{$empresa}')";

        $query_update = mysqli_query($con,$sql_insert_usuario);
        if ($query_update){
            $messages[] = "Usuario agregado exitosamente.";
        } else $errors []= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);
    
    }else $errors []= "R.U.T / CORREO ya existen.";

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;
}
