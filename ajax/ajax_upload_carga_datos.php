<?php
    include('../vendor/jQueryfiler/php/class.uploader.php');
    include "../config/config.php";//Contiene funcion que conecta a la base de datos

    //$fecha_actual = date('Y-m-d');

/*$token_rezagado=$_GET["token_rezagado"];
//crear carpeta para guardar los archivos
$boleta_doc = '../boletas_docentes/rezagados/'.$token_rezagado;
if (!file_exists($boleta_doc)) {
    mkdir($boleta_doc, 0777, true);
}*/

    $uploader = new Uploader();
    $data = $uploader->upload($_FILES['files'], array(
        'limit' => 1, //Maximum Limit of files. {null, Number}
        'maxSize' => 1, //Maximum Size of files {null, Number(in MB's)}
        'extensions' => array('csv'), //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
        'required' => true, //Minimum one file is required for upload {Boolean}
        //'uploadDir' => $boleta_doc.'/', //Upload directory {String}
        'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
        'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
        'replace' => false, //Replace the file if it already exists  {Boolean}
        'perms' => null, //Uploaded file permisions {null, Number}
        'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
        'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
        'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
        'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
        'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
        'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
    ));


// si la importación es correcta ///////////////////////////////////
    if($data['isComplete']){
        $files = $data['data'];
        $nombre = $files['metas'][0]['file'];
//print_r($files);
//exit;
        //Borramos la tabla con la fecha actual en caso de subir el mismo archivo
        $sql_borrar_fecha_actual = "DELETE  FROM recorrido WHERE fecha_import = curdate()";
        $query_delete = mysqli_query($con,$sql_borrar_fecha_actual);

         //si es correcto, entonces damos permisos de lectura para subir
         $handle = fopen($nombre, "r");

         while (($campos = fgetcsv($handle, 1000, ";")) !== FALSE)
         {
            $cont=0;
            $num = count($campos);
            for ($c=0; $c < $num; $c++) {
               if ($campos[$c]=='') {
                    $campos[$c]=0;
                    $cont++; 
                }
            }
            // Valido que si estan todos los campos en blancos no los inserto
            if ($cont<16){ 

              $monto = str_replace ( ".", "", $campos[6]);
              $monto = str_replace ( "$", "", $monto);
              //Insertamos los datos con los valores...
              $sql = "INSERT INTO recorrido
              ( n_recorrido,
                categoria,
                negocio,
                n_clientes,
                n_facturas,
                monto,
                comuna,
                kilos,
                volumen_30,
                volumen_60,
                volumen_100,
                n_pallet,
                ubicacion,
                anden,
                jornada,
                observaciones,
                fecha_import )
              VALUES
              ('$campos[1]',
              '$campos[2]',
              '$campos[3]',
              $campos[4],
              $campos[5],
              $monto,
              '$campos[7]',
              $campos[8],
              0,
              0,
              0,
              $campos[12],
              '$campos[13]',
              $campos[14],
              '$campos[15]',
              '$campos[16]',
              curdate())";

              //insertamos en cada vuelta el valor $obj_conexion->query($sql);
              $query_insert = mysqli_query($con,$sql);
            }
  
         }
         //cerramos la lectura del archivo "abrir archivo" con un "cerrar archivo"
         fclose($handle);

        ////////////////////////////////////////////////////////////////////////////////
        $count_query = mysqli_query($con, "SELECT count(*) AS numrows FROM recorrido WHERE fecha_import=curdate()");
        $row= mysqli_fetch_array($count_query);
        $numrows = $row['numrows']; 

        if ($numrows > 0) {
            echo json_encode($files['metas'][0]['name']);
        }else{
           $errors = $data['errors'];
            echo json_encode($errors); 
        }
        ////////////////////////////////////////////////////////////////////////////////

    }

// si la importación no es correcta ///////////////////////////////////
    if($data['hasErrors']){
        $errors = $data['errors'];
        echo json_encode($errors);
    }

    exit;
?>
