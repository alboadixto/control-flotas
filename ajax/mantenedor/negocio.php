<?php

include "../../variable.php";

// ELIMINAR PPU  ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="eliminar_ppu") {
   
   $id_vehiculo=$_GET['id_vehiculo'];
   $id_negocio=$_GET['id_negocio'];

   $sql_eliminar = "DELETE T1
                     FROM FLOTA_CONVENIDA T1
                     WHERE T1.ID_VEHICULO=$id_vehiculo AND T1.ID_NEGOCIO=$id_negocio";

   $query_eliminar = mysqli_query($con,$sql_eliminar);

   if ($query_eliminar){
       $messages[] = "Eliminado.";
   } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
                   $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN ELIMINAR PPU ------------------------------------------------------------------

// ASIGNAR PPU ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="asignar_ppu") {

   $id_vehiculo=$_GET['id_vehiculo'];
   $id_negocio=$_GET['id_negocio'];

   $sql_insert = "INSERT INTO FLOTA_CONVENIDA (
                                 ID_VEHICULO,
                                 ID_NEGOCIO
                                 )
                                 VALUES (
                                 $id_vehiculo,
                                 $id_negocio
                                 )";

   $query_insert = mysqli_query($con,$sql_insert);

   if ($query_insert){
       $messages[] = "Asignado.";
   } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
                   $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN ASIGNAR PPU ------------------------------------------------------------------

// ELIMINAR NEGOCIO  ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="eliminar_negocio") {
   
   $id_negocio=$_GET['id_negocio'];

   $sql_eliminar = "DELETE T1
                     FROM NEGOCIO T1
                     WHERE T1.ID_NEGOCIOS=$id_negocio";

   $query_eliminar = mysqli_query($con,$sql_eliminar);

   if ($query_eliminar){
       $messages[] = "Eliminado.";
   } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
                   $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN ELIMINAR NEGOCIO ------------------------------------------------------------------

// INSERTAR MANTENEDOR NEGOCIO----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="btn_mantenedor_negocio") {

   $FECHA_OPERACION=$_GET['fecha_operacion_form_negocio']; 
   $REPRES_LEG_NOMBRE=strtoupper($_GET['nom_repres_legal_form_negocio']); 
   $REPRES_LEG_RUT=$_GET['rut_repres_legal_form_negocio']; 
   $RAZON_SOCIAL=strtoupper($_GET['razon_social_form_negocio']); 
   $RUT_NEGOCIO=$_GET['rut_negocio_form_negocio']; 
   $GIRO_NEGOCIO=strtoupper($_GET['giro_negocio_form_negocio']); 
   $UBICA_OFIC_PERSONAL=strtoupper($_GET['ubicacion_personal_form_negocio']); 
   $UBICA_CENTRO_DISTRIBUCION=strtoupper($_GET['ubicacion_centro_distribucion_form_negocio']); 
   $CONTAC_NOMBRE=strtoupper($_GET['contacto_nombre_form_negocio']); 
   $CONTAC_CARGO=strtoupper($_GET['contacto_cargo_form_negocio']); 
   $CONTRATO=$_GET['posee_contrato_form_negocio'];  
   $DESCRIPCION_NEGOCIO=strtoupper($_GET['descripcion_negocio_form_negocio']); 
   $DOCU_F30=$_GET['documento_30_form_negocio']; 
   $DOCU_F31=$_GET['documento_31_form_negocio']; 
   $DOCU_CONTRATO=$_GET['documento_contrato_form_negocio']; 
   $DOCU_ENTREGA_EPP=$_GET['documento_entrega_epp_form_negocio']; 
   $DOCU_MUTUAL=$_GET['documento_mutual_form_negocio']; 
   $PLAZO_PAGO_FECHA_CIERRE=$_GET['plazo_pago_fecha_cierre_form_negocio']; 
   $PLAZO_PAGO_FECHA_FACTURACION=$_GET['plazo_pago_fecha_facturacion_form_negocio']; 
   $PLAZO_PAGO_FECHA_PAGO=$_GET['plazo_pago_fecha_pago_form_negocio']; 
   $PLAZO_PAGO_PAGA_ESTACIONAMIENTO=$_GET['paga_estacionamiento_form_negocio']; 
   $PLAZO_PAGO_HORARIO_DESPACHO=$_GET['hora_despacho_form_negocio']; 
   $PLAZO_PAGO_ENTREGA_RESPALDO=$_GET['entrega_respaldo_form_negocio']; 
   $LIDER_PROYECTO=strtoupper($_GET['lider_proyecto_form_negocio']); 

   $INSERT_CONTRATO_PLAZO="";
   $VALUES_CONTRATO_PLAZO="";
   if ($_GET['posee_contrato_form_negocio'] == 1) {
      $INSERT_CONTRATO_PLAZO=",CONTRATO_PLAZO";
      $CONTRATO_PLAZO=$_GET['plazo_contrato_form_negocio'];
      $VALUES_CONTRATO_PLAZO=", '$CONTRATO_PLAZO'";
   }

   $sql_insert = "INSERT INTO NEGOCIO (
                                       FECHA_OPERACION,
                                       REPRES_LEG_NOMBRE,
                                       REPRES_LEG_RUT,
                                       RAZON_SOCIAL,
                                       RUT_NEGOCIO,
                                       GIRO_NEGOCIO,
                                       UBICA_OFIC_PERSONAL,
                                       UBICA_CENTRO_DISTRIBUCION,
                                       CONTAC_NOMBRE,
                                       CONTAC_CARGO,
                                       CONTRATO,
                                       DESCRIPCION_NEGOCIO,
                                       DOCU_F30,
                                       DOCU_F31,
                                       DOCU_CONTRATO,
                                       DOCU_ENTREGA_EPP,
                                       DOCU_MUTUAL,
                                       PLAZO_PAGO_FECHA_CIERRE,
                                       PLAZO_PAGO_FECHA_FACTURACION,
                                       PLAZO_PAGO_FECHA_PAGO,
                                       PLAZO_PAGO_PAGA_ESTACIONAMIENTO,
                                       PLAZO_PAGO_HORARIO_DESPACHO,
                                       PLAZO_PAGO_ENTREGA_RESPALDO,
                                       LIDER_PROYECTO
                                       $INSERT_CONTRATO_PLAZO
                                       )
                                       VALUES (
                                       '$FECHA_OPERACION',
                                       '$REPRES_LEG_NOMBRE',
                                       '$REPRES_LEG_RUT',
                                       '$RAZON_SOCIAL',
                                       '$RUT_NEGOCIO',
                                       '$GIRO_NEGOCIO',
                                       '$UBICA_OFIC_PERSONAL',
                                       '$UBICA_CENTRO_DISTRIBUCION',
                                       '$CONTAC_NOMBRE',
                                       '$CONTAC_CARGO',
                                       $CONTRATO,
                                       '$DESCRIPCION_NEGOCIO',
                                       '$DOCU_F30',
                                       '$DOCU_F31',
                                       $DOCU_CONTRATO,
                                       $DOCU_ENTREGA_EPP,
                                       $DOCU_MUTUAL,
                                       '$PLAZO_PAGO_FECHA_CIERRE',
                                       '$PLAZO_PAGO_FECHA_FACTURACION',
                                       '$PLAZO_PAGO_FECHA_PAGO',
                                       $PLAZO_PAGO_PAGA_ESTACIONAMIENTO,
                                       '$PLAZO_PAGO_HORARIO_DESPACHO',
                                       $PLAZO_PAGO_ENTREGA_RESPALDO,
                                       '$LIDER_PROYECTO'
                                       $VALUES_CONTRATO_PLAZO
                                       )";

   $query_insert = mysqli_query($con,$sql_insert);

   if ($query_insert){
      $messages[] = "Negocio agregado exitosamente.";
   } else $errors[]= "Negocio no agregado, contacte con el administrador.".mysqli_error($con);

   if (isset($errors)){
      foreach ($errors as $error) {
         $datos=array('success'=>false, 'message'=>$error);
      }    
   }

   if (isset($messages)){
      foreach ($messages as $message) {
         $datos=array('success'=>true, 'message'=>$message);
      }  
   }
   
   echo json_encode($datos);
   exit;

}
// FIN INSERTAR MANTENEDOR NEGOCIO------------------------------------------------