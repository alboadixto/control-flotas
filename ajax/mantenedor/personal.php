<?php

include "../../variable.php";

// ELIMINAR PERSONAL  ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="eliminar_personal") {
   
   $id_personal=$_GET['id_personal'];

   $sql_eliminar = "DELETE T1
                     FROM PERSONAL T1
                     WHERE T1.ID_PERSONAL=$id_personal";

   $query_eliminar = mysqli_query($con,$sql_eliminar);

   if ($query_eliminar){
       $messages[] = "Eliminado.";
   } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
                   $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN ELIMINAR PERSONAL ------------------------------------------------------------------

// INSERTAR MANTENEDOR PERSONAL----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="btn_mantenedor_personal") {

    $RUT=$_GET['rut_personal_form_personal']; 
    $NOMBRE=strtoupper($_GET['nombre_personal_form_personal']); 
    $APELLIDO=strtoupper($_GET['apellidos_personal_form_personal']); 
    $DIRECCION=strtoupper($_GET['direccion_personal_form_personal']); 
    $TELEFONO=$_GET['telefono_personal_form_personal']; 
    $FECHA_NACIMIENTO=$_GET['fecha_nacimiento_form_personal']; 
    $ID_PREVISION=$_GET['prevision_form_personal']; 
    $ID_AFP=$_GET['afp_form_personal']; 
    $CURSO_MUTUAL=$_GET['curso_mutual_form_personal']; 
    $CONTACTO_EMERGENCIA_NOMBRE=strtoupper($_GET['nombre_emer_personal_form_personal']); 
    $FONO_EMERGENCIA=$_GET['numero_emer_personal_form_personal']; 
    $FECHA_INGRESO=$_GET['fecha_ingreso_form_personal']; 
    $ID_TIPO_CONTRATO=$_GET['tipo_contrato_form_personal']; 
    $PLAZO_CONTRATO=$_GET['plazo_contrato_form_personal']; 
    $ID_TIPO_LICENCIA=$_GET['tipo_licencia_form_personal']; 
    $BONOS_PAUTADOS=$_GET['bonos_pautados_form_personal']; 
    $JORNADA_LABORAL_INICIO=$_GET['hora_jornada_inicio_form_personal']; 
    $JORNADA_LABORAL_FIN=$_GET['hora_jornada_fin_form_personal']; 
    $ID_TIPO_EMPLEADO=$_GET['tipo_empleado_form_personal']; 

   $VALUES_MONTO_PAUTEADO=0;
   if ($_GET['bonos_pautados_form_personal'] == 1) {

      $MONTO_PAUTADO=str_replace ( ".", "", $_GET['monto_pautado_form_personal']);
      $VALUES_MONTO_PAUTEADO="$MONTO_PAUTADO";
   }

    $foto_perfil = '../../files/foto_perfil/'.$RUT;
    if (!file_exists($foto_perfil)) {
        mkdir($foto_perfil, 0777, true);
    }

    if ($_FILES['foto_perfil_form_personal']["error"] > 0){
        $errors[]= "Lo siento algo ha salido mal, ARCHIVO CORRUPTO.". $_FILES['foto_perfil_form_personal']['error'];
    }else{
      /* echo "Nombre: " . $_FILES['foto_perfil_form_personal']['name'] . "<br>";
      echo "Tipo: " . $_FILES['foto_perfil_form_personal']['type'] . "<br>";
      echo "Tamaño: " . ($_FILES["foto_perfil_form_personal"]["size"] / 1024) . " kB<br>";
      echo "Carpeta temporal: " . $_FILES['foto_perfil_form_personal']['tmp_name'];  */
   
      $nombre = $_FILES['foto_perfil_form_personal']['name'];
      $RUTA = 'files/foto_perfil/'.$RUT.'/'.$nombre.'';
      move_uploaded_file($_FILES['foto_perfil_form_personal']['tmp_name'], $foto_perfil."/".$nombre);

        $sql_insert = "INSERT INTO PERSONAL (
                                            RUT,
                                            NOMBRE,
                                            APELLIDO,
                                            DIRECCION,
                                            TELEFONO,
                                            FECHA_NACIMIENTO,
                                            ID_PREVISION,
                                            ID_AFP,
                                            CURSO_MUTUAL,
                                            CONTACTO_EMERGENCIA_NOMBRE,
                                            FONO_EMERGENCIA,
                                            FECHA_INGRESO,
                                            ID_TIPO_CONTRATO,
                                            PLAZO_CONTRATO,
                                            ID_TIPO_LICENCIA,
                                            BONOS_PAUTADOS,
                                            JORNADA_LABORAL_INICIO,
                                            JORNADA_LABORAL_FIN,
                                            ID_TIPO_EMPLEADO,
                                            FOTO_PERFIL,
                                            MONTO_PAUTADO
                                            )
                                            VALUES (
                                            '$RUT',
                                            '$NOMBRE',
                                            '$APELLIDO',
                                            '$DIRECCION',
                                            $TELEFONO,
                                            '$FECHA_NACIMIENTO',
                                            $ID_PREVISION,
                                            $ID_AFP,
                                            $CURSO_MUTUAL,
                                            '$CONTACTO_EMERGENCIA_NOMBRE',
                                            $FONO_EMERGENCIA,
                                            '$FECHA_INGRESO',
                                            $ID_TIPO_CONTRATO,
                                            '$PLAZO_CONTRATO',
                                            $ID_TIPO_LICENCIA,
                                            $BONOS_PAUTADOS,
                                            '$JORNADA_LABORAL_INICIO',
                                            '$JORNADA_LABORAL_FIN',
                                            $ID_TIPO_EMPLEADO,
                                            '$RUTA',
                                            $VALUES_MONTO_PAUTEADO
                                            )";

        $query_insert = mysqli_query($con,$sql_insert);

        if ($query_insert){
            $messages[] = "Personal agregado exitosamente.";
        } else $errors[]= "Personal no agregado, contacte con el administrador.".mysqli_error($con);

    }

   if (isset($errors)){
      foreach ($errors as $error) {
         $datos=array('success'=>false, 'message'=>$error);
      }    
   }

   if (isset($messages)){
      foreach ($messages as $message) {
         $datos=array('success'=>true, 'message'=>$message);
      }  
   }
   
   echo json_encode($datos);
   exit;

}
// FIN INSERTAR MANTENEDOR PERSONAL------------------------------------------------

// SELECT TIPO EMPLEADO----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_tipo_empleado") {

   $sql_select = "SELECT 
                  ID_TIPO_EMPLEADO id, 
                  DESCRIPCION nombre
                  FROM TIPO_EMPLEADO";

   $query_select = mysqli_query($con,$sql_select);
   $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

   if ($query_select){
       $messages[] = $res_query_select;
   } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
               $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN TIPO EMPLEADO-------------------------------------------------------

// SELECT TIPO CONTRATO----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_tipo_contrato") {

   $sql_select = "SELECT 
                  ID_TIPO_CONTRATO id, 
                  DESCRIPCION nombre
                  FROM TIPO_CONTRATO";

   $query_select = mysqli_query($con,$sql_select);
   $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

   if ($query_select){
       $messages[] = $res_query_select;
   } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
               $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN TIPO CONTRATO-------------------------------------------------------

// SELECT TIPO LICENCIA----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_tipo_licencia") {

   $sql_select = "SELECT 
                  ID_TIPO_LICENCIA id, 
                  DESCRIPCION nombre
                  FROM TIPO_LICENCIA";

   $query_select = mysqli_query($con,$sql_select);
   $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

   if ($query_select){
       $messages[] = $res_query_select;
   } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
               $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN TIPO LICENCIA-------------------------------------------------------

// SELECT AFP----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_afp") {

    $sql_select = "SELECT 
                   ID_AFP id, 
                   NOMBRE_AFP nombre
                   FROM AFP";
 
    $query_select = mysqli_query($con,$sql_select);
    $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 
 
    if ($query_select){
        $messages[] = $res_query_select;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);
 
    if (isset($errors)){
        foreach ($errors as $error) {
                $datos=array('success'=>false, 'message'=>$error);
            }    
        }
 
    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  
 
}
// FIN AFP-------------------------------------------------------

// SELECT PREVISION----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_prevision") {

    $sql_select = "SELECT 
                   ID_PREVISION id, 
                   NOMBRE_PREVISION nombre
                   FROM PREVISION";
 
    $query_select = mysqli_query($con,$sql_select);
    $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 
 
    if ($query_select){
        $messages[] = $res_query_select;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);
 
    if (isset($errors)){
        foreach ($errors as $error) {
                $datos=array('success'=>false, 'message'=>$error);
            }    
        }
 
    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  
 
}
// FIN PREVISION-------------------------------------------------------