<?php

include "../../variable.php";

// ELIMINAR VEHICULO  ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="eliminar_vehiculo") {
   
   $id_vehiculo=$_GET['id_vehiculo'];

   $sql_eliminar = "DELETE T1
                     FROM VEHICULO T1
                     WHERE T1.ID_VEHICULO=$id_vehiculo";

   $query_eliminar = mysqli_query($con,$sql_eliminar);

   if ($query_eliminar){
       $messages[] = "Eliminado.";
   } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
                   $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN ELIMINAR VEHICULO ------------------------------------------------------------------

// INSERTAR MANTENEDOR VEHÍCULO----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="btn_mantenedor_vehiculo") {

   $FECHA_ADQUISICION=$_GET['fecha_adquisicion_form_vehiculo']; 
   $VENDEDOR=strtoupper($_GET['vendedor_form_vehiculo']);
   $VALOR_ADQUISICION=str_replace ( ".", "", $_GET['valor_adquisicion_form_vehiculo']);
   $FORMA_ADQUISICION=$_GET['forma_adquisicion_form_vehiculo'];
   $GPS=$_GET['gps_form_vehiculo'];
   $MARCA=$_GET['marca_form_vehiculo'];
   $MODELO=$_GET['modelo_form_vehiculo'];
   $ANO=str_replace ( ".", "", $_GET['ano_form_vehiculo']);
   $CAPACIDAD_KG=str_replace ( ".", "", $_GET['capacidad_form_vehiculo']);
   $VOLUMEN=str_replace ( ".", "", $_GET['volumen_form_vehiculo']);
   $AMBIENTE=$_GET['ambiente_form_vehiculo'];
   $COMBUSTIBLE=str_replace ( ".", "", $_GET['combustible_form_vehiculo']);
   $KILOMETRAJE=str_replace ( ".", "", $_GET['kilometraje_form_vehiculo']);
   $POLIZA_SEGURO=$_GET['poliza_seguro_form_vehiculo'];
   $POLIZA_SEGURO_CARGA=$_GET['poliza_seguro_carga_form_vehiculo'];
   $PER_CIRCULACION_FECHA_CONTRATACION=$_GET['p_circulacion_f_contrato_form_vehiculo'];
   $PER_CIRCULACION_FECHA_VENCIMIENTO=$_GET['p_circulacion_f_vencimiento_form_vehiculo'];
   $PER_CIRCULACION_MONTO=str_replace ( ".", "", $_GET['p_circulacion_monto_form_vehiculo']);
   $PER_CIRCULACION_MUNICIPALIDAD=strtoupper($_GET['p_circulacion_municipalidad_form_vehiculo']);
   $SOAP_FECHA_CONTRATACION=$_GET['soap_fecha_contrato_form_vehiculo'];
   $SOAP_FECHA_VENCIMIENTO=$_GET['soap_fecha_vencimiento_form_vehiculo'];
   $SOAP_MONTO=str_replace ( ".", "", $_GET['soap_monto_form_vehiculo']);
   $REV_TEC_FECHA=$_GET['rev_tec_fecha_form_vehiculo'];
   $REV_TEC_PLANTA=strtoupper($_GET['rev_tec_planta_form_vehiculo']);
   $GASES_FECHA=$_GET['gases_fecha_form_vehiculo'];
   $GASES_PLANTA=strtoupper($_GET['gases_planta_form_vehiculo']);
   $PATENTE=strtoupper($_GET['patente_form_vehiculo']);
   $JUEGO_LLAVES=$_GET['juego_llaves_form_vehiculo'];
   $GATA=$_GET['gata_form_vehiculo'];
   $NEUMATICO_REPUESTO=$_GET['neumatico_repuesto_form_vehiculo'];
   $LLAVE_CRUZ=$_GET['llave_cruz_form_vehiculo'];
   $EXTINTOR=$_GET['extintor_form_vehiculo'];
   $TRIANGULO_SEGURIDAD=$_GET['triangulo_seguridad_form_vehiculo'];
   $PANEL_RADIO=$_GET['panel_radio_form_vehiculo'];
   $CUNAS=$_GET['cunas_form_vehiculo'];
   $CORTA_CORRIENTE=$_GET['corta_corriente_form_vehiculo'];

   $INSERT_FORMA_ADQUISICION="";
   $VALUES_FORMA_ADQUISICION="";
   if ($_GET['forma_adquisicion_form_vehiculo'] == 2) {
      $INSERT_FORMA_ADQUISICION=",
                                 BANCO_LEASING,
                                 CUOTA_LEASING,
                                 FECHA_LEASING";
      $BANCO_LEASING=strtoupper($_GET['banco_leasing_form_vehiculo']);
      $CUOTA_LEASING=str_replace ( ".", "", $_GET['cuota_leasing_form_vehiculo']);
      $FECHA_LEASING=$_GET['fecha_leasing_form_vehiculo'];
      $VALUES_FORMA_ADQUISICION=",
                                 '$BANCO_LEASING',
                                 $CUOTA_LEASING,
                                 '$FECHA_LEASING'";
   }

   $sql_insert = "INSERT INTO VEHICULO (
                                       MARCA,
                                       MODELO,
                                       ANO,
                                       AMBIENTE,
                                       VOLUMEN,
                                       CAPACIDAD_KG,
                                       COMBUSTIBLE,
                                       KILOMETRAJE,
                                       GPS,
                                       CORTA_CORRIENTE,
                                       FECHA_ADQUISICION,
                                       VENDEDOR,
                                       VALOR_ADQUISICION,
                                       FORMA_ADQUISICION,
                                       POLIZA_SEGURO,
                                       POLIZA_SEGURO_CARGA,
                                       PER_CIRCULACION_FECHA_CONTRATACION,
                                       PER_CIRCULACION_FECHA_VENCIMIENTO,
                                       PER_CIRCULACION_MONTO,
                                       PER_CIRCULACION_MUNICIPALIDAD,
                                       SOAP_FECHA_CONTRATACION,
                                       SOAP_FECHA_VENCIMIENTO,
                                       SOAP_MONTO,
                                       REV_TEC_FECHA,
                                       REV_TEC_PLANTA,
                                       GASES_FECHA,
                                       GASES_PLANTA,
                                       PATENTE,
                                       JUEGO_LLAVES,
                                       GATA,
                                       NEUMATICO_REPUESTO,
                                       LLAVE_CRUZ,
                                       EXTINTOR,
                                       TRIANGULO_SEGURIDAD,
                                       PANEL_RADIO,
                                       CUNAS
                                       $INSERT_FORMA_ADQUISICION
                                       )
                                       VALUES (
                                       $MARCA,
                                       '$MODELO',
                                       $ANO,
                                       $AMBIENTE,
                                       $VOLUMEN,
                                       $CAPACIDAD_KG,
                                       $COMBUSTIBLE,
                                       $KILOMETRAJE,
                                       $GPS,
                                       $CORTA_CORRIENTE,
                                       '$FECHA_ADQUISICION',
                                       '$VENDEDOR',
                                       $VALOR_ADQUISICION,
                                       $FORMA_ADQUISICION,
                                       $POLIZA_SEGURO,
                                       $POLIZA_SEGURO_CARGA,
                                       '$PER_CIRCULACION_FECHA_CONTRATACION',
                                       '$PER_CIRCULACION_FECHA_VENCIMIENTO',
                                       $PER_CIRCULACION_MONTO,
                                       '$PER_CIRCULACION_MUNICIPALIDAD',
                                       '$SOAP_FECHA_CONTRATACION',
                                       '$SOAP_FECHA_VENCIMIENTO',
                                       $SOAP_MONTO,
                                       '$REV_TEC_FECHA',
                                       '$REV_TEC_PLANTA',
                                       '$GASES_FECHA',
                                       '$GASES_PLANTA',
                                       '$PATENTE',
                                       $JUEGO_LLAVES,
                                       $GATA,
                                       $NEUMATICO_REPUESTO,
                                       $LLAVE_CRUZ,
                                       $EXTINTOR,
                                       $TRIANGULO_SEGURIDAD,
                                       $PANEL_RADIO,
                                       $CUNAS
                                       $VALUES_FORMA_ADQUISICION
                                       )";

   $query_insert = mysqli_query($con,$sql_insert);

   if ($query_insert){
      $messages[] = "Vehículo agregado exitosamente.";
   } else $errors[]= "Vehículo no agregado, contacte con el administrador.".mysqli_error($con);

   if (isset($errors)){
      foreach ($errors as $error) {
         $datos=array('success'=>false, 'message'=>$error);
      }    
   }

   if (isset($messages)){
      foreach ($messages as $message) {
         $datos=array('success'=>true, 'message'=>$message);
      }  
   }
   
   echo json_encode($datos);
   exit;

}
// FIN INSERTAR MANTENEDOR VEHÍCULO------------------------------------------------

// SELECT FORMA ADQUISICION VEHICULO----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_forma_adquisicion") {

   $sql_select = "SELECT 
                  ID_FORMA_ADQUISICION id, 
                  DESCRIPCION nombre
                  FROM FORMA_ADQUISICION_VEHICULO";

   $query_select = mysqli_query($con,$sql_select);
   $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

   if ($query_select){
       $messages[] = $res_query_select;
   } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
               $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN FORMA ADQUISICION VEHICULO-------------------------------------------------------

// SELECT MARCA VEHICULO----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_marca_vehiculo") {

   $sql_select = "SELECT 
                  ID_MARCA id, 
                  NOMBRE_MARCA nombre
                  FROM MARCA_VEHICULO";

   $query_select = mysqli_query($con,$sql_select);
   $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

   if ($query_select){
       $messages[] = $res_query_select;
   } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
               $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN MARCA VEHICULO-------------------------------------------------------

// SELECT AMBIENTE VEHICULO----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_ambiente_vehiculo") {

   $sql_select = "SELECT 
                  ID_AMBIENTE id, 
                  NOMBRE_AMBIENTE nombre
                  FROM AMBIENTE_VEHICULO";

   $query_select = mysqli_query($con,$sql_select);
   $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

   if ($query_select){
       $messages[] = $res_query_select;
   } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
               $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN AMBIENTE VEHICULO-------------------------------------------------------

// SELECT COMBUSTIBLE VEHICULO----------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_combustible_vehiculo") {

   $sql_select = "SELECT 
                  ID_COMBUSTIBLE id, 
                  TIPO nombre
                  FROM TIPO_COMBUSTIBLE";

   $query_select = mysqli_query($con,$sql_select);
   $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC); 

   if ($query_select){
       $messages[] = $res_query_select;
   } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
               $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN COMBUSTIBLE VEHICULO-------------------------------------------------------