<?php

include "../../variable.php";//Contiene funcion que conecta a la base de datos

// DESCARGAR DOCUMENTO  ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="descargar_documento_negocio") {
   
   $id_documento_negocio=$_GET['id_documento_negocio'];

   $sql_select = "SELECT 
                     DESCRIPCION,
                     RUTA
                     FROM DOCUMENTO_NEGOCIO
                     WHERE ID_DOCUMENTO_NEGOCIO=$id_documento_negocio";

   $query_select = mysqli_query($con,$sql_select);
   $res_query_select = $query_select->fetch_all(MYSQLI_ASSOC);

   if ($res_query_select){
      
      $archivo = $res_query_select[0]["DESCRIPCION"];
      $ruta = '../../'.$res_query_select[0]["RUTA"];

      header('Content-Type: application/force-download');
      header('Content-Disposition: attachment; filename='.$archivo);
      header('Content-Transfer-Encoding: binary');
      header('Content-Length: '.filesize($ruta));
   
      readfile($ruta);

   } 

   exit;  

}
// FIN DESCARGAR DOCUMENTO ------------------------------------------------------------------

// ELIMINAR DOCUMENTO  ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="eliminar_documento_negocio") {
   
   $id_documento_negocio=$_GET['id_documento_negocio'];

   $sql_eliminar = "DELETE T1
                     FROM DOCUMENTO_NEGOCIO T1
                     WHERE T1.ID_DOCUMENTO_NEGOCIO=$id_documento_negocio";

   $query_eliminar = mysqli_query($con,$sql_eliminar);

   if ($query_eliminar){
       $messages[] = "Eliminado.";
   } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);

   if (isset($errors)){
       foreach ($errors as $error) {
                   $datos=array('success'=>false, 'message'=>$error);
           }    
       }

   if (isset($messages)){
        foreach ($messages as $message) {
               $datos=array('success'=>true, 'message'=>$message);
           }  
       }
   
   echo json_encode($datos);
   exit;  

}
// FIN ELIMINAR DOCUMENTO ------------------------------------------------------------------

// SUBIR DOCUMENTO  ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="subir_documento_negocio") {
   $id_negocio=$_GET["id_negocio"];
   //$fecha_actual = date('Y-m-d');
   
   //$token_rezagado=$_GET["token_rezagado"];
   //crear carpeta para guardar los archivos
   //$doc_negocio = '../boletas_docentes/'.$token_rezagado;
   $doc_negocio = '../../files/mantenedor_doc_negocio/'.$id_negocio;
   if (!file_exists($doc_negocio)) {
      mkdir($doc_negocio, 0777, true);
   } 
   
   if ($_FILES['archivo']["error"] > 0){
      $errors[]= "Lo siento algo ha salido mal, ARCHIVO CORRUPTO.". $_FILES['archivo']['error'];
    }else{
      /* echo "Nombre: " . $_FILES['archivo']['name'] . "<br>";
      echo "Tipo: " . $_FILES['archivo']['type'] . "<br>";
      echo "Tamaño: " . ($_FILES["archivo"]["size"] / 1024) . " kB<br>";
      echo "Carpeta temporal: " . $_FILES['archivo']['tmp_name']; */
   
      $nombre = $_FILES['archivo']['name'];
      $ruta = 'files/mantenedor_doc_negocio/'.$id_negocio.'/'.$nombre.'';
      move_uploaded_file($_FILES['archivo']['tmp_name'], $doc_negocio."/".$_FILES['archivo']['name']);
   
      $sql_insert = "INSERT INTO DOCUMENTO_NEGOCIO (
                                                ID_NEGOCIO,
                                                DESCRIPCION,
                                                RUTA,
                                                FECHA
                                                )
                                                VALUES (
                                                $id_negocio,
                                                '$nombre',
                                                '$ruta',
                                                CURDATE()
                                                )";                                          
   
      $query_insert = mysqli_query($con,$sql_insert);
   
      if ($query_insert){
      $messages[] = "Asignado.";
      } else $errors[]= "Lo siento algo ha salido mal, intenta nuevamente.".mysqli_error($con);
   
    }
   
   if (isset($errors)){
      foreach ($errors as $error) {
         $datos=array('success'=>false, 'message'=>$error);
      }    
   }
   
   if (isset($messages)){
      foreach ($messages as $message) {
         $datos=array('success'=>true, 'message'=>$message);
      }  
   }
   
   echo json_encode($datos);
   exit;  
}
// FIN SUBIR DOCUMENTO  ------------------------------------------------------------------
