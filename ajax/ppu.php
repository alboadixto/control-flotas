<?php

include "../variables.php";

// SELECT PPU_AMBIENTE ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_ambiente") {

    $sql_select = "SELECT 
                     DISTINCT(ambiente) nombre
                     FROM vehiculos 
                     WHERE ambiente IS NOT NULL AND permiso_circulacion >=CURDATE()
                     ORDER BY 1";

    $query = mysqli_query($con,$sql_select);
    $res_query = $query->fetch_all(MYSQLI_ASSOC); 

    if ($query){
        $messages[] = $res_query;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN SELECT PPU_AMBIENTE------------------------------------------------------------------

// SELECT PPU_KILOS ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_kilos") {

    $sql_select = "SELECT 
                     DISTINCT(kilos) nombre
                     FROM vehiculos
                     WHERE kilos IS NOT NULL AND permiso_circulacion >=CURDATE()
                     ORDER BY 1";

    $query = mysqli_query($con,$sql_select);
    $res_query = $query->fetch_all(MYSQLI_ASSOC); 

    if ($query){
        $messages[] = $res_query;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN SELECT PPU_KILOS------------------------------------------------------------------

// SELECT PPU_METROS ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_metros") {

    $sql_select = "SELECT 
                     DISTINCT(vol_mcubicos) nombre
                     FROM vehiculos
                     WHERE vol_mcubicos IS NOT NULL AND permiso_circulacion >=CURDATE()
                     ORDER BY 1";

    $query = mysqli_query($con,$sql_select);
    $res_query = $query->fetch_all(MYSQLI_ASSOC); 

    if ($query){
        $messages[] = $res_query;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN SELECT PPU_METROS------------------------------------------------------------------

// SELECT RECORRIDO ------------------------------------------------------------------
if (isset($_GET['action']) && $_GET['action']=="select_recorrido") {

    $sql_select = "SELECT 
                        RE.idrecorrido id, 
                        RE.n_recorrido nombre 
                    FROM recorrido RE
                    WHERE RE.idrecorrido NOT IN (SELECT id_recorrido FROM asignacion_recorrido) AND RE.fecha_import = '$fecha'
                    ORDER BY 1";

    $query = mysqli_query($con,$sql_select);
    $res_query = $query->fetch_all(MYSQLI_ASSOC); 

    if ($query){
        $messages[] = $res_query;
    } else $errors[]= "Datos no cargados, intenta nuevamente.".mysqli_error($con);

    if (isset($errors)){
        foreach ($errors as $error) {
                    $datos=array('success'=>false, 'message'=>$error);
            }    
        }

    if (isset($messages)){
         foreach ($messages as $message) {
                $datos=array('success'=>true, 'message'=>$message);
            }  
        }
    
    echo json_encode($datos);
    exit;  

}
// FIN SELECT RECORRIDO------------------------------------------------------------------