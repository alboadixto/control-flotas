<?php
include_once('../../../DAL/gd/gd_boletas_profesores.php');
$gd_boletas_profesores = new gd_boletas_profesores();

// funcion para eliminar la carpeta y todo su contenido, ya que no se elimina la carpeta si no se elimina el contenido dentro.
function rmDir_rf($carpeta){
  foreach(glob($carpeta . "/*") as $archivos_carpeta){             
    if (is_dir($archivos_carpeta)){
      rmDir_rf($archivos_carpeta);
    } else {
    unlink($archivos_carpeta);
    }
  }
  rmdir($carpeta);
}
/////////////////////////////////////////////////////////////////////////////

// borrar la carpeta del servidor y elimina el registro completo en la base de datos.
if (isset($_GET["agno"]) && isset($_GET["mes"]) && isset($_GET["rut"])){
  $consultar=$gd_boletas_profesores->buscarId_rezagado($_GET["agno"],$_GET["mes"],$_GET["rut"]);
  $res=$gd_boletas_profesores->eliminar_rezagado($_GET["agno"],$_GET["mes"],$_GET["rut"]);

  if ($res) {
    $carpeta = $consultar[0]["BOPR_PATH_BOLETA"];
    // activamos la función y se borra la carpeta
    rmDir_rf($carpeta);
    $data=array("success"=>true);
  }else $data=array("success"=>false);
  echo json_encode($data);
  exit;
}

// borrar la carpeta temporal, sin que se haya subido a la base de datos
if(isset($_GET['token_rezagado'])){
  $carpeta = '../boletas_docentes/rezagados/'.$_GET['token_rezagado'];

  // activamos la función y se borra la carpeta
  rmDir_rf($carpeta);

}
?>
