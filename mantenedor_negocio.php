<?php

  session_start();
  include "config/config.php";

  if (!isset($_SESSION['user_id']) && $_SESSION['user_id']==null || $_SESSION['user_id']!=1) {
      header("location: config/index.php");
  }

  include "head.php";

// MANTENEDOR negocio--------------------------------------
  $query_form_mantenedor_negocio=mysqli_query($con,"SELECT * FROM FORM_MANTENEDOR_NEGOCIO WHERE ESTADO=1 ORDER BY 4");

  $class_col=' class="col-xs-12 col-sm-12 col-md-12 col-lg-4"';
  $count_separacion=3; 
  $item_negocio="";
  $item_documentacion="";
  $item_plazo="";
  $count_negocio=0;
  $count_documentacion=0;
  $count_plazo=0;
  
   while ($row=mysqli_fetch_array($query_form_mantenedor_negocio)) {
   
      switch ($row["CATEGORIA"]) {
         case 1:
            $count_negocio++;
            $form_group_ini_negocio='';
            $form_group_fin_negocio='';
            
            if($count_negocio == 1){
               $form_group_ini_negocio='<div class="row form-group">';
            }
      
            if($count_negocio == $count_separacion){
               $form_group_fin_negocio='</div>';
               $count_negocio = 0;
            }
         
            $item_negocio.=$form_group_ini_negocio.' 
                                    <div'.$class_col.'>
                                       <div class="input-group">
                                          <div class="input-group-addon">
                                             <i class="'.$row["CLASS"].'">'.$row["DESCRIPCION"].'</i>
                                          </div>
                                          '.$row["INPUT"].'
                                       </div>
                                    </div'.'>'.$form_group_fin_negocio;
            break;
         case 2:
            $count_documentacion++;
            $form_group_ini_documentacion='';
            $form_group_fin_documentacion='';
            
            if($count_documentacion == 1){
               $form_group_ini_documentacion='<div class="row form-group">';
            }
      
            if($count_documentacion == $count_separacion){
               $form_group_fin_documentacion='</div>';
               $count_documentacion = 0;
            }
            
            $item_documentacion.=$form_group_ini_documentacion.' 
                                    <div'.$class_col.'>
                                       <div class="input-group">
                                          <div class="input-group-addon">
                                             <i class="'.$row["CLASS"].'">'.$row["DESCRIPCION"].'</i>
                                          </div>
                                          '.$row["INPUT"].'
                                       </div>
                                    </div>'.$form_group_fin_documentacion;
            break;
         case 3:
            $count_plazo++;
            $form_group_ini_plazo='';
            $form_group_fin_plazo='';
            
            if($count_plazo == 1){
               $form_group_ini_plazo='<div class="row form-group">';
            }
      
            if($count_plazo == $count_separacion){
               $form_group_fin_plazo='</div>';
               $count_plazo = 0;
            }
            
            $item_plazo.=$form_group_ini_plazo.' 
                                    <div'.$class_col.'>
                                       <div class="input-group">
                                          <div class="input-group-addon">
                                             <i class="'.$row["CLASS"].'">'.$row["DESCRIPCION"].'</i>
                                          </div>
                                          '.$row["INPUT"].'
                                       </div>
                                    </div>'.$form_group_fin_plazo;
            break;
      }
  
   }

  if($form_group_fin_negocio == ''){
   $form_group_fin_negocio='</div>';
  } else $form_group_fin_negocio='';

  $item_negocio.=$form_group_fin_negocio;

  if($form_group_fin_documentacion == ''){
   $form_group_fin_documentacion='</div>';
  } else $form_group_fin_documentacion='';

  $item_documentacion.=$form_group_fin_documentacion;

  if($form_group_fin_plazo == ''){
   $form_group_fin_plazo='</div>';
  } else $form_group_fin_plazo='';

  $item_plazo.=$form_group_fin_plazo;

// FIN MANTENEDOR negocio--------------------------------------

// MODAL -->
  
   
   include "modal/mantenedor/negocio.php";
   include "modal/mantenedor/documento_negocio.php";


// END MODAL -->
?>

<section class="p-t-20">
  
  <!-- CONTAINER -->
  <div class="container">

   <!-- Formulario ingresar negocio-->
   <div id="div_ingresar_negocio" class="col-lg-12">
      <div class="card">
      
         <div class="card-header">
            <i class="fa fa-industry"></i>
            <strong> Ingreso Negocio</strong>
            <span id="loading_gif_mantenedor_negocio" aria-hidden="true" class="pull-right"></span>
         </div>
         
         <div class="card-body card-block">
            <form id="frm_mantenedor_negocio" name="frm_mantenedor_negocio">

               <div class="custom-tab">                 
                  <nav>
                     <div class="nav nav-tabs" id="nav-tab" role="tablist">
                     
                        <a class="nav-item nav-link active show" id="nav-negocio-tab" data-toggle="tab" href="#nav-negocio" role="tab" aria-controls="nav-negocio" aria-selected="true">Información general de negocio</a>
                        
                        <a class="nav-item nav-link" id="nav-documentacion-tab" data-toggle="tab" href="#nav-documentacion" role="tab" aria-controls="nav-documentacion" aria-selected="false">Documentación para pago</a>
                        
                        <a class="nav-item nav-link" id="nav-plazo-tab" data-toggle="tab" href="#nav-plazo" role="tab" aria-controls="nav-plazo" aria-selected="false">Plazo de pago</a>
                     
                     </div>
                  </nav>

                  <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                     <div class="tab-pane fade active show" id="nav-negocio" role="tabpanel" aria-labelledby="nav-negocio-tab">
                        
                        <?php echo $item_negocio; ?>
                     
                     </div>

                     <div class="tab-pane fade" id="nav-documentacion" role="tabpanel" aria-labelledby="nav-documentacion-tab">
                        
                        <?php echo $item_documentacion; ?>
                     
                     </div>
                     
                     <div class="tab-pane fade" id="nav-plazo" role="tabpanel" aria-labelledby="nav-plazo-tab">
                        
                        <?php echo $item_plazo; ?>
                     
                     </div>
                  
                  </div>
               </div>

            </form>
         </div>

         <div class="card-footer">
               <button type="button" id="btn_limpiar_mantenedor_negocio" class="btn btn-info" title="Limpiar formulario negocio">
                  <i class="fa fa-eraser"></i>
               </button>
               <button type="button" id="btn_mantenedor_negocio" class="btn btn-success pull-right" title="Agregar vehículo">
                  <i class="fas fa-check"></i>
               </button>
         </div>

      </div>
   </div>
   <!-- Fin Formulario ingresar negocio-->

   <!-- TABLA NEGOCIO-->
   <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> Negocio</h3>
   <div class="row m-t-30">
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="table-responsive m-b-40">

         <?php 

            include "dt/mantenedor/view/dt_mantenedor_negocio.php" 

         ?>

         </div>
      </div>
   </div>
   <!-- TABLA NEGOCIO-->

  </div>
  <!-- FIN CONTAINER -->

</section>

<?php 
  
  include "footer.php" 

?>

<script type="text/javascript" src="js/mantenedor/negocio.js"></script>
<script type="text/javascript" src="dt/mantenedor/dt.js"></script>

