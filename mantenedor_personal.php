<?php

  session_start();
  include "config/config.php";

  if (!isset($_SESSION['user_id']) && $_SESSION['user_id']==null || $_SESSION['user_id']!=1) {
      header("location: config/index.php");
  }

  include "head.php";

// MANTENEDOR PERSONAL--------------------------------------
  $query_form_mantenedor_personal=mysqli_query($con,"SELECT * FROM FORM_MANTENEDOR_PERSONAL WHERE ESTADO=1 ORDER BY 4");

   $class_col=' class="col-xs-12 col-sm-12 col-md-12 col-lg-4"';
   $count_separacion=3; 
   $item_datos_personales="";
   $item_info_general="";
   $item_foto_perfil="";
   $count_datos_personales=0;
   $count_info_general=0;
   $count_foto_perfil=0;

   while ($row=mysqli_fetch_array($query_form_mantenedor_personal)) {
   
      switch ($row["CATEGORIA"]) {
         case 1:
            $count_datos_personales++;
            $form_group_ini_datos_personales='';
            $form_group_fin_datos_personales='';
            
            if($count_datos_personales == 1){
               $form_group_ini_datos_personales='<div class="row form-group">';
            }
      
            if($count_datos_personales == $count_separacion){
               $form_group_fin_datos_personales='</div>';
               $count_datos_personales = 0;
            }
            
            $item_datos_personales.=$form_group_ini_datos_personales.' 
                                    <div'.$class_col.'>
                                       <div class="input-group">
                                          <div class="input-group-addon">
                                             <i class="'.$row["CLASS"].'">'.$row["DESCRIPCION"].'</i>
                                          </div>
                                          '.$row["INPUT"].'
                                       </div>
                                    </div'.'>'.$form_group_fin_datos_personales;
            break;
         case 2:
            $count_info_general++;
            $form_group_ini_info_general='';
            $form_group_fin_info_general='';
            
            if($count_info_general == 1){
               $form_group_ini_info_general='<div class="row form-group">';
            }
      
            if($count_info_general == $count_separacion){
               $form_group_fin_info_general='</div>';
               $count_info_general = 0;
            }
            
            $item_info_general.=$form_group_ini_info_general.' 
                                    <div'.$class_col.'>
                                       <div class="input-group">
                                          <div class="input-group-addon">
                                             <i class="'.$row["CLASS"].'">'.$row["DESCRIPCION"].'</i>
                                          </div>
                                          '.$row["INPUT"].'
                                       </div>
                                    </div>'.$form_group_fin_info_general;
            break;
         case 3:
            $count_foto_perfil++;
            $form_group_ini_foto_perfil='';
            $form_group_fin_foto_perfil='';
            
            if($count_foto_perfil == 1){
               $form_group_ini_foto_perfil='<div class="row form-group">';
            }
      
            if($count_foto_perfil == $count_separacion){
               $form_group_fin_foto_perfil='</div>';
               $count_foto_perfil = 0;
            }
            
            $item_foto_perfil.=$form_group_ini_foto_perfil.' 
                                    <div'.$class_col.'>
                                       <div class="input-group">
                                          '.$row["INPUT"].'
                                       </div>
                                    </div>'.$form_group_fin_foto_perfil;
            break;
      }
   }

   if($form_group_fin_datos_personales == ''){
   $form_group_fin_datos_personales='</div>';
   } else $form_group_fin_datos_personales='';

   $item_datos_personales.=$form_group_fin_datos_personales;

   if($form_group_fin_info_general == ''){
   $form_group_fin_info_general='</div>';
   } else $form_group_fin_info_general='';

   $item_info_general.=$form_group_fin_info_general;

   if($form_group_fin_foto_perfil == ''){
   $form_group_fin_foto_perfil='</div>';
   } else $form_group_fin_foto_perfil='';

   $item_foto_perfil.=$form_group_fin_foto_perfil;

// FIN MANTENEDOR PERSONAL--------------------------------------

?>

<section class="p-t-20">
  
  <!-- CONTAINER -->
  <div class="container">

   <!-- Formulario ingresar personal-->
   <div id="div_ingresar_personal" class="col-lg-12">
      <div class="card">
      
         <div class="card-header">
            <i class="fa fa-user-circle"></i>
            <strong> Ingreso Personal</strong>
            <span id="loading_gif_mantenedor_personal" aria-hidden="true" class="pull-right"></span>
         </div>
         
         <div class="card-body card-block">
            <form id="frm_mantenedor_personal" name="frm_mantenedor_personal">

               <div class="custom-tab">                 
                  <nav>
                     <div class="nav nav-tabs" id="nav-tab" role="tablist">
                     
                        <a class="nav-item nav-link active show" id="nav-datos_personales-tab" data-toggle="tab" href="#nav-datos_personales" role="tab" aria-controls="nav-datos_personales" aria-selected="true">Datos Personales</a>
                        
                        <a class="nav-item nav-link" id="nav-info_general-tab" data-toggle="tab" href="#nav-info_general" role="tab" aria-controls="nav-info_general" aria-selected="false">Info. General</a>

                        <a class="nav-item nav-link" id="nav-foto_perfil-tab" data-toggle="tab" href="#nav-foto_perfil" role="tab" aria-controls="nav-foto_perfil" aria-selected="false">Foto Perfil</a>
                     
                     </div>
                  </nav>

                  <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                     <div class="tab-pane fade active show" id="nav-datos_personales" role="tabpanel" aria-labelledby="nav-datos_personales-tab">
                        
                        <?php echo $item_datos_personales; ?>
                     
                     </div>

                     <div class="tab-pane fade" id="nav-info_general" role="tabpanel" aria-labelledby="nav-info_general-tab">
                        
                        <?php echo $item_info_general; ?>
                     
                     </div>

                     <div class="tab-pane fade" id="nav-foto_perfil" role="tabpanel" aria-labelledby="nav-foto_perfil-tab">
                        
                        <?php echo $item_foto_perfil; ?>
                     
                     </div>
                  
                  </div>
               </div>

            </form>
         </div>

         <div class="card-footer">
               <button type="button" id="btn_limpiar_mantenedor_personal" class="btn btn-info" title="Limpiar formulario Personal">
                  <i class="fa fa-eraser"></i>
               </button>
               <button type="button" id="btn_mantenedor_personal" class="btn btn-success pull-right" title="Agregar personal">
                  <i class="fas fa-check"></i>
               </button>
         </div>

      </div>
   </div>
   <!-- Fin Formulario ingresar personal-->

   <!-- TABLA PERSONAL-->
   <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> Personal</h3>
   <div class="row m-t-30">
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="table-responsive m-b-40">

         <?php 

            include "dt/mantenedor/view/dt_mantenedor_personal.php" 

         ?>

         </div>
      </div>
   </div>
   <!-- TABLA PERSONAL-->

  </div>
  <!-- FIN CONTAINER -->

</section>

<?php 
  
  include "footer.php" 

?>

<script type="text/javascript" src="js/mantenedor/personal.js"></script>
<script type="text/javascript" src="dt/mantenedor/dt.js"></script>