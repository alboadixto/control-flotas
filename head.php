<?php
    
   $title ="Flotas Admin | ";
   $id=$_SESSION['user_id'];
    
   $query=mysqli_query($con,"SELECT * from USUARIO where idusuarios=$id");
   while ($row=mysqli_fetch_array($query)) {
      $username = $row['rut_usuario'];
      $name = $row['nombre_completo'];
      $email = $row['correo'];
   }

   $query_menu_sub=mysqli_query($con,"SELECT * from MENU_SUB WHERE estado=1 ORDER BY 6");
   $item="";
   while ($row=mysqli_fetch_array($query_menu_sub)) {
      $item.='<a href="'.$row["href"].'"><i class="'.$row["class"].'"></i>'.$row["descripcion"].'</a>';
   }

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="angelo.carrasco1990@gmail.com">
    <meta name="author" content="angelo.carrasco1990@gmail.com">
    <meta name="keywords" content="angelo.carrasco1990@gmail.com">

    <!-- Title Page-->
    <title><?php echo $title.$name  ?></title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Fontfaces CSS 2.0 https://fontawesome.com/icons?d=gallery-->
    <link href="vendor/fontawesome-free-5.8.1-web/css/all.min.css" rel="stylesheet" media="all">

    <!-- Material Icons https://google.github.io/material-design-icons/#icon-font-for-the-web-->
    <link href="vendor/material-icons/iconfont/material-icons.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

    <!-- DataTable-->
    <link href="vendor/datatables.min.css" rel="stylesheet" media="all">

    <!-- alertify -->
        <!-- include the style -->
    <link rel="stylesheet" href="vendor/AlertifyJS-master/build/css/alertify.min.css" media="all"/>
        <!-- include a theme -->
    <link rel="stylesheet" href="vendor/AlertifyJS-master/build/css/themes/bootstrap.css" media="all"/>

    <!-- JqueryFiler -->
    <link href="vendor/jQueryfiler/css/jquery.filer.css" rel="stylesheet" media="all"/> 

    <!-- Select2 -->
    <link rel="stylesheet" href="vendor/select2-4.0.6-rc.1/dist/css/select2.min.css">
    <link rel="stylesheet" href="vendor/select2-bootstrap4-theme-master/dist/select2-bootstrap4.min.css">

</head>

<body class="animsition">

    <!-- PAGE WRAPPER-->
    <div class="page-wrapper">

        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="flotas_index.php">
                            <img src="images/icon/logo.png" alt="FlotasAdmin" />
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled">
                            <!-- <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-calendar-alt"></i> Inicio Día
                                    <span class="bot-line"></span>
                                </a>
                                <ul class="header3-sub-list list-unstyled">
                                    <li>
                                        <a href="flotas_index.php"><i class="fas fa-truck"></i> Asignación</a>
                                    </li>
                                </ul>
                            </li> -->

                            <li>
                                <a href="flotas_index.php"><i class="fas fa-plus-circle"></i>
                                    <span class="bot-line"></span> Home</a>  
                            </li>

                            <li>
                                <a href="retorno.php"><i class="fas fa-truck"></i>
                                    <span class="bot-line"></span> Vehículos</a>  
                            </li>

                            <li>
                                <a href="usuarios.php"><i class="fas fa-users"></i>
                                    <span class="bot-line"></span>Negocio</a>
                            </li>

                            <li>
                                <a href="ppu.php"><i class="fas fa-truck"></i>
                                    <span class="bot-line"></span>Servicio Técnico</a>
                            </li>

                            <li>
                                <a href="ppu.php"><i class="fas fa-truck"></i>
                                    <span class="bot-line"></span>Persona</a>
                            </li>

                            <li>
                                <a href="ppu.php"><i class="fas fa-truck"></i>
                                    <span class="bot-line"></span>Asignaciones</a>
                            </li>

                            <li>
                                <a href="ppu.php"><i class="fas fa-truck"></i>
                                    <span class="bot-line"></span>Call</a>
                            </li>

                        </ul>
                    </div>
                    <div class="header__tool">

                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src="images/icon/avatar-01.jpg" alt="laUltimaMilla" />
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn" href="#"><?php echo $name ?></a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="info clearfix">
                                        <div class="image">
                                            <a href="#">
                                                <img src="images/icon/avatar-01.jpg" alt="laUltimaMilla" />
                                            </a>
                                        </div>
                                        <div class="content">
                                            <h5 class="name">
                                                <a href="#"><?php echo $name ?></a>
                                            </h5>
                                            <span class="email"><?php echo $email ?></span>
                                        </div>
                                    </div>
                                    <div class="account-dropdown__footer">
                                    
                                       <?php echo $item ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="flotas_index.php">
                            <img src="images/icon/logo.png" alt="FlotasAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <!-- <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-calendar-alt"></i> Inicio Día</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="flotas_index.php"><i class="fas fa-truck"></i> Asignación</a>
                                </li>
                            </ul>
                        </li> -->

                        <li>
                            <a href="flotas_index.php"><i class="fas fa-plus-circle"></i>
                                <span class="bot-line"></span> Asignación</a>  
                        </li>

                        <li>
                            <a href="retorno.php"><i class="fas fa-exchange-alt"></i>
                                <span class="bot-line"></span> Retorno</a>  
                        </li>

                        <li>
                            <a href="usuarios.php"><i class="fas fa-users"></i>
                                <span class="bot-line"></span>Usuarios</a>
                        </li>

                        <li>
                            <a href="ppu.php"><i class="fas fa-truck"></i>
                                <span class="bot-line"></span>PPU</a>
                            </li>
                    </ul>
                </div>
            </nav>
        </header>

        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        <div class="image">
                            <img src="images/icon/avatar-01.jpg" alt="laUltimaMilla" />
                        </div>
                        <div class="content">
                            <a class="js-acc-btn" href="#"><?php echo $name ?></a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="info clearfix">
                                <div class="image">
                                    <a href="#">
                                        <img src="images/icon/avatar-01.jpg" alt="laUltimaMilla" />
                                    </a>
                                </div>
                                <div class="content">
                                    <h5 class="name">
                                        <a href="#"><?php echo $name ?></a>
                                    </h5>
                                    <span class="email"><?php echo $email ?></span>
                               </div>
                            </div>
                            <div class="account-dropdown__footer">

                              <?php echo $item ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
