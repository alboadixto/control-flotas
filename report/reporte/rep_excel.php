<?php
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet as spreadsheet; // instead PHPExcel
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as xlsx; // Instead PHPExcel_Writer_Excel2007
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing as drawing; // Instead PHPExcel_Worksheet_Drawing
use PhpOffice\PhpSpreadsheet\Style\Style as style; // Instead style
use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment; // Instead alignment
use PhpOffice\PhpSpreadsheet\Style\Fill as fill; // Instead fill
use PhpOffice\PhpSpreadsheet\Style\Border as border; // Instead border
use PhpOffice\PhpSpreadsheet\Style\Color; //Instead PHPExcel_Style_Color
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup as pagesetup; // Instead PHPExcel_Worksheet_PageSetup
use PhpOffice\PhpSpreadsheet\IOFactory; // Instead PHPExcel_IOFactory
//http://www.codedrinks.com/crear-un-reporte-en-excel-con-php-y-mysql/
//http://www.colorscodes.com/

/* TIPS---------------------------------------------
Si las fechas tienen el mismo formato las puedes comparar directamente:
$fecha1="27-10-2008";
$fecha2="28-10-2008";
if($fecha1 > $fecha2)
echo "$fecha1 > $fecha2";

Si las fechas tienen distinto formato lo puedes hacer con la función strtotime:
$fecha1=strtotime("27-10-2008");
$fecha2=strtotime("2008-10-28");
if($fecha1 > $fecha2)
echo "Fecha1 > Fecha2";
-----------------------------------------------------*/

session_start();
//conexiones, conexiones everywhere
include "../../config/config.php";//Contiene funcion que conecta a la base de datos

//variables
$nombre="Reporte";
$fecha_actual = date("d-m-Y");
$sTable="vista_dashboard";
$sWhere="";

//Obtener la fecha máxima-----------------------
$sql_fecha_max = "SELECT MAX(fecha_import) fecha FROM recorrido";

$query_fecha_max = mysqli_query($con,$sql_fecha_max);

$res_query_fecha_max = $query_fecha_max->fetch_all(MYSQLI_ASSOC);
$fecha = $res_query_fecha_max[0]["fecha"];
$filtro_fecha = "AND fecha = '$fecha' ";
//Fin Obtener la fecha máxima-----------------------

/////////////////////////////////////////////////////////////////

    if (isset($_GET["start_at"])){
        if ($_GET["start_at"]!="") {
          $filtro_fecha=" AND fecha = '{$_GET["start_at"]}' ";
        }
    }

$sWhere=$filtro_fecha;
/////////////////////////////////////////////////////////////////

//contar para saber si tengo datos o no
$count_query = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable WHERE 1=1 $sWhere");
$row= mysqli_fetch_array($count_query);
$numrows = $row['numrows']; 

//Inicio de la instancia para la exportación en Excel
	if($numrows > 0){				
		date_default_timezone_set('America/Santiago');
		if (PHP_SAPI == 'cli')
			die('Este archivo solo se puede ver desde un navegador web');

		// Se crea el objeto PHPExcel
		$objPHPExcel = new spreadsheet();
		// Se asignan las propiedades del libro
		$objPHPExcel->getProperties()->setCreator("Angelo Carrasco S.") //Autor
							 ->setLastModifiedBy("Angelo Carrasco S.") //Ultimo usuario que lo modificó
							 ->setTitle("Reporte Gestion")
							 ->setSubject("Reporte")
							 ->setDescription("Reporte Gestion")
							 ->setKeywords("Reporte Gestion ")
							 ->setCategory("Reporte excel");
    $tituloReporte = "DASHBOARD";//A1

		$titulosColumnas = array(
      'RECORRIDO',//A2
      'PATENTE',//B2
      'EFECTIVIDAD',//C2
      'CONDUCTOR',//D2
      'EFECTIVIDAD',//E2
      'ASISTENTE',//F2
      'EFECTIVIDAD',//G2
      'EMPRESA',//H2
      'VALOR',//I2
      'FECHA',//J2
    );

		$objPHPExcel->setActiveSheetIndex(0)
        		    ->mergeCells('A1:J1');		
		// Se agregan los titulos del reporte
		$objPHPExcel->setActiveSheetIndex(0)
                  ->setCellValue('A1',  $tituloReporte)
                  ->setCellValue('A2',  $titulosColumnas[0])
                  ->setCellValue('B2',  $titulosColumnas[1])
                  ->setCellValue('C2',  $titulosColumnas[2])
                  ->setCellValue('D2',  $titulosColumnas[3])
                  ->setCellValue('E2',  $titulosColumnas[4])
                  ->setCellValue('F2',  $titulosColumnas[5])
                  ->setCellValue('G2',  $titulosColumnas[6])
                  ->setCellValue('H2',  $titulosColumnas[7])
                  ->setCellValue('I2',  $titulosColumnas[8])
                  ->setCellValue('J2',  $titulosColumnas[9]);

		$i = 3; //Numero de fila donde se va a comenzar a rellenar
    //Obtener los datos de partidas abiertas
    $sql_dashboard="SELECT
                            *
                            FROM
                              $sTable
                            WHERE 1=1 $sWhere";

    $query = mysqli_query($con, $sql_dashboard);
//print_r(mysqli_fetch_array($query));
//exit;
		while ($lst_excel=mysqli_fetch_array($query)) {
    
			$objPHPExcel->setActiveSheetIndex(0)
                  ->setCellValue('A'.$i,  $lst_excel['n_recorrido'])
                  ->setCellValue('B'.$i,  $lst_excel['patente'])
                  ->setCellValue('C'.$i,  $lst_excel['efectividad_evaluacion_vehiculo'])
                  ->setCellValue('D'.$i,  $lst_excel['conductor'])
                  ->setCellValue('E'.$i,  $lst_excel['efectividad_evaluacion_conductor'])
                  ->setCellValue('F'.$i,  $lst_excel['asistente'])
                  ->setCellValue('G'.$i,  $lst_excel['efectividad_evaluacion_asistente'])
                  ->setCellValue('H'.$i,  $lst_excel['negocio'])
                  ->setCellValue('I'.$i,  $lst_excel['valor'])
                  ->setCellValue('J'.$i,  $lst_excel['fecha']);
        
                  $i++;
		}


		$estiloTituloReporte = array(
        	'font' => array(
	        	'name'      => 'Verdana',
    	        'bold'      => true,
        	    'italic'    => false,
                'strike'    => false,
               	'size' =>14,
	            	'color'     => array(
    	            	'rgb' => 'FFFFFF'
        	       	)
            ),
          'fill' => array(
            'fillType'  => fill::FILL_SOLID,
            'color' => array('argb' => '40B222')
          //http://www.colorscodes.com/
          ),
            'borders' => array(
               	'allborders' => array(
                	'style' => border::BORDER_NONE                    
               	)
            ), 
            'alignment' =>  array(
        			'horizontal' => alignment::HORIZONTAL_CENTER,
        			'vertical'   => alignment::VERTICAL_CENTER,
        			'rotation'   => 0,
        			'wrap'          => TRUE
    		)
        );

		$estiloTituloColumnas = array(
            'font' => array(
                'name'      => 'Arial',
                'bold'      => true,                          
                'color'     => array(
                    'rgb' => 'FFFFFF'
                )
            ),
            'fill' => array(
              'fillType'  => fill::FILL_SOLID,
              'color' => array('argb' => '9B1E33')
          //http://www.colorscodes.com/
            ),
            'borders' => array(
            	'top'     => array(
                    'style' => border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '143860'
                    )
                ),
                'bottom'     => array(
                    'style' => border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '143860'
                    )
                )
            ),
			'alignment' =>  array(
        			'horizontal' => alignment::HORIZONTAL_CENTER,
        			'vertical'   => alignment::VERTICAL_CENTER,
        			'wrap'          => TRUE
    		));	

		$estiloInformacion = new style();
		$estiloInformacion->applyFromArray(
			array(
           	'font' => array(
               	'name'      => 'Arial',               
               	'color'     => array(
                   	'rgb' => '000000'
               	)
           	),
            'alignment' => array(
                'horizontal' => alignment::HORIZONTAL_LEFT,
            ),
           	'borders' => array(
               	'left'     => array(
                   	'style' => border::BORDER_THIN ,
	                'color' => array(
    	            	'rgb' => '3a2a47'
                   	)
               	)             
           	)
        )); 

		$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($estiloTituloColumnas);
		$objPHPExcel->getActiveSheet()->duplicateStyle($estiloInformacion, "A3:J".($i-1));
        // se aplica el tamaño de las columnas	
		$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('D')->setWidth(50);
    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('E')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('F')->setWidth(50);
    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('G')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('H')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('I')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('J')->setWidth(20);
    /* for($i = 'A'; $i <= 'AW'; $i++){
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($i)->setAutoSize(TRUE);
		}*/
		// Se asigna el nombre a la hoja
		$objPHPExcel->getActiveSheet()->setTitle($nombre);
		// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
		$objPHPExcel->setActiveSheetIndex(0);

		// Inmovilizar paneles 
		//$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,3);


		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment; filename=Reporte_$fecha_actual.xlsx");
		header('Cache-Control: max-age=0');
    $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
    $objWriter->save('php://output');
		//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		//$objWriter->save('php://output');

		exit;

	}
	else{
		print_r('No hay resultados para mostrar');
	}
?>