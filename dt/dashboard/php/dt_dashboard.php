<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Obtener la fecha máxima-----------------------
include "../../../variables.php";
//Fin Obtener la fecha máxima-----------------------
 
// DB table to use
$table = 'vista_dashboard';
 
// Table's primary key
$primaryKey = 'id_recorrido';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(

    array( 'db' => 'n_recorrido', 'dt' => 0),
    array( 'db' => 'patente',
           'dt' => 1,
            'formatter' => function( $d, $row ) {
        
            $vehiculo="<button type=\"button\" class=\"btn btn-outline btn-sm\" title=\"Vehículo\"><i class=\"fas fa-truck\"></i>&nbsp; ".$d."</button>";

            return $vehiculo;
        }
    ),
    array( 'db'        => 'conductor',
           'dt'        => 2,
           'formatter' => function( $d, $row ) {

                $nombre="<button type=\"button\" class=\"btn btn-outline btn-sm\" title=\"Conductor\"><i class=\"fas fa-user-cog\"></i>&nbsp; ".utf8_encode($d)."</button>";
                
                return $nombre;
            }
    ),
    array( 'db'        => 'asistente',
            'dt'        => 3,
            'formatter' => function( $d, $row ) {
                
                $nombre='';

                if($row["id_asistente"]){

                    $nombre="<button type=\"button\" class=\"btn btn-outline btn-sm\" title=\"Asistente\"><i class=\"fas fa-users-cog\"></i>&nbsp; ".utf8_encode($d)."</button>";

                }
                
                return $nombre;
            }
    ),
    array( 'db'        => 'negocio',
           'dt'        => 4,
           'formatter' => function( $d, $row ) {
                $negocio='<span class="block-email">'.$d.'</span>';
                
                return $negocio;
            }
    ),
    array( 'db'        => 'valor',
           'dt'        => 5,
           'formatter' => function( $d, $row ) {
                $valor='<p style="text-align: right;">$'.number_format($d, 0, ',', '.').'</p>';
                
                return $valor;
            }
    ),
    array( 'db' => 'fecha', 'dt' => 6 ),
    array( 'db' => 'id_asistente', 'dt' => 7 )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";  */     
$extraWhere = " 1=1 AND fecha = '$fecha' ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
