<?php
//conexiones, conexiones everywhere
include "../../../variable.php";//Contiene funcion que conecta a la base de datos

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
 
// DB table to use
$table = 'VISTA_PERSONAL';
 
// Table's primary key
$primaryKey = 'ID_PERSONAL';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    
    array( 'db'        => 'ID_PERSONAL',
           'dt'        => 0,
           'formatter' => function( $d, $row ) {

                $eliminar="<button type=\"button\" class=\"btn btn-outline-danger btn-sm\" title=\"Eliminar personal\" onclick=\"eliminar_personal('$d');\"><i class=\"fas fas fa-times\"></i></button>";
      
                return $eliminar;
            }
         ),
    array( 'db'        => 'RUT', 
           'dt'        => 1,
           'formatter' => function( $d, $row ) {

              return $d;
            }
         ),
    array( 'db'        => 'NOMBRE', 
           'dt'        => 2,
           'formatter' => function( $d, $row ) {

               $d="<p>".utf8_encode($d)."</p>";

                return $d;
            }
         ),
    array( 'db'        => 'APELLIDO', 
           'dt'        => 3,
           'formatter' => function( $d, $row ) {

             $d="<p>".utf8_encode($d)."</p>";

              return $d;
          }
         ),
    array( 'db'        => 'TELEFONO', 
           'dt'        => 4,
           'formatter' => function( $d, $row ) {

              return $d;
            }
         ),
    array( 'db'        => 'FECHA_INGRESO', 
           'dt'        => 5,
           'formatter' => function( $d, $row ) {

              return $d;
            }
         ),
    array( 'db'        => 'TIPO_EMPLEADO',
           'dt'        => 6,
           'formatter' => function( $d, $row ) {

               $d="<p>".utf8_encode($d)."</p>";

                return $d;
            }
         )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";    */   
$extraWhere = " 1=1 ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
