<?php
//conexiones, conexiones everywhere
include "../../../variable.php";//Contiene funcion que conecta a la base de datos

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
 
// DB table to use
$table = 'VISTA_NEGOCIO';
 
// Table's primary key
$primaryKey = 'ID_NEGOCIOS';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    
    array( 'db'        => 'ID_NEGOCIOS',
           'dt'        => 0,
           'formatter' => function( $d, $row ) {

               $ppu="<button type=\"button\" class=\"btn btn-outline-success btn-sm\" title=\"Asignar PPU\" data-toggle=\"modal\" data-target=\"#modal_asignacion_ppu_negocio\" onclick=\"btn_asignar_ppu_negocio('$d');\"><i class=\"fas fa-truck\"></i></button>";

               $doc="<button type=\"button\" class=\"btn btn-outline-info btn-sm\" title=\"Asignar Documento\" data-toggle=\"modal\" data-target=\"#modal_documento_negocio\" onclick=\"btn_documento_negocio('$d');\"><i class=\"fas fa-file\"></i></button>";

                $eliminar="<button type=\"button\" class=\"btn btn-outline-danger btn-sm\" title=\"Eliminar negocio\" onclick=\"eliminar_negocio('$d');\"><i class=\"fas fas fa-times\"></i></button>";
      
                return $ppu." ".$doc." ".$eliminar;
            }
         ),
    array( 'db'        => 'REPRES_LEG_NOMBRE', 
           'dt'        => 1,
           'formatter' => function( $d, $row ) {

               $d="<p>".utf8_encode($d)."</p>";

                return $d;
            }
         ),
    array( 'db'        => 'REPRES_LEG_RUT',  
           'dt'        => 2,
           'formatter' => function( $d, $row ) {

               $d="<p>".utf8_encode($d)."</p>";

                return $d;
            }
         ),
    array( 'db'        => 'RAZON_SOCIAL',  
         'dt'        => 3,
         'formatter' => function( $d, $row ) {

             $d="<p>".utf8_encode($d)."</p>";

              return $d;
          }
         ),

    array( 'db'        => 'RUT_NEGOCIO',  
            'dt'        => 4,
         'formatter' => function( $d, $row ) {

           $d="<p>".utf8_encode($d)."</p>";

            return $d;
         }
        ),
    array( 'db'        => 'GIRO_NEGOCIO',  
            'dt'        => 5,
         'formatter' => function( $d, $row ) {

         $d="<p>".utf8_encode($d)."</p>";

            return $d;
         }
      ),
    array( 'db'        => 'CONTAC_NOMBRE', 
           'dt'        => 6,
           'formatter' => function( $d, $row ) {

               $d="<p>".utf8_encode($d)."</p>";  

                return $d;
            }
         ),
    array( 'db'        => 'DESCRIPCION_NEGOCIO', 
           'dt'        => 7,
           'formatter' => function( $d, $row ) {

               $d="<p>".utf8_encode($d)."</p>";

                return $d;
            }
         ),
    array( 'db'        => 'LIDER_PROYECTO', 
           'dt'        => 8,
           'formatter' => function( $d, $row ) {

                return $d;
            }
         )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";    */   
$extraWhere = " 1=1 ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
