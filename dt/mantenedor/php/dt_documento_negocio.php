<?php
//conexiones, conexiones everywhere
include "../../../variable.php";//Contiene funcion que conecta a la base de datos

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
$filtro_negocio="";
if (isset($_GET['action']) && $_GET['action']=="filtro_negocio") {
   
   $id_negocio=$_GET['id_negocio'];
   $filtro_negocio.=' AND ID_NEGOCIO = '.$id_negocio.'';


}
// DB table to use
$table = 'DOCUMENTO_NEGOCIO';
 
// Table's primary key
$primaryKey = 'ID_DOCUMENTO_NEGOCIO';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    
    array( 'db'        => 'ID_DOCUMENTO_NEGOCIO',
           'dt'        => 0,
           'formatter' => function( $d, $row ) {

               $descargar="<button type=\"button\" class=\"btn btn-outline-info btn-sm\" title=\"Descargar Documento\" onclick=\"descargar_documento_negocio('$d');\"><i class=\"fa fa-download\"></i></button>";

               $eliminar="<button type=\"button\" class=\"btn btn-outline-danger btn-sm\" title=\"Eliminar documento\" onclick=\"eliminar_documento_negocio('$d');\"><i class=\"fas fas fa-times\"></i></button>";
      
               return $descargar." ".$eliminar;
            }
         ),
    array( 'db'        => 'DESCRIPCION', 
           'dt'        => 1,
           'formatter' => function( $d, $row ) {

               $d="<p>".utf8_encode($d)."</p>";

                return $d;
            }
         ),
    array( 'db'        => 'FECHA', 
            'dt'        => 2,
            'formatter' => function( $d, $row ) {

              return $d;
          }
       )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";    */   
$extraWhere = " 1=1 $filtro_negocio ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
