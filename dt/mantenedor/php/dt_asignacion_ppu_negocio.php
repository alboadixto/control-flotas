<?php
//conexiones, conexiones everywhere
include "../../../variable.php";//Contiene funcion que conecta a la base de datos

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

$filtro_negocio=" AND 1 <> 1 ";
if (isset($_GET['action']) && $_GET['action']=="btn_asignar_ppu_negocio") {
   
   $id_negocio=$_GET['id_negocio'];
   $filtro_negocio= " AND ID_NEGOCIO = $id_negocio ";

}
 
// DB table to use
$table = 'VISTA_ASIGNAR_PPU_NEGOCIO';
 
// Table's primary key
$primaryKey = 'ID_VEHICULO';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    
    array( 'db'        => 'ID_VEHICULO',
           'dt'        => 0,
           'formatter' => function( $d, $row ) {

               $eliminar="<button type=\"button\" class=\"btn btn-outline-danger btn-sm\" title=\"Eliminar vehiculo\" onclick=\"accion_ppu(".$d.",".$row["ID_NEGOCIO"].",'eliminar');\"><i class=\"fas fas fa-times\"></i></button>";
      
                return $eliminar;
            }
         ),
    array( 'db'        => 'NOMBRE_MARCA', 
           'dt'        => 1,
           'formatter' => function( $d, $row ) {

               $d="<p>".utf8_encode($d)."</p>";

                return $d;
            }
         ),
    array( 'db'        => 'MODELO',  
           'dt'        => 2,
           'formatter' => function( $d, $row ) {

               $d="<p>".utf8_encode($d)."</p>";

                return $d;
            }
         ),
    array( 'db'        => 'ANO',
           'dt'        => 3,
           'formatter' => function( $d, $row ) {

               $d='<p style="text-align: right;">'.number_format($d, 0, ',', '.').'</p>';

                return $d;
            }
         ),
    array( 'db'        => 'PATENTE', 
           'dt'        => 4,
           'formatter' => function( $d, $row ) {

                return $d;
            }
         ),
    array( 'db'        => 'ID_NEGOCIO', 
           'dt'        => 5,
           'formatter' => function( $d, $row ) {

               return $d;
            }
         )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";    */   
$extraWhere = " 1=1 $filtro_negocio ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
