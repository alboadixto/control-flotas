var mantenedor_tabla_vehiculo;
var mantenedor_tabla_negocio;
var mantenedor_tabla_personal;
var asignacion_ppu_negocio;
var asignacion_no_ppu_negocio;
var tabla_documento_negocio;

tabla_documento_negocio=$('#tabla_documento_negocio').DataTable({
   "processing": true,
   "serverSide": true,
   "ajax": "./dt/mantenedor/php/dt_documento_negocio.php",
   "order": [[ 0, "desc" ]],
   "language": {
               "decimal":        ",",
               "emptyTable":     "No hay registros",
               "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
               "infoEmpty":      "Mostrando 0 al 0 de 0 registros",
               "infoFiltered":   "(de un total de _MAX_ registros)",
               "infoPostFix":    "",
               "thousands":      ".",
               "lengthMenu":     "Mostrar _MENU_ registros",
               "loadingRecords": "Cargando...",
               "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
               "search":         "<i class='fas fa-search'></i>",
               "zeroRecords":    "No se encontraron registros",
               "paginate": {
                   "first":      "Primero",
                   "last":       "Ultimo",
                   "next":       "Siguiente",
                   "previous":   "Anterior"
               },
               "aria": {
                   "sortAscending":  ": activate to sort column ascending",
                   "sortDescending": ": activate to sort column descending"
               }
           }
});

asignacion_no_ppu_negocio=$('#tabla_asignacion_no_ppu_negocio').DataTable({
   "processing": true,
   "serverSide": true,
   "ajax": "./dt/mantenedor/php/dt_asignacion_no_ppu_negocio.php",
   "order": [[ 0, "desc" ]],
   "language": {
               "decimal":        ",",
               "emptyTable":     "No hay registros",
               "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
               "infoEmpty":      "Mostrando 0 al 0 de 0 registros",
               "infoFiltered":   "(de un total de _MAX_ registros)",
               "infoPostFix":    "",
               "thousands":      ".",
               "lengthMenu":     "Mostrar _MENU_ registros",
               "loadingRecords": "Cargando...",
               "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
               "search":         "<i class='fas fa-search'></i>",
               "zeroRecords":    "No se encontraron registros",
               "paginate": {
                   "first":      "Primero",
                   "last":       "Ultimo",
                   "next":       "Siguiente",
                   "previous":   "Anterior"
               },
               "aria": {
                   "sortAscending":  ": activate to sort column ascending",
                   "sortDescending": ": activate to sort column descending"
               }
           }
});

asignacion_ppu_negocio=$('#tabla_asignacion_ppu_negocio').DataTable({
   "processing": true,
   "serverSide": true,
   "ajax": "./dt/mantenedor/php/dt_asignacion_ppu_negocio.php",
   "order": [[ 0, "desc" ]],
   "language": {
               "decimal":        ",",
               "emptyTable":     "No hay registros",
               "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
               "infoEmpty":      "Mostrando 0 al 0 de 0 registros",
               "infoFiltered":   "(de un total de _MAX_ registros)",
               "infoPostFix":    "",
               "thousands":      ".",
               "lengthMenu":     "Mostrar _MENU_ registros",
               "loadingRecords": "Cargando...",
               "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
               "search":         "<i class='fas fa-search'></i>",
               "zeroRecords":    "No se encontraron registros",
               "paginate": {
                   "first":      "Primero",
                   "last":       "Ultimo",
                   "next":       "Siguiente",
                   "previous":   "Anterior"
               },
               "aria": {
                   "sortAscending":  ": activate to sort column ascending",
                   "sortDescending": ": activate to sort column descending"
               }
           }
});

mantenedor_tabla_negocio=$('#mantenedor_tabla_negocio').DataTable({
   "processing": true,
   "serverSide": true,
   "ajax": "./dt/mantenedor/php/dt_mantenedor_negocio.php",
   "order": [[ 0, "desc" ]],
   "language": {
               "decimal":        ",",
               "emptyTable":     "No hay registros",
               "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
               "infoEmpty":      "Mostrando 0 al 0 de 0 registros",
               "infoFiltered":   "(de un total de _MAX_ registros)",
               "infoPostFix":    "",
               "thousands":      ".",
               "lengthMenu":     "Mostrar _MENU_ registros",
               "loadingRecords": "Cargando...",
               "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
               "search":         "<i class='fas fa-search'></i>",
               "zeroRecords":    "No se encontraron registros",
               "paginate": {
                   "first":      "Primero",
                   "last":       "Ultimo",
                   "next":       "Siguiente",
                   "previous":   "Anterior"
               },
               "aria": {
                   "sortAscending":  ": activate to sort column ascending",
                   "sortDescending": ": activate to sort column descending"
               }
           }
});

mantenedor_tabla_vehiculo=$('#mantenedor_tabla_vehiculo').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "./dt/mantenedor/php/dt_mantenedor_vehiculo.php",
    "order": [[ 0, "desc" ]],
    "language": {
                "decimal":        ",",
                "emptyTable":     "No hay registros",
                "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                "infoEmpty":      "Mostrando 0 al 0 de 0 registros",
                "infoFiltered":   "(de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ".",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
                "search":         "<i class='fas fa-search'></i>",
                "zeroRecords":    "No se encontraron registros",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
});

mantenedor_tabla_personal=$('#mantenedor_tabla_personal').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "./dt/mantenedor/php/dt_mantenedor_personal.php",
    "order": [[ 0, "desc" ]],
    "language": {
                "decimal":        ",",
                "emptyTable":     "No hay registros",
                "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                "infoEmpty":      "Mostrando 0 al 0 de 0 registros",
                "infoFiltered":   "(de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ".",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
                "search":         "<i class='fas fa-search'></i>",
                "zeroRecords":    "No se encontraron registros",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
});