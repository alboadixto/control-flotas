<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Obtener la fecha máxima-----------------------
include "../../../variables.php";
//Fin Obtener la fecha máxima-----------------------
 
// DB table to use
$table = 'vista_retorno';
 
// Table's primary key
$primaryKey = 'id_vehiculo_retorno';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(

    array( 'db' => 'orden',
           'dt' => 0,
        'formatter' => function( $d, $row ) {
        
            $estado='<span class="badge badge-pill badge-secondary" title="En ruta"><i class="fas fa-road"></i>&nbsp;&nbsp;En ruta <img src="./images/en_ruta.gif" width="32" height="5"></span>';

            if($row["estado_retorno"] != 0){
                $estado='<span class="badge badge-pill badge-info" title="'.$row["estado_retorno"].' Retorno(s)">'.$row["estado_retorno"].'</span>';
            }

            return $estado;
        }
    ),
    array( 'db' => 'patente',
           'dt' => 1,
        'formatter' => function( $d, $row ) {
        
            $vehiculo="<button type=\"button\" class=\"btn btn-link\" title=\"PPU\" data-toggle=\"modal\" data-target=\"#modal_retorno\" onclick=\"eva_recorrido('".$d."','".$row["id_vehiculo_retorno"]."','".$row["fecha_import"]."','".$row["estado_retorno"]."');\"><i class=\"fas fa-truck\"></i>&nbsp; ".$d."</button>";
    

            switch ($row["efectividad"]) {
                case ($row["efectividad"] <= 80):
                    $color='danger';
                    break;
                
                case ($row["efectividad"] <= 89):
                    $color='warning';
                    break;
                    
                default:
                    $color='success';
            }
    
                $efectividad='<span class="badge badge-pill badge-'.$color.'">efec: '.$row["efectividad"].' %</span>';

            return $vehiculo.$efectividad;
        }
    ),
    array( 'db'        => 'monto',
           'dt'        => 2,
           'formatter' => function( $d, $row ) {
                $monto='<p style="text-align: right;">$'.number_format($d, 0, ',', '.').'</p>';
                
                return $monto;
            }
    ),
    array( 'db' => 'fecha_import', 'dt' => 3 ),
    array( 'db' => 'id_vehiculo_retorno', 'dt' => 4 ),
    array( 'db' => 'efectividad', 'dt' => 5 ),
    array( 'db' => 'estado_retorno', 'dt' => 6 )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";  */     
$extraWhere = " 1=1 AND fecha_import = '$fecha' ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
