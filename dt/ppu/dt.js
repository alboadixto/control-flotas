var tabla_ppu;
var tabla_asignaciones;
var tabla_select_recorrido;

tabla_ppu=$('#tabla_ppu').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "./dt/ppu/php/dt_ppu.php",
    "order": [[ 0, 'asc' ]],
    "language": {
        "decimal":        ",",
        "emptyTable":     "No hay registros",
        "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "infoEmpty":      "0 registros",
        "infoFiltered":   "",
        "infoPostFix":    "",
        "thousands":      ".",
        "lengthMenu":     "Mostrar _MENU_ registros",
        "loadingRecords": "Cargando...",
        "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
        "search":         "<i class='fas fa-search'></i>",
        "zeroRecords":    "No se encontraron registros",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    }
});

tabla_asignaciones=$('#tabla_asignaciones').DataTable({
    "processing": true,
    "serverSide": true,
    "searching": false,
    "paging":   false,
    "ordering": false,
    "info":     false,
    "ajax": "./dt/ppu/php/dt_asignaciones.php",
    //"order": [[ 2, "desc" ]],
    "language": {
        "decimal":        ",",
        "emptyTable":     "No hay registros",
        "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "infoEmpty":      "0 registros",
        "infoFiltered":   "",
        "infoPostFix":    "",
        "thousands":      ".",
        "lengthMenu":     "Mostrar _MENU_ registros",
        "loadingRecords": "Cargando...",
        "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
        "search":         "<i class='fas fa-search'></i>",
        "zeroRecords":    "No se encontraron registros",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    }
});

tabla_select_recorrido=$('#tabla_select_recorrido').DataTable({
    "processing": true,
    "serverSide": true,
    "searching": false,
    "paging":   false,
    "ordering": false,
    "info":     false,
    "ajax": "./dt/ppu/php/dt_select_recorrido.php",
    //"order": [[ 2, "desc" ]],
    "language": {
        "decimal":        ",",
        "emptyTable":     "No hay registros",
        "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "infoEmpty":      "0 registros",
        "infoFiltered":   "",
        "infoPostFix":    "",
        "thousands":      ".",
        "lengthMenu":     "Mostrar _MENU_ registros",
        "loadingRecords": "Cargando...",
        "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
        "search":         "<i class='fas fa-search'></i>",
        "zeroRecords":    "Seleccione un recorrido",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    }
});
