<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//-----------------------
include "../../../variables.php";

$ambiente = "";
$kilos = "";
$metros = "";

if (isset($_GET['action']) && $_GET['action']=="filtro") {
    if($_GET['ppu_ambiente']!=""){
        $filtro_ambiente = $_GET['ppu_ambiente'];
        $ambiente = "AND ambiente = '$filtro_ambiente' ";
    } 
    
    if($_GET['ppu_kilos']!=""){
        $filtro_kilos = $_GET['ppu_kilos'];
        $kilos = "AND kilos = '$filtro_kilos' ";
    } 

    if($_GET['ppu_metros']!=""){
        $filtro_metros = $_GET['ppu_metros'];
        $metros = "AND vol_mcubicos = '$filtro_metros' ";
    }  
}
//-----------------------
 
// DB table to use
$table = 'vista_ppu';
 
// Table's primary key
$primaryKey = 'idvehiculos';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(

    array( 'db' => 'idvehiculos',
           'dt' => 0,
           'formatter' => function( $d, $row ) {
                $asignar="<button type=\"button\" class=\"btn btn-outline-success btn-sm\" title=\"Asignar recorrido\" data-toggle=\"modal\" data-target=\"#modal_asignacion_ppu\" onclick=\"asignar_recorrido(".$d.",'".$row["patente"]."');\"><i class=\"fas fa-plus\"></i></button>";

                return $asignar;
            }
    ),
    array( 'db' => 'patente',
           'dt' => 1,
            'formatter' => function( $d, $row ) {

                $estado="";

                $vehiculo="<button type=\"button\" class=\"btn btn-outline-link\" title=\"PPU\"><i class=\"fas fa-truck\"></i>&nbsp; ".$d."</button>";

                if(isset($row["estado_retorno"])){
                    $estado='<span class="badge badge-pill badge-secondary" title="En ruta"><i class="fas fa-road"></i>&nbsp;&nbsp;En ruta <img src="./images/en_ruta.gif" width="32" height="5"></span>';
                }

                return $vehiculo.$estado;
            }
    ),
    array( 'db' => 'descripcion',
           'dt' => 2,
            'formatter' => function( $d, $row ) {
                $d=utf8_encode($d);
                
                return $d;
            }
    ),
    array( 'db' => 'ano_vehiculo',
           'dt' => 3,
            'formatter' => function( $d, $row ) {

                return $d;
            }
    ),
    array( 'db' => 'ambiente',
           'dt' => 4,
            'formatter' => function( $d, $row ) {

                return $d;
            }
    ),
    array( 'db' => 'permiso_circulacion',
           'dt' => 5,
            'formatter' => function( $d, $row ) {

                return $d;
            }
    ),
    array( 'db' => 'seguro',
           'dt' => 6,
            'formatter' => function( $d, $row ) {

                return $d;
            }
    ),
    array( 'db' => 'estado_retorno', 'dt' => 7 )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";  */     
$extraWhere = " 1=1 $ambiente $kilos $metros";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
