<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Obtener la fecha máxima-----------------------
include "../../../variables.php";
//Fin Obtener la fecha máxima-----------------------

$id_recorrido = " AND 1 <> 1 ";

if (isset($_GET['action']) && $_GET['action']=="recorrido") {

    if($_GET['id_recorrido']!=""){
        $recorrido = $_GET['id_recorrido'];
        $id_recorrido = "AND idrecorrido = $recorrido ";
    }
    
}
 
// DB table to use
$table = 'recorrido';
 
// Table's primary key
$primaryKey = 'idrecorrido';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(

    array( 'db' => 'categoria', 'dt' => 0),
    array( 'db' => 'negocio',
           'dt' => 1,
        'formatter' => function( $d, $row ) {

            return $d;
        }
    ),
    array( 'db'        => 'monto',
           'dt'        => 2,
           'formatter' => function( $d, $row ) {
                $monto='<p style="text-align: right;">$'.number_format($d, 0, ',', '.').'</p>';
                
                return $monto;
            }
    ),
    array( 'db' => 'comuna',
           'dt' => 3,
        'formatter' => function( $d, $row ) {

            return $d;
        }
    ),
    array( 'db' => 'ubicacion',
           'dt' => 4,
        'formatter' => function( $d, $row ) {

            return $d;
        }
    ),
    array( 'db' => 'observaciones',
           'dt' => 5,
        'formatter' => function( $d, $row ) {

            return $d;
        }
    )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";  */
$extraWhere = " 1=1 $id_recorrido ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
