<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
include "../../../variables.php";
// DB table to use
$table = 'vista_resumen_vehiculo';
 
// Table's primary key
$primaryKey = 'patente_vehiculo';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'patente_vehiculo', 'dt' => 0 ),
    array( 'db' => 'descripcion_vehiculo', 'dt' => 1 ),
    array( 'db' => 'unidad_vehiculo', 'dt' => 2 ),
    array( 'db' => 'neumatico_vehiculo', 'dt' => 3 ),
    array( 'db' => 'combustible_vehiculo', 'dt' => 4 ),
    array( 'db'        => 'kilometraje_vehiculo',
           'dt'        => 5,
           'formatter' => function( $d, $row ) {
                $kilometraje='<p>'.number_format($d, 0, ',', '.').'</p>';
                
                return $kilometraje;
            }
    )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";  */     
$extraWhere = " 1=1 ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
