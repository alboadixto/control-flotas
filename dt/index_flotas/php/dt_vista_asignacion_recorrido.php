<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
include "../../../variables.php";
// DB table to use
$table = 'vista_asignacion_recorrido';
 
// Table's primary key
$primaryKey = 'idrecorrido';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db'        => 'idrecorrido',
           'dt'        => 0,
           'formatter' => function( $d, $row ) {
            $asignar='<button type="button" class="btn btn-outline-success btn-sm" title="Asignar recorrido" data-toggle="modal" data-target="#modal_asignaciones" onclick="btn_asignar_tabla_recorrido('.$d.');"><i class="fas fa-plus"></i></button>';
            $remover='';
            $asignado='';

            if ($row['patente']!=NULL) {
                $asignar='';
                $remover='<button type="button" class="btn btn-outline-danger btn-sm" title="Eliminar recorrido" onclick="eliminar_recorrido('.$d.');"><i class="fas fa-minus"></i></button>';
                $asignado='<span class="badge badge-info">'.$row['patente'].'</span>';
            }

                return $asignar." ".$remover." ".$asignado;
            }
    ),
    array( 'db' => 'n_recorrido', 'dt' => 1 ),
    array( 'db' => 'categoria', 'dt' => 2 ),
    array( 'db' => 'negocio', 'dt' => 3 ),
    array( 'db' => 'n_clientes', 'dt' => 4 ),
    array( 'db' => 'n_facturas', 'dt' => 5 ),
    array( 'db'        => 'monto',
           'dt'        => 6,
           'formatter' => function( $d, $row ) {
                $monto='<p style="text-align: right;">$'.number_format($d, 0, ',', '.').'</p>';
                
                return $monto;
            }
    ),
    array( 'db' => 'comuna', 'dt' => 7 ),
    array( 'db' => 'kilos', 'dt' => 8 ),
    array( 'db' => 'volumen_30', 'dt' => 9 ),
    array( 'db' => 'volumen_60', 'dt' => 10 ),
    array( 'db' => 'volumen_100', 'dt' => 11 ),
    array( 'db' => 'n_pallet', 'dt' => 12 ),
    array( 'db' => 'ubicacion', 'dt' => 13 ),
    array( 'db' => 'anden', 'dt' => 14 ),
    array( 'db' => 'jornada', 'dt' => 15 ),
    array( 'db' => 'observaciones', 'dt' => 16 ),
    array( 'db' => 'fecha_import', 'dt' => 17 ),
    array( 'db' => 'patente', 'dt' => 18)

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";  */     
$extraWhere = " 1=1 ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
