<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Obtener la fecha máxima-----------------------
include "../../../variables.php";
//Fin Obtener la fecha máxima-----------------------
 
// DB table to use
$table = 'vista_evaluacion_asignacion';
 
// Table's primary key
$primaryKey = 'id_recorrido';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(

    array( 'db' => 'id_recorrido',
           'dt' => 0,
        'formatter' => function( $d, $row ) {
        
            $finalizado='<span class="badge badge-pill badge-danger"><i class="fas fa-key"></i></span>';

            if($row["efectividad_evaluacion_vehiculo"] && $row["efectividad_evaluacion_conductor"]){
                
                $finalizado='<span class="badge badge-pill badge-success"><i class="fas fa-key"></i></span>';
                
                if($row["id_asistente"] && !$row["efectividad_evaluacion_asistente"]){
                    $finalizado='<span class="badge badge-pill badge-danger"><i class="fas fa-key"></i></span>';
                }
    
            }

            return $finalizado;
        }
    ),
    array( 'db' => 'n_recorrido', 'dt' => 1),
    array( 'db' => 'patente',
           'dt' => 2,
        'formatter' => function( $d, $row ) {
        
            $vehiculo="<button type=\"button\" class=\"btn btn-link\" title=\"Evaluar Vehículo\" data-toggle=\"modal\" data-target=\"#modal_asignaciones_eva_vehiculo\" onclick=\"eva_vehiculo('".$row["id_recorrido"]."','".$row["n_recorrido"]."','".$d."','".$row["id_vehiculo"]."');\"><i class=\"fas fa-truck\"></i>&nbsp; ".$d."</button>";
        
            $efectividad='';

            if($row["efectividad_evaluacion_vehiculo"]){

                switch ($row["efectividad_evaluacion_vehiculo"]) {
                    case ($row["efectividad_evaluacion_vehiculo"] <= 80):
                        $color='danger';
                        break;
                    
                    case ($row["efectividad_evaluacion_vehiculo"] <= 89):
                        $color='warning';
                        break;
                        
                    default:
                        $color='success';
                }
    
                $efectividad='<span class="badge badge-pill badge-'.$color.'">efec: '.$row["efectividad_evaluacion_vehiculo"].' %</span>';
            }

            return $vehiculo.$efectividad;
        }
    ),
    array( 'db'        => 'conductor',
           'dt'        => 3,
           'formatter' => function( $d, $row ) {

                $nombre="<button type=\"button\" class=\"btn btn-link\" title=\"Evaluar tripulacion\" data-toggle=\"modal\" data-target=\"#modal_asignaciones_eva_tripulacion\" onclick=\"eva_tripulacion('".$row["id_recorrido"]."','".$row["n_recorrido"]."','".utf8_encode($d)."','".$row["id_conductor"]."');\"><i class=\"fas fa-user-cog\"></i>&nbsp; ".utf8_encode($d)."</button>";
        
                $efectividad='';

                if($row["efectividad_evaluacion_conductor"]){

                    switch ($row["efectividad_evaluacion_conductor"]) {
                        case ($row["efectividad_evaluacion_conductor"] <= 80):
                            $color='danger';
                            break;
                        
                        case ($row["efectividad_evaluacion_conductor"] <= 89):
                            $color='warning';
                            break;
                            
                        default:
                            $color='success';
                    }
        
                    $efectividad='<span class="badge badge-pill badge-'.$color.'">efec: '.$row["efectividad_evaluacion_conductor"].' %</span>';
                }
                
                return $nombre.$efectividad;
            }
    ),
    array( 'db'        => 'asistente',
            'dt'        => 4,
            'formatter' => function( $d, $row ) {
                
                $nombre='';

                if($row["id_asistente"]){

                    $nombre="<button type=\"button\" class=\"btn btn-link\" title=\"Evaluar tripulacion\" data-toggle=\"modal\" data-target=\"#modal_asignaciones_eva_tripulacion\" onclick=\"eva_tripulacion('".$row["id_recorrido"]."','".$row["n_recorrido"]."','".utf8_encode($d)."','".$row["id_asistente"]."');\"><i class=\"fas fa-users-cog\"></i>&nbsp; ".utf8_encode($d)."</button>";

                }
        
                $efectividad='';

                if($row["efectividad_evaluacion_asistente"]){

                    switch ($row["efectividad_evaluacion_asistente"]) {
                        case ($row["efectividad_evaluacion_asistente"] <= 80):
                            $color='danger';
                            break;
                        
                        case ($row["efectividad_evaluacion_asistente"] <= 89):
                            $color='warning';
                            break;
                            
                        default:
                            $color='success';
                    }
        
                    $efectividad='<span class="badge badge-pill badge-'.$color.'">efec: '.$row["efectividad_evaluacion_asistente"].' %</span>';
                }
                
                return $nombre.$efectividad;
            }
    ),
    array( 'db' => 'fecha_import', 'dt' => 5 ),
    array( 'db' => 'efectividad_evaluacion_vehiculo', 'dt' => 6 ),
    array( 'db' => 'id_conductor', 'dt' => 7 ),
    array( 'db' => 'efectividad_evaluacion_conductor', 'dt' => 8 ),
    array( 'db' => 'id_asistente', 'dt' => 9 ),
    array( 'db' => 'efectividad_evaluacion_asistente', 'dt' => 10 ),
    array( 'db' => 'id_vehiculo', 'dt' => 11 )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";  */     
$extraWhere = " 1=1 AND fecha_import = '$fecha' ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
