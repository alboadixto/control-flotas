<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Obtener la fecha máxima-----------------------
include "../../../variables.php";
//Fin Obtener la fecha máxima-----------------------
 
// DB table to use
$table = 'recorrido';
 
// Table's primary key
$primaryKey = 'idrecorrido';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(

            array( 'db' => 'n_recorrido', 'dt' => 0 ),
            array( 'db'        => 'volumen_30',
                  'dt'        => 1,
                  'formatter' => function( $d, $row ) {
                     
                        $campo="volumen_30";
                        $volumen="<input class=\"form-control\" type=\"number\" value=\"".$d."\" id=\"".$campo."-".$row["idrecorrido"]."\" onchange=\"actualizar_volumen(".$row["idrecorrido"].",'".$campo."')\">";

                     return $volumen;
                  }
            ),
            array( 'db'        => 'volumen_60',
                  'dt'        => 2,
                  'formatter' => function( $d, $row ) {
                     
                        $campo="volumen_60";
                        $volumen="<input class=\"form-control\" type=\"number\" value=\"".$d."\" id=\"".$campo."-".$row["idrecorrido"]."\" onchange=\"actualizar_volumen(".$row["idrecorrido"].",'".$campo."')\">";

                     return $volumen;
                  }
            ),
            array( 'db'        => 'volumen_100',
                  'dt'        => 3,
                  'formatter' => function( $d, $row ) {
               
                        $campo="volumen_100";
                        $volumen="<input class=\"form-control\" type=\"number\" value=\"".$d."\" id=\"".$campo."-".$row["idrecorrido"]."\" onchange=\"actualizar_volumen(".$row["idrecorrido"].",'".$campo."')\">";

                     return $volumen;
                  }
            ),
            array( 'db' => 'idrecorrido', 'dt' => 4 )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";  */     
$extraWhere = " 1=1 AND fecha_import = '$fecha' ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
