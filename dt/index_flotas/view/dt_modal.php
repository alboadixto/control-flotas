<!-- modal finalizar asignación -->
<div class="modal fade" id="modal_finalizar_asignacion" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fas fa-question-circle"></i>&nbsp;&nbsp;¿Está seguro?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button> 
            </div>

            <div class="modal-body">

                <div class="container" >
                    <center><div id="loading_gif_finalizar_asignacion"></div></center>

                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                <h4 class="alert-heading">Vehículo</h4>
                                <div class="table-responsive">

                                    <table id="tabla_resumen_vehiculo" class="table table-top-campaign" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th>Patente</th>
                                                <th>Descripción</th>
                                                <th>Unidad</th>
                                                <th>Neumático</th>
                                                <th>Combustible</th>
                                                <th>Kilometro</th>
                                            </tr>
                                        </thead>
                                    </table>

                                </div>
                            </div>

                            <div class="alert alert-info" role="alert">
                                <h4 class="alert-heading">Recorrido</h4>
                                <div class="table-responsive">

                                    <table id="tabla_resumen_recorrido" class="table table-top-campaign" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th>Recorrido</th>
                                                <th>Categoria</th>
                                                <th>Negocio</th>
                                                <th>Monto</th>
                                                <th>Comuna</th>
                                                <th>Ubicación</th>
                                            </tr>
                                        </thead>
                                    </table>

                                </div>
                            </div>

                            <div class="alert alert-secondary" role="alert">
                                <h4 class="alert-heading">Tripulación</h4>
                                <div class="table-responsive">

                                    <table id="tabla_resumen_tripulacion" class="table table-top-campaign" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>R.U.T</th>
                                                <th>Zapato</th>
                                                <th>Pantalón</th>
                                                <th>Camisa</th>
                                                <th>Seguridad</th>
                                                <th>Llegada</th>
                                            </tr>
                                        </thead>
                                    </table>

                                </div>
                            </div>
                        </div>

                </div>

            </div>

            <div class="modal-footer">
                <button id="btn_ok_modal_finalizar_asignacion" type="button" class="btn btn-success"><i class="fas fa-check"></i> Confirmar
                </button>
                <button id="btn_cerrar_modal_finalizar_asignacion" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fas fa-times"></i> Cancelar
                </button> 
            </div>

        </div>
    </div>
</div>
<!-- end modal finalizar asignación -->

<!-- modal carga datos -->
<div class="modal fade" id="modal_carga_datos" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fas fa-file-import"></i>&nbsp;&nbsp;Planilla de Carga</h5>
                <h5 class="modal-title"><i class="far fa-calendar-alt"></i>&nbsp;&nbsp;<?php echo date("d-m-Y");?></h5>  
            </div>

            <div class="modal-body">

                <div id="filer" class="row">
                  <div class="col-md-6 offset-md-3 mr-auto ml-auto">
                    <div class="input-group">
                        <input type="file" name="files[]" id="filer_input" accept=".csv">
                    </div>
                  </div>
                </div>

            <!-- TABLA CARGA DATOS-->
                <div class="container" >
                    <center><div id="loading_gif"></div></center>
                    <div id="datos_cargados" class="row">
                        <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> Datos Importados</h3>
                        <div class="table-responsive m-b-40">
                            <!-- ajax -->
                            <table id="tabla_carga_datos" class="table table-borderless table-striped table-earning" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>Recorrido</th>
                                        <th>Categoria</th>
                                        <th>Negocio</th>
                                        <th>Clientes</th>
                                        <th>Facturas</th>
                                        <th>Monto</th>
                                        <th>Comuna</th>
                                        <th>Kilos</th>
                                        <th>Volumen_30</th>
                                        <th>Volumen_60</th>
                                        <th>Volumen_100</th>
                                        <th>Pallet</th>
                                        <th>Ubicación</th>
                                        <th>Anden</th>
                                        <th>Jornada</th>
                                        <th>Observaciones</th>
                                        <th>Fecha</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- /ajax -->
                        </div>
                    </div>
                </div>
            <!-- TABLA CARGA DATOS-->
            </div>

            <div class="modal-footer">
                <button id="btn_cerrar_modal" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fas fa-times"></i> Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal carga datos -->
