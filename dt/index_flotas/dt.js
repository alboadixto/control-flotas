
var tabla_carga_datos;
var tabla_asignaciones;
var tabla_volumen;
var tabla_asignacion_recorridos;
/* 
var tabla_resumen_recorridos;
var tabla_resumen_tripulacion;

tabla_resumen_recorridos=$('#tabla_resumen_recorrido').DataTable({
    "processing": true,
    "serverSide": true,
    "searching": false,
    "paging":   false,
    "ordering": false,
    "info":     false,
    "ajax": "./dt/index_flotas/php/dt_resumen_recorrido.php",
    "order": [[ 0, "desc" ]],
    "language": {
        "decimal":        ",",
        "emptyTable":     "No hay registros",
        "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "infoEmpty":      "0 registros",
        "infoFiltered":   "(de un total de _MAX_ registros)",
        "infoPostFix":    "",
        "thousands":      ".",
        "lengthMenu":     "Mostrar _MENU_ registros",
        "loadingRecords": "Cargando...",
        "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
        "search":         "<i class='fas fa-search'></i>",
        "zeroRecords":    "No se encontraron registros",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    }
});

tabla_resumen_tripulacion=$('#tabla_resumen_tripulacion').DataTable({
    "processing": true,
    "serverSide": true,
    "searching": false,
    "paging":   false,
    "ordering": false,
    "info":     false,
    "ajax": "./dt/index_flotas/php/dt_resumen_tripulacion.php",
    "order": [[ 0, "asc" ]],
    "language": {
        "decimal":        ",",
        "emptyTable":     "No hay registros",
        "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "infoEmpty":      "0 registros",
        "infoFiltered":   "(de un total de _MAX_ registros)",
        "infoPostFix":    "",
        "thousands":      ".",
        "lengthMenu":     "Mostrar _MENU_ registros",
        "loadingRecords": "Cargando...",
        "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
        "search":         "<i class='fas fa-search'></i>",
        "zeroRecords":    "No se encontraron registros",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    }
}); 
*/

tabla_volumen=$('#tabla_volumen').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "./dt/index_flotas/php/dt_volumen.php",
    "order": [[ 0, "desc" ]],
    "language": {
        "decimal":        ",",
        "emptyTable":     "No hay registros",
        "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "infoEmpty":      "0 registros",
        "infoFiltered":   "(de un total de _MAX_ registros)",
        "infoPostFix":    "",
        "thousands":      ".",
        "lengthMenu":     "Mostrar _MENU_ registros",
        "loadingRecords": "Cargando...",
        "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
        "search":         "<i class='fas fa-search'></i>",
        "zeroRecords":    "No se encontraron registros",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    }
});

tabla_asignacion_recorridos=$('#tabla_asignacion_recorrido').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "./dt/index_flotas/php/dt_asignacion_recorrido.php",
    "order": [[ 0, "desc" ]],
    "language": {
        "decimal":        ",",
        "emptyTable":     "No hay registros",
        "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "infoEmpty":      "0 registros",
        "infoFiltered":   "(de un total de _MAX_ registros)",
        "infoPostFix":    "",
        "thousands":      ".",
        "lengthMenu":     "Mostrar _MENU_ registros",
        "loadingRecords": "Cargando...",
        "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
        "search":         "<i class='fas fa-search'></i>",
        "zeroRecords":    "No se encontraron registros",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    }
});

tabla_carga_datos=$('#tabla_carga_datos').DataTable({
    "processing": true,
    "serverSide": true,
    //"responsive": true,
    "ajax": "./dt/index_flotas/php/dt_cargar_datos.php",
    "order": [[ 16, "desc" ]],
    "language": {
        "decimal":        ",",
        "emptyTable":     "No hay registros",
        "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "infoEmpty":      "0 registros",
        "infoFiltered":   "(de un total de _MAX_ registros)",
        "infoPostFix":    "",
        "thousands":      ".",
        "lengthMenu":     "Mostrar _MENU_ registros",
        "loadingRecords": "Cargando...",
        "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
        "search":         "<i class='fas fa-search'></i>",
        "zeroRecords":    "No se encontraron registros",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    }
});

tabla_asignaciones=$('#tabla_asignaciones').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "./dt/index_flotas/php/dt_asignaciones.php",
    //"order": [[ 2, "desc" ]],
    "language": {
        "decimal":        ",",
        "emptyTable":     "No hay registros",
        "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "infoEmpty":      "0 registros",
        "infoFiltered":   "",
        "infoPostFix":    "",
        "thousands":      ".",
        "lengthMenu":     "Mostrar _MENU_ registros",
        "loadingRecords": "Cargando...",
        "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
        "search":         "<i class='fas fa-search'></i>",
        "zeroRecords":    "No se encontraron registros",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    }
});
