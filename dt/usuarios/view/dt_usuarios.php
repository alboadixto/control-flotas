<!-- TABLA USER-->
<h3 class="title-5 m-b-35"><i class="fas fa-table"></i> Usuarios</h3>
    <div class="row m-t-30">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="table-responsive m-b-40">

            <!-- ajax -->
            <table id="tabla_usuarios" class="table table-borderless table-striped table-earning" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Nombre</th>
                        <th>RUT</th>
                        <th>E-mail</th>
                        <th>Empresa</th>
                    </tr>
                </thead>
            </table>
            <!-- /ajax -->

          </div>
        </div>
    </div>
<!-- TABLA USER-->
