<?php
//conexiones, conexiones everywhere
include "../../../config/config.php";//Contiene funcion que conecta a la base de datos

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
 
// DB table to use
$table = 'usuarios';
 
// Table's primary key
$primaryKey = 'idusuarios';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    
    array( 'db'        => 'idusuarios',
           'dt'        => 0,
           'formatter' => function( $d, $row ) {
                $eliminar='<button type="button" class="btn btn-outline-danger btn-sm" title="Eliminar usuario" onclick="eliminar_usuario('.$d.');"><i class="fas fa-user-minus"></i></button>';
                if ($d==1) {
                    $eliminar="";
                }
                return $eliminar;
            }
    ),
    array( 'db'        => 'nombre_completo', 
           'dt'        => 1,
           'formatter' => function( $d, $row ) {
                $nombre=$d;
                if ($row["idusuarios"]==1) {
                    $nombre='<span class="status--process">'.$d.'</span>';
                }
                return $nombre;
            }
    ),
    array( 'db'        => 'rut_usuario',  
           'dt'        => 2,
           'formatter' => function( $d, $row ) {
                $rut=$d;
                if ($row["idusuarios"]==1) {
                    $rut='<span class="status--process">'.$d.'</span>';
                }
                return $rut;
            }
    ),
    array( 'db'        => 'correo',
           'dt'        => 3,
           'formatter' => function( $d, $row ) {
                $correo='<span class="block-email">'.$d.'</span>';
                if ($row["idusuarios"]==1) {
                    $correo='<span class="block-email status--process">'.$d.'</span>';
                }
                return $correo;
            }
    ),
    array( 'db'        => 'empresa', 
           'dt'        => 4,
           'formatter' => function( $d, $row ) {
                $empresa=$d;
                if ($row["idusuarios"]==1) {
                    $empresa='<span class="status--process">'.$d.'</span>';
                }
                return $empresa;
            }
    )

);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db'   => DB_NAME,
    'host' => DB_HOST
); 

/*$joinQuery = "FROM {$table} ";    */   
$extraWhere = " 1=1 ";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('../../../vendor/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $extraWhere )
);
