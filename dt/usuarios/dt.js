var tabla_usuarios;

tabla_usuarios=$('#tabla_usuarios').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "./dt/usuarios/php/dt_usuarios.php",
    "order": [[ 0, "desc" ]],
    "language": {
                "decimal":        ",",
                "emptyTable":     "No hay registros",
                "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                "infoEmpty":      "Mostrando 0 al 0 de 0 registros",
                "infoFiltered":   "(de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ".",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
            "processing":     '<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...',
                "search":         "<i class='fas fa-search'></i>",
                "zeroRecords":    "No se encontraron registros",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
    });
