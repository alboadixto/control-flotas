<?php

  session_start();
  include "config/config.php";

  if (!isset($_SESSION['user_id']) && $_SESSION['user_id']==null || $_SESSION['user_id']!=1) {
      header("location: config/index.php");
  }

  include "head.php";

?>

<section class="p-t-20">
  
  <!-- CONTAINER -->
  <div class="container">

    <!-- form search -->
    <!-- 
    <form class="container" role="form">
      <div class="form-group row">
        <input type="text" class="au-input au-input--xl" id="q" placeholder="Dato a buscar" onkeyup='load(1);'>
        <button type="button" class="au-btn--submit" onclick='load(1);'>
            <i class="zmdi zmdi-search"></i>
          </button>
      </div>
    </form>   
    -->
    <!-- end form search -->

    <!-- Formulario ingresar usuario-->
    <div class="col-lg-12">
      <div class="card">
      
          <div class="card-header"><i class="fas fa-user-edit"></i><strong> Formulario</strong></div>
          
          <div class="card-body card-block">
              <form id="frmUsuario" name="frmUsuario">
                  
                  <div class="row form-group">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input type="text" id="username" name="username" placeholder="Nombre" class="form-control" autofocus>
                      </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-4">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-credit-card"></i>
                        </div>
                        <input type="text" id="rut" name="rut" placeholder="R.U.T (Ej: 12345678k)" class="form-control">
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-4">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-envelope"></i>
                        </div>
                        <input type="email" id="email" name="email" placeholder="E-mail" class="form-control">
                      </div>
                    </div>
                  </div>

                  <div class="row form-group">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-asterisk"></i>
                        </div>
                        <input type="password" id="password" name="password" placeholder="Contraseña" class="form-control">
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-4">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-briefcase"></i>
                        </div>
                        <input type="text" id="empresa" name="empresa" placeholder="Empresa" class="form-control">
                      </div>
                    </div>
                  </div>

              </form>
          </div>

          <div class="card-footer">
            <button type="button" id="btn_agregar_usuario" class="au-btn au-btn-icon au-btn--green au-btn--small pull-right" title="Agregar usuario">
              <i class="fas fa-user-plus"></i>
            </button>
          </div>

      </div>
    </div>
    <!-- Fin Formulario ingresar usuario-->

    <?php 

      include "dt/usuarios/view/dt_usuarios.php" 

    ?>

  </div>
  <!-- FIN CONTAINER -->

</section>

<?php 
  
  include "footer.php" 

?>

<script type="text/javascript" src="js/usuarios.js"></script>
<script type="text/javascript" src="dt/usuarios/dt.js"></script>

