$(document).ready(function(){
  
  //Formatear numero con separadores de miles./////////////
  $("#rendimiento_retorno,#kilometraje_retorno,#rechazos_retorno,#dinero_retorno,#dinero_sobrante_retorno,#total_dinero_retorno,#estacionamiento_retorno,#peaje_retorno,#otros_retorno,#diferencia_dinero_retorno").on({
    "focus": function(event) {
      $(event.target).select();
    },
    "keyup": function(event) {
      $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
      });
    }
  });
  //FIN Formatear numero con separadores de miles./////////////

});

// se llama desde ajax/retorno.php
function select_unidad(id_vehiculo,fecha,estado_retorno) {
   //console.log(estado_retorno);
  $('#unidad_ppu_retorno').empty();
  $.ajax({
    type: "GET",
    url: './ajax/retorno.php?action=select_unidad&id_vehiculo='+id_vehiculo+'&fecha='+fecha+'&estado_retorno='+estado_retorno,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_retorno').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_retorno").html('');
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        //console.log(array[0]);
        if(!array[0]["asignado"]){
          $("#unidad_ppu_retorno").append('<option value="">- Seleccione -</option>');
        }

        $.each(array,function(key, data) {
          $("#unidad_ppu_retorno").append('<option value='+data.id+'>'+data.nombre+'</option>');
        });

      } else {
        $("#unidad_ppu_retorno").append('<option value="">' +response.message+'</option>');
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

// se llama desde ajax/retorno.php
function select_combustible(id_vehiculo,fecha,estado_retorno) {
  $('#combustible_ppu_retorno').empty();
  $.ajax({
    type: "GET",
    url: './ajax/retorno.php?action=select_combustible&id_vehiculo='+id_vehiculo+'&fecha='+fecha+'&estado_retorno='+estado_retorno,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_retorno').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_retorno").html('');
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        //console.log(array[0]);
        if(!array[0]["asignado"]){
          $("#combustible_ppu_retorno").append('<option value="">- Seleccione -</option>');
        }

        $.each(array,function(key, data) {
          $("#combustible_ppu_retorno").append('<option value='+data.id+'>'+data.nombre+'</option>');
        });

      } else {
        $("#combustible_ppu_retorno").append('<option value="">' +response.message+'</option>');
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

// se llama desde dt/index_flotas/php/dt_asignaciones.php
function eva_recorrido(patente,id_vehiculo,fecha,estado_retorno) {
   $("#id_vehiculo_retorno").val(id_vehiculo);
   $("#vehiculo_retorno").text(patente);
   select_unidad(id_vehiculo,fecha,estado_retorno);
   select_combustible(id_vehiculo,fecha,estado_retorno);
 }