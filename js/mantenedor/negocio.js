$(document).ready(function(){
  
  posee_contrato();
    
});

function posee_contrato() {
  $("#plazo_contrato_form_negocio").attr("readonly","readonly");
  
  var id = $("#posee_contrato_form_negocio").val();
  
  if(id !="" && id == 1){
    $("#plazo_contrato_form_negocio").removeAttr("readonly");
  }
  
}

function limpiar_formulario_negocio(){
  $('#frm_mantenedor_negocio').trigger("reset");
} 

$('#btn_limpiar_mantenedor_negocio').click(function(){
  limpiar_formulario_negocio();
});

$('#btn_mantenedor_negocio').click(function(){

  if ($('#fecha_operacion_form_negocio').val()=='') {
      alertify.error('<center><i class="fas fas fa-times"></i>&nbsp;&nbsp;Fecha operación</center>');
      return false;
  }

  document.getElementById('btn_mantenedor_negocio').disabled = true; 
  var frm = $('#frm_mantenedor_negocio').serialize();

  $.ajax({
  type: "GET",
  url: './ajax/mantenedor/negocio.php',
  data: '&action=btn_mantenedor_negocio&'+frm,
      beforeSend: function (objeto) {
        $('#loading_gif_mantenedor_negocio').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
        $("#loading_gif_mantenedor_negocio").html('');
        response = JSON.parse(datos);
        if (response.success) {
          limpiar_formulario_negocio();
          alertify.success("<i class='fa fa-check'></i> " + response.message + "");
          mantenedor_tabla_negocio.ajax.reload();

        } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
        document.getElementById('btn_mantenedor_negocio').disabled = false;
      }
  });

});

function eliminar_negocio(id_negocio) {
   alertify.confirm('Eliminar Negocio', '¿Está seguro?.', function(){ 
      $.ajax({
      type: "GET",
      url: './ajax/mantenedor/negocio.php',
      data: '&action=eliminar_negocio&id_negocio='+id_negocio,
      beforeSend: function (objeto) {
      },
      success: function (datos) {
         response = JSON.parse(datos);
         if (response.success) {
            mantenedor_tabla_negocio.ajax.reload();
         } else {
            alertify.error('<i class="fa fa-times"></i>' +response.message+'');
         }
      }
      });
   }
   , function(){ alertify.error('Cancelado')});
 }

 function btn_asignar_ppu_negocio(id_negocio) {
   var url = './dt/mantenedor/php/dt_asignacion_ppu_negocio.php?action=btn_asignar_ppu_negocio&id_negocio='+id_negocio;
   asignacion_ppu_negocio.ajax.url(url).load();

   var url2 = './dt/mantenedor/php/dt_asignacion_no_ppu_negocio.php?action=btn_asignar_ppu_negocio&id_negocio='+id_negocio;
   asignacion_no_ppu_negocio.ajax.url(url2).load();
 }

 function accion_ppu(id_vehiculo, id_negocio, nombre_accion) {
   
   if(nombre_accion=="asignar"){
      document.getElementById('btn_asignar-'+id_vehiculo).disabled = true; 
   }

      $.ajax({
      type: "GET",
      url: './ajax/mantenedor/negocio.php',
      data: '&action='+nombre_accion+'_ppu&id_vehiculo='+id_vehiculo+'&id_negocio='+id_negocio,
      beforeSend: function (objeto) {
      },
      success: function (datos) {
         response = JSON.parse(datos);
         if (response.success) {
            asignacion_no_ppu_negocio.ajax.reload();
            asignacion_ppu_negocio.ajax.reload();
         } else {
            alertify.error('<i class="fa fa-times"></i>' +response.message+'');
         }
         if(nombre_accion=="asignar"){
            document.getElementById('btn_asignar-'+id_vehiculo).disabled = false;
         }
      } 
      });

 }

 function btn_documento_negocio(id_negocio) {

   document.getElementById("input_id_negocio").value = id_negocio;
   var url = './dt/mantenedor/php/dt_documento_negocio.php?action=filtro_negocio&id_negocio='+id_negocio;
   tabla_documento_negocio.ajax.url(url).load();
   
}

///////////////// funcion para subir el documento /////////////////
$('#btn_documento_negocio').on('click', function() {
   let data = new FormData($("#frm_documento_negocio")[0]);
   var id_negocio = $("#input_id_negocio").val();

   $.ajax({
      url: './ajax/mantenedor/documento_negocio.php?id_negocio='+id_negocio+'&action=subir_documento_negocio',
      type: 'POST',
      data: data,
      processData: false,
      contentType: false,
      beforeSend: function (objeto) {
         $('#loading_gif_modal_documento_negocio').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
         $('#loading_gif_modal_documento_negocio').html('');
         response = JSON.parse(datos);
         if (response.success) {
            tabla_documento_negocio.ajax.reload();
         } else {
            alertify.error('<i class="fa fa-times"></i>' +response.message+'');
         }
      } 
   });
 }); 
///////////////// fin funcion para subir el documento /////////////////

 function eliminar_documento_negocio(id_documento_negocio) {
   alertify.confirm('Eliminar Documento', '¿Está seguro?.', function(){ 
      $.ajax({
         type: "GET",
         url: './ajax/mantenedor/documento_negocio.php?',
         data: '&action=eliminar_documento_negocio&id_documento_negocio='+id_documento_negocio,
         beforeSend: function (objeto) {
            $('#loading_gif_modal_documento_negocio').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
         },
         success: function (datos) {
            $('#loading_gif_modal_documento_negocio').html('');
            response = JSON.parse(datos);
            if (response.success) {
               tabla_documento_negocio.ajax.reload();
            } else {
               alertify.error('<i class="fa fa-times"></i>' +response.message+'');
            }
         } 
      });
   }, function(){ alertify.error('Cancelado')});
   
}

function descargar_documento_negocio(id_documento_negocio) {
   var url = './ajax/mantenedor/documento_negocio.php?action=descargar_documento_negocio&id_documento_negocio='+id_documento_negocio;

 	//window.location.href=url;
    window.open(
		url,
		'_blank' // <- This is what makes it open in a new window.
	);
   
}