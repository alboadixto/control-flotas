$(document).ready(function(){

//Formatear numero con separadores de miles./////////////
$("#valor_adquisicion_form_vehiculo,#cuota_leasing_form_vehiculo,#ano_form_vehiculo,#capacidad_form_vehiculo,#volumen_form_vehiculo,#combustible_form_vehiculo,#kilometraje_form_vehiculo,#p_circulacion_monto_form_vehiculo,#soap_monto_form_vehiculo").on({
   "focus": function(event) {
     $(event.target).select();
   },
   "keyup": function(event) {
      $(event.target).val(function(index, value) {
       return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
      });
   }
});
//FIN Formatear numero con separadores de miles./////////////
  
   select_forma_adquisicion();
   forma_adquisicion();
   select_marca_vehiculo();
   select_ambiente_vehiculo();
   select_combustible_vehiculo();
 
});

function select_forma_adquisicion() {
   $('#forma_adquisicion_form_vehiculo').empty();

   $.ajax({
     type: "GET",
     url: './ajax/mantenedor/vehiculo.php?action=select_forma_adquisicion',
     beforeSend: function (objeto) {
     },
     success: function (datos) {
       response = JSON.parse(datos);
       if (response.success) {
         var array = response.message;
         $("#forma_adquisicion_form_vehiculo").append('<option value="">Seleccione</option>');
         $("#forma_adquisicion_form_vehiculo").append(' <optgroup label="---------">');
       
         $.each(array,function(key, ppu) {
           $("#forma_adquisicion_form_vehiculo").append('<option value='+ppu.id+'>'+ppu.nombre+'</option>');
         });
 
         $("#forma_adquisicion_form_vehiculo").append('</optgroup>');
 
       } else {
         alertify.error('<i class="fa fa-times"></i>' +response.message+'');
       }
     }
   });

}

function forma_adquisicion() {
$("#banco_leasing_form_vehiculo,#cuota_leasing_form_vehiculo,#fecha_leasing_form_vehiculo").attr("readonly","readonly");

   var id = $("#forma_adquisicion_form_vehiculo").val();

   if(id !="" && id == 2){
      $("#banco_leasing_form_vehiculo,#cuota_leasing_form_vehiculo,#fecha_leasing_form_vehiculo").removeAttr("readonly");
   }

}

function limpiar_formulario_vehiculo(){
   $('#frm_mantenedor_vehiculo').trigger("reset");
} 

$('#btn_limpiar_mantenedor_vehiculo').click(function(){
   limpiar_formulario_vehiculo();
});

$('#btn_mantenedor_vehiculo').click(function(){

   if ($('#fecha_adquisicion_form_vehiculo').val()=='') {
      alertify.error('<center><i class="fas fas fa-times"></i>&nbsp;&nbsp;Fecha adquisición</center>');
      return false;
   }

   document.getElementById('btn_mantenedor_vehiculo').disabled = true; 
   var frm = $('#frm_mantenedor_vehiculo').serialize();

   $.ajax({
   type: "GET",
   url: './ajax/mantenedor/vehiculo.php',
   data: '&action=btn_mantenedor_vehiculo&'+frm,
      beforeSend: function (objeto) {
         $('#loading_gif_mantenedor_vehiculo').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
         $("#loading_gif_mantenedor_vehiculo").html('');
         response = JSON.parse(datos);
         if (response.success) {
         limpiar_formulario_vehiculo();
         alertify.success("<i class='fa fa-check'></i> " + response.message + "");
         mantenedor_tabla_vehiculo.ajax.reload();

         } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
         document.getElementById('btn_mantenedor_vehiculo').disabled = false;
      }
   });

});

function eliminar_vehiculo(id_vehiculo) {
   alertify.confirm('Eliminar Vehículo', '¿Está seguro?.', function(){ 
      $.ajax({
      type: "GET",
      url: './ajax/mantenedor/vehiculo.php',
      data: '&action=eliminar_vehiculo&id_vehiculo='+id_vehiculo,
      beforeSend: function (objeto) {
      },
      success: function (datos) {
         response = JSON.parse(datos);
         if (response.success) {
               mantenedor_tabla_vehiculo.ajax.reload();
         } else {
            alertify.error('<i class="fa fa-times"></i>' +response.message+'');
         }
      }
      });
   }
   , function(){ alertify.error('Cancelado')});
 }

 function select_ambiente_vehiculo() {
   $('#ambiente_form_vehiculo').empty();

   $.ajax({
     type: "GET",
     url: './ajax/mantenedor/vehiculo.php?action=select_ambiente_vehiculo',
     beforeSend: function (objeto) {
     },
     success: function (datos) {
       response = JSON.parse(datos);
       if (response.success) {
         var array = response.message;
         $("#ambiente_form_vehiculo").append('<option value="">Seleccione</option>');
         $("#ambiente_form_vehiculo").append(' <optgroup label="---------">');
       
         $.each(array,function(key, ppu) {
           $("#ambiente_form_vehiculo").append('<option value='+ppu.id+'>'+ppu.nombre+'</option>');
         });
 
         $("#ambiente_form_vehiculo").append('</optgroup>');
 
       } else {
         alertify.error('<i class="fa fa-times"></i>' +response.message+'');
       }
     }
   });

}

 function select_marca_vehiculo() {
   $('#marca_form_vehiculo').empty();

   $.ajax({
     type: "GET",
     url: './ajax/mantenedor/vehiculo.php?action=select_marca_vehiculo',
     beforeSend: function (objeto) {
     },
     success: function (datos) {
       response = JSON.parse(datos);
       if (response.success) {
         var array = response.message;
         $("#marca_form_vehiculo").append('<option value="">Seleccione</option>');
         $("#marca_form_vehiculo").append(' <optgroup label="---------">');
       
         $.each(array,function(key, ppu) {
           $("#marca_form_vehiculo").append('<option value='+ppu.id+'>'+ppu.nombre+'</option>');
         });
 
         $("#marca_form_vehiculo").append('</optgroup>');
 
       } else {
         alertify.error('<i class="fa fa-times"></i>' +response.message+'');
       }
     }
   });

}

function select_combustible_vehiculo() {
   $('#combustible_form_vehiculo').empty();

   $.ajax({
     type: "GET",
     url: './ajax/mantenedor/vehiculo.php?action=select_combustible_vehiculo',
     beforeSend: function (objeto) {
     },
     success: function (datos) {
       response = JSON.parse(datos);
       if (response.success) {
         var array = response.message;
         $("#combustible_form_vehiculo").append('<option value="">Seleccione</option>');
         $("#combustible_form_vehiculo").append(' <optgroup label="---------">');
       
         $.each(array,function(key, ppu) {
           $("#combustible_form_vehiculo").append('<option value='+ppu.id+'>'+ppu.nombre+'</option>');
         });
 
         $("#combustible_form_vehiculo").append('</optgroup>');
 
       } else {
         alertify.error('<i class="fa fa-times"></i>' +response.message+'');
       }
     }
   });

}