$(document).ready(function(){

   //Formatear numero con separadores de miles./////////////
   $("#monto_pautado_form_personal").on({
      "focus": function(event) {
        $(event.target).select();
      },
      "keyup": function(event) {
         $(event.target).val(function(index, value) {
          return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
         });
      }
   });
   //FIN Formatear numero con separadores de miles./////////////
     
      monto_pautado();
      select_tipo_empleado();
      select_tipo_contrato();
      select_tipo_licencia();
      select_afp();
      select_prevision();
   });

   function monto_pautado() {

      $("#monto_pautado_form_personal").attr("readonly","readonly");
      
      var id = $("#bonos_pautados_form_personal").val();
        
      if(id !="" && id == 1){
         $("#monto_pautado_form_personal").removeAttr("readonly");
      }
      
   }   

   function select_prevision() {
      $('#prevision_form_personal').empty();
   
      $.ajax({
        type: "GET",
        url: './ajax/mantenedor/personal.php?action=select_prevision',
        beforeSend: function (objeto) {
        },
        success: function (datos) {
          response = JSON.parse(datos);
          if (response.success) {
            var array = response.message;
            $("#prevision_form_personal").append('<option value="">Seleccione</option>');
            $("#prevision_form_personal").append(' <optgroup label="---------">');
          
            $.each(array,function(key, ppu) {
              $("#prevision_form_personal").append('<option value='+ppu.id+'>'+ppu.nombre+'</option>');
            });
    
            $("#prevision_form_personal").append('</optgroup>');
    
          } else {
            alertify.error('<i class="fa fa-times"></i>' +response.message+'');
          }
        }
      });
   
   }  

   function select_afp() {
      $('#afp_form_personal').empty();
   
      $.ajax({
        type: "GET",
        url: './ajax/mantenedor/personal.php?action=select_afp',
        beforeSend: function (objeto) {
        },
        success: function (datos) {
          response = JSON.parse(datos);
          if (response.success) {
            var array = response.message;
            $("#afp_form_personal").append('<option value="">Seleccione</option>');
            $("#afp_form_personal").append(' <optgroup label="---------">');
          
            $.each(array,function(key, ppu) {
              $("#afp_form_personal").append('<option value='+ppu.id+'>'+ppu.nombre+'</option>');
            });
    
            $("#afp_form_personal").append('</optgroup>');
    
          } else {
            alertify.error('<i class="fa fa-times"></i>' +response.message+'');
          }
        }
      });
   
   }  

   function select_tipo_licencia() {
      $('#tipo_licencia_form_personal').empty();
   
      $.ajax({
        type: "GET",
        url: './ajax/mantenedor/personal.php?action=select_tipo_licencia',
        beforeSend: function (objeto) {
        },
        success: function (datos) {
          response = JSON.parse(datos);
          if (response.success) {
            var array = response.message;
            $("#tipo_licencia_form_personal").append('<option value="">Seleccione</option>');
            $("#tipo_licencia_form_personal").append(' <optgroup label="---------">');
          
            $.each(array,function(key, ppu) {
              $("#tipo_licencia_form_personal").append('<option value='+ppu.id+'>'+ppu.nombre+'</option>');
            });
    
            $("#tipo_licencia_form_personal").append('</optgroup>');
    
          } else {
            alertify.error('<i class="fa fa-times"></i>' +response.message+'');
          }
        }
      });
   
   }  

   function select_tipo_contrato() {
      $('#tipo_contrato_form_personal').empty();
   
      $.ajax({
        type: "GET",
        url: './ajax/mantenedor/personal.php?action=select_tipo_contrato',
        beforeSend: function (objeto) {
        },
        success: function (datos) {
          response = JSON.parse(datos);
          if (response.success) {
            var array = response.message;
            $("#tipo_contrato_form_personal").append('<option value="">Seleccione</option>');
            $("#tipo_contrato_form_personal").append(' <optgroup label="---------">');
          
            $.each(array,function(key, ppu) {
              $("#tipo_contrato_form_personal").append('<option value='+ppu.id+'>'+ppu.nombre+'</option>');
            });
    
            $("#tipo_contrato_form_personal").append('</optgroup>');
    
          } else {
            alertify.error('<i class="fa fa-times"></i>' +response.message+'');
          }
        }
      });
   
   }
   
   function select_tipo_empleado() {
      $('#tipo_empleado_form_personal').empty();
   
      $.ajax({
        type: "GET",
        url: './ajax/mantenedor/personal.php?action=select_tipo_empleado',
        beforeSend: function (objeto) {
        },
        success: function (datos) {
          response = JSON.parse(datos);
          if (response.success) {
            var array = response.message;
            $("#tipo_empleado_form_personal").append('<option value="">Seleccione</option>');
            $("#tipo_empleado_form_personal").append(' <optgroup label="---------">');
          
            $.each(array,function(key, ppu) {
              $("#tipo_empleado_form_personal").append('<option value='+ppu.id+'>'+ppu.nombre+'</option>');
            });
    
            $("#tipo_empleado_form_personal").append('</optgroup>');
    
          } else {
            alertify.error('<i class="fa fa-times"></i>' +response.message+'');
          }
        }
      });
   
   }
   
   function limpiar_formulario_personal(){
      $('#frm_mantenedor_personal').trigger("reset");
   } 
   
   $('#btn_limpiar_mantenedor_personal').click(function(){
      limpiar_formulario_personal();
   });
   
   $('#btn_mantenedor_personal').click(function(){
   
      if ($('#rut_personal_form_personal').val()=='') {
         alertify.error('<center><i class="fas fas fa-times"></i>&nbsp;&nbsp;R.U.T</center>');
         return false;
      }

      let data = new FormData($("#frm_mantenedor_personal")[0]);
      document.getElementById('btn_mantenedor_personal').disabled = true; 
      var frm = $('#frm_mantenedor_personal').serialize();
   
      $.ajax({
      type: "POST",
      url: './ajax/mantenedor/personal.php?action=btn_mantenedor_personal&'+frm,
      data: data,
      processData: false,
      contentType: false,
         beforeSend: function (objeto) {
            $('#loading_gif_mantenedor_personal').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
         },
         success: function (datos) {
            $("#loading_gif_mantenedor_personal").html('');
            response = JSON.parse(datos);
            if (response.success) {
            limpiar_formulario_personal();
            alertify.success("<i class='fa fa-check'></i> " + response.message + "");
            mantenedor_tabla_personal.ajax.reload();
   
            } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
            document.getElementById('btn_mantenedor_personal').disabled = false;
         }
      });
   
   });
   
   function eliminar_personal(id_personal) {
      alertify.confirm('Eliminar Personal', '¿Está seguro?.', function(){ 
         $.ajax({
         type: "GET",
         url: './ajax/mantenedor/personal.php',
         data: '&action=eliminar_personal&id_personal='+id_personal,
         beforeSend: function (objeto) {
         },
         success: function (datos) {
            response = JSON.parse(datos);
            if (response.success) {
               mantenedor_tabla_personal.ajax.reload();
            } else {
               alertify.error('<i class="fa fa-times"></i>' +response.message+'');
            }
         }
         });
      }
      , function(){ alertify.error('Cancelado')});
   }

////////////////////// FOTO DE PERFIL SELECCIONADA ///////////////////////
  $(window).on("load",function(){
    $(function() {
      $('#foto_perfil_form_personal').change(function(e) {
          addImage(e); 
        });

        function addImage(e){
          var file = e.target.files[0],
          imageType = /image.*/;
        
          if (!file.type.match(imageType))
          return;
      
          var reader = new FileReader();
          reader.onload = fileOnload;
          reader.readAsDataURL(file);
        }
      
        function fileOnload(e) {
          var result=e.target.result;
          $('#imgSalida').attr("src",result);
        }
    });
  });
////////////////////// FIN FOTO DE PERFIL SELECCIONADA ///////////////////////