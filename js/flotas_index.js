var token_asistente = 0;

$(document).ready(function(){
  
  actualizar_ppu();
  actualizar_tripulacion_conductor();
  actualizar_tripulacion_asistente();

  //Switch asistente tripulación./////////////
  $('#switch_asistente_asignacion').on('change', function() {
    $("#switch_asistente_tripulacion_asignacion").toggle("slow");
  });
  //FIN Switch asistente tripulación./////////////
  
  //Formatear numero con separadores de miles./////////////
  $("#kilometraje_ppu_asignacion,#dinero_ppu_asignacion,#litros_cargados_ppu_asignacion").on({
    "focus": function(event) {
      $(event.target).select();
    },
    "keyup": function(event) {
      $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
      });
    }
  });
  //FIN Formatear numero con separadores de miles./////////////

  $('#btn_modal_eva_tripulacion').click(function(){

    if( $('#pantalon_tripulacion_asignacion').prop('checked') ) {
      $('#pantalon_tripulacion_asignacion').val("1");
    }
    if( $('#camisa_tripulacion_asignacion').prop('checked') ) {
      $('#camisa_tripulacion_asignacion').val("1");
    }
    if( $('#higiene_tripulacion_asignacion').prop('checked') ) {
      $('#higiene_tripulacion_asignacion').val("1");
    }
    if( $('#casco_tripulacion_asignacion').prop('checked') ) {
      $('#casco_tripulacion_asignacion').val("1");
    }
    if( $('#chaleco_tripulacion_asignacion').prop('checked') ) {
      $('#chaleco_tripulacion_asignacion').val("1");
    }
    if( $('#zapatos_tripulacion_asignacion').prop('checked') ) {
      $('#zapatos_tripulacion_asignacion').val("1");
    }
    if ($('#hora_tripulacion_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-clock"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }

    document.getElementById('btn_modal_eva_tripulacion').disabled = true; 
    var frm = $('#frmEvaTripulacion').serialize();
    //console.log(frm);
 
      $.ajax({
      type: "GET",
      url: "./ajax/flotas_index.php",
      data: '&action=btn_modal_eva_tripulacion&'+frm,
      beforeSend: function (objeto) {
        $('#loading_gif_modal_eva_tripulacion').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
        $("#loading_gif_modal_eva_tripulacion").html('');
        response = JSON.parse(datos);
        if (response.success) {
          alertify.success("<i class='fa fa-check'></i> " + response.message + "");
          document.getElementById("btn_modal_cancelar_eva_tripulacion").click();
          tabla_asignaciones.ajax.reload();

        } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
        document.getElementById('btn_modal_eva_tripulacion').disabled = false;
      }
    });

  });

  $('#btn_modal_eva_vehiculo').click(function(){

    if ($('#unidad_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-shower"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    if ($('#presion_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-truck-monster"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }

    if ($('#agua_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-water"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }

    if ($('#aceite_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-oil-can"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }

    if ($('#combustible_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-gas-pump"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }

    if ($('#kilometraje_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-route"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }

    if ($('#dinero_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-money-bill"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }

    if ($('#litros_cargados_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-gas-pump"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }

    document.getElementById('btn_modal_eva_vehiculo').disabled = true; 
    var frm = $('#frmEvaVehiculo').serialize();
 
    $.ajax({
      type: "GET",
      url: "./ajax/flotas_index.php",
      data: '&action=btn_modal_eva_vehiculo&'+frm,
      beforeSend: function (objeto) {
        $('#loading_gif_modal_eva_vehiculo').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
        $("#loading_gif_modal_eva_vehiculo").html('');
        response = JSON.parse(datos);
        if (response.success) {
          alertify.success("<i class='fa fa-check'></i> " + response.message + "");
          document.getElementById("btn_modal_cancelar_eva_vehiculo").click();
          tabla_asignaciones.ajax.reload();

        } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
        document.getElementById('btn_modal_eva_vehiculo').disabled = false;
      }
    });

  });

  $('#btn_modal_asignar_recorrido').click(function(){

    token_asistente = 0;

    if ($('#ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-truck"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    if ($('#conductor_tripulacion_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-user-cog"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }

    if($('#switch_asistente_tripulacion_asignacion').css('display') != 'none'){

      token_asistente = 1;

      if ($('#asistente_tripulacion_asignacion').val()=='') {
        alertify.error('<center><i class="fas fa-users-cog"></i>&nbsp;&nbsp;No definido</center>');
        return false;
      }
    }

    document.getElementById('btn_modal_asignar_recorrido').disabled = true; 
    var frm = $('#frmAsignarRecorrido').serialize();
 
    $.ajax({
      type: "GET",
      url: "./ajax/flotas_index.php",
      data: '&action=btn_modal_asignar_recorrido&'+frm+'&token_asistente='+token_asistente,
      beforeSend: function (objeto) {
        $('#loading_gif_modal_asignaciones').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
        $("#loading_gif_modal_asignaciones").html('');
        response = JSON.parse(datos);
        if (response.success) {
          insertar_evaluacion_vehiculo();
          actualizar_evaluacion_vehiculo();
          insertar_evaluacion_trabajador(token_asistente);
          actualizar_evaluacion_trabajador(token_asistente);
          alertify.success("<i class='fa fa-check'></i> " + response.message + "");
          document.getElementById("btn_modal_cancelar_recorrido").click();
          $("#frmAsignarRecorrido").trigger('reset');
          document.getElementById("switch_asistente_tripulacion_asignacion").style.display = "none";
          tabla_asignacion_recorridos.ajax.reload();
          tabla_asignaciones.ajax.reload();

        } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
        document.getElementById('btn_modal_asignar_recorrido').disabled = false;
      }
    });

  });

/*   $('#btn_ok_modal_finalizar_asignacion').click(function(){
    $("#ppu_asignacion").removeAttr('disabled');
    var frm = $('#frmAsignacion').serialize();
    $("#ppu_asignacion").attr("disabled", "disabled");
    //console.log(frm);
    $.ajax({
      type: "GET",
      url: "./ajax/flotas_index.php",
      data: '&action=btn_ok_modal_finalizar_asignacion&'+frm+'&token_asistente='+token_asistente,
      beforeSend: function (objeto) {
        $('#loading_gif_finalizar_asignacion').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
        $("#loading_gif_finalizar_asignacion").html('');
        response = JSON.parse(datos);
        if (response.success) {
          alertify.success("<i class='fa fa-check'></i> " + response.message + "");
          document.getElementById("btn_cerrar_modal_finalizar_asignacion").click();
          $("#Asignacion").hide("slow");
          document.getElementById('btn_tripulacion_asignacion').style.display = "none";
          document.getElementById('btn_ppu_asignacion').style.display = "inline";
          $("#ppu_asignacion,#unidad_ppu_asignacion,#presion_ppu_asignacion,#combustible_ppu_asignacion,#kilometraje_ppu_asignacion").removeAttr('disabled');
          $("#frmAsignacion").trigger('reset');
          //cambio en el tab activo
            $('#asignacion-nav-tripulacion-tab,#asignacion-nav-tripulacion').removeClass('active show');
            $('#asignacion-nav-tripulacion-tab').attr("aria-selected","false");
            $('#asignacion-nav-tripulacion-tab,#asignacion-nav-recorrido-tab').addClass('disabled');
            $('#asignacion-nav-vehiculo-tab,#asignacion-nav-vehiculo').addClass('active show');
            $('#asignacion-nav-vehiculo-tab').attr("aria-selected","true");
          //fin cambio en el tab activo
          actualizar_ppu();
          actualizar_tripulacion_conductor();
          actualizar_tripulacion_asistente();
          tabla_asignaciones.ajax.reload();
          $("#asignaciones").show("slow");
        } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
      }
    });
  }); */

/*   $('#btn_tripulacion_asignacion').click(function(){

    token_asistente = 0;

    if ($('#ropa_zapatos_conductor_tripulacion_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-shoe-prints"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    if ($('#ropa_pantalon_conductor_tripulacion_asignacion').val()=='') {
      alertify.error('<center><i class="fab fa-gitter"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    if ($('#ropa_camisa_conductor_tripulacion_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-tshirt"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    if ($('#seguridad_conductor_tripulacion_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-shield-alt"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    if ($('#llegada_conductor_tripulacion_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-clock"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    // Validar si el switch esta activo, de lo contrario no es obligatorio completar los campos del asistente
    if($('#switch_asistente_tripulacion_asignacion').css('display') != 'none'){

       token_asistente = 1;

      if ($('#ropa_zapatos_asistente_tripulacion_asignacion').val()=='') {
        alertify.error('<center><i class="fas fa-shoe-prints"></i>&nbsp;&nbsp;No definido</center>');
        return false;
      }
      if ($('#ropa_pantalon_asistente_tripulacion_asignacion').val()=='') {
        alertify.error('<center><i class="fab fa-gitter"></i>&nbsp;&nbsp;No definido</center>');
        return false;
      }
      if ($('#ropa_camisa_asistente_tripulacion_asignacion').val()=='') {
        alertify.error('<center><i class="fas fa-tshirt"></i>&nbsp;&nbsp;No definido</center>');
        return false;
      }
      if ($('#seguridad_asistente_tripulacion_asignacion').val()=='') {
        alertify.error('<center><i class="fas fa-shield-alt"></i>&nbsp;&nbsp;No definido</center>');
        return false;
      }
      if ($('#llegada_asistente_tripulacion_asignacion').val()=='') {
        alertify.error('<center><i class="fas fa-clock"></i>&nbsp;&nbsp;No definido</center>');
        return false;
      }
    }

    var frm = $('#frmAsignacion').serialize();
    $.ajax({
      type: "GET",
      url: "./ajax/flotas_index.php",
      data: '&action=btn_tripulacion_asignacion&'+frm+'&token_asistente='+token_asistente,
      beforeSend: function () {
        $('#loading_gif_finalizar_asignacion').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
        $("#loading_gif_finalizar_asignacion").html('');
        response = JSON.parse(datos);
        if (response.success) {
          tabla_resumen_recorridos.ajax.reload();
          tabla_resumen_tripulacion.ajax.reload();
    
          //$('#vehiculo_patente').text('Patente: '+document.getElementById("ppu_asignacion").options[0].text);
        } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
      }
    });

  }); */

  $('#btn_cancelar_siniestro,#btn_cancelar_asignacion').click(function(){
    $("#asignaciones").show("slow");
    $("#frmSiniestro,#Asignacion").hide("slow");
  });

  $('.select2').select2({
    allowClear:true,
    placeholder: '---- Seleccione ----',
    theme: 'bootstrap4',
    width: '100%',
  });

  $('#btn_nueva_asignacion').click(function(){
    tabla_asignacion_recorridos.ajax.reload();
    $("#asignaciones,#frmSiniestro").hide("slow");
    $("#Asignacion").show("slow");
  });

  $('#btn_limpiar_asignacion').click(function(){
    $.ajax({
      type: "GET",
      url: "./ajax/flotas_index.php",
      data: '&action=btn_limpiar',
      beforeSend: function (objeto) {
        $('#loading_gif_asignacion').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
        $("#loading_gif_asignacion").html('');
        response = JSON.parse(datos);
        if (response.success) {
            $("#ppu_asignacion,#unidad_ppu_asignacion,#presion_ppu_asignacion,#combustible_ppu_asignacion,#kilometraje_ppu_asignacion").removeAttr('disabled');
            tabla_asignacion_recorridos.ajax.reload();
            document.getElementById('btn_ppu_asignacion').style.display = "inline";
            document.getElementById('btn_recorrido_asignacion').style.display = "none";
            document.getElementById('btn_tripulacion_asignacion').style.display = "none";
          //cambio en el tab activo
            $('#asignacion-nav-tripulacion-tab,#asignacion-nav-recorrido-tab,#asignacion-nav-tripulacion,#asignacion-nav-recorrido').removeClass('active show');
            $('#asignacion-nav-tripulacion-tab,#asignacion-nav-recorrido-tab').attr("aria-selected","false");
            $('#asignacion-nav-tripulacion-tab,#asignacion-nav-recorrido-tab').addClass('disabled');
            $('#asignacion-nav-vehiculo-tab,#asignacion-nav-vehiculo').addClass('active show');
            $('#asignacion-nav-vehiculo-tab').attr("aria-selected","true");
          //fin cambio en el tab activo
        } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
      }
    });
  });

/*   $('#btn_ppu_asignacion').click(function(){
    if ($('#ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-truck"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    if ($('#unidad_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-shower"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    if ($('#presion_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-truck-monster"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    if ($('#combustible_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-gas-pump"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    if ($('#kilometraje_ppu_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-route"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
    var url = $('#frmAsignacion').serialize();
    $.ajax({
      type: "GET",
      url: "./ajax/flotas_index.php",
      data: url,
      beforeSend: function (objeto) {
        $('#loading_gif_asignacion').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
        $("#loading_gif_asignacion").html('');
        response = JSON.parse(datos);
        if (response.success) {
            $("#ppu_asignacion,#unidad_ppu_asignacion,#presion_ppu_asignacion,#combustible_ppu_asignacion,#kilometraje_ppu_asignacion").attr("disabled", "disabled");
            document.getElementById('btn_ppu_asignacion').style.display = "none";
            document.getElementById('btn_recorrido_asignacion').style.display = "inline";
            //fin cambio en el tab activo
              $('#asignacion-nav-vehiculo-tab,#asignacion-nav-vehiculo').removeClass('active show');
              $('#asignacion-nav-vehiculo-tab').attr("aria-selected","false");
              $('#asignacion-nav-recorrido-tab').removeClass('disabled');
              $('#asignacion-nav-recorrido-tab,#asignacion-nav-recorrido').addClass('active show');
              $('#asignacion-nav-recorrido-tab').attr("aria-selected","true");
            //fin cambio en el tab activo
            alertify.success("<i class='fa fa-check'></i> " + response.message + "");
        } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
      }
    });
  }); */

/*   $('#btn_recorrido_asignacion').click(function(){
    //quito el atributo de deshabilitado para poder tomar el idvehiculo (ppu_asignación)
    $("#ppu_asignacion").removeAttr('disabled');
    var frm = $('#frmAsignacion').serialize();
    $.ajax({
      type: "GET",
      url: "./ajax/flotas_index.php",
      data: '&action=btn_recorrido&'+frm,
      beforeSend: function (objeto) {
        $('#loading_gif_asignacion').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
        $("#loading_gif_asignacion").html('');
        response = JSON.parse(datos);
        if (response.success) {
            tabla_asignacion_recorridos.ajax.reload(); 
            document.getElementById('btn_recorrido_asignacion').style.display = "none";
            document.getElementById('btn_tripulacion_asignacion').style.display = "inline";
            //fin cambio en el tab activo
              $('#asignacion-nav-recorrido-tab,#asignacion-nav-recorrido').removeClass('active show');
              $('#asignacion-nav-recorrido-tab').attr("aria-selected","false");
              $('#asignacion-nav-tripulacion-tab').removeClass('disabled');
              $('#asignacion-nav-tripulacion-tab,#asignacion-nav-tripulacion').addClass('active show');
              $('#asignacion-nav-tripulacion-tab').attr("aria-selected","true");
            //fin cambio en el tab activo
            alertify.success("<i class='fa fa-check'></i> " + response.message + "");
        } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
          //vuelvo a dejar el atributo deshabilitado (ppu_asignación)
          $("#ppu_asignacion").attr("disabled", "disabled");
      }
    });
  }); */

});


///////////////// funcion para subir el documento en CSV /////////////////
  $("#filer_input").filer({
    enableApi: true,
    limit: null,//Maximum Limit of files. {null, Number}
    maxSize: 1,
    extensions: ["csv"],
    showThumbs: false,
    uploadFile: {
      url: './ajax/ajax_upload_carga_datos.php',
      data: null,
      type: 'POST',
      enctype: 'multipart/form-data',
      synchron: true,
      beforeSend: function(){
        $('#loading_gif').html('<img src="./images/ajax-loader_control_flotas.gif" width="100" height="100"> Cargando...'); 
      },
      success: function(data, itemEl, listEl, boxEl, newInputEl, inputEl, id){
        var parent = itemEl.find(".jFiler-jProgressBar").parent(),
          new_file_name = JSON.parse(data),
          filerKit = inputEl.prop("jFiler");

            filerKit.files_list[id].name = new_file_name;

        itemEl.find(".jFiler-jProgressBar").fadeOut("slow", function(){
          $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Correcto</div>").hide().appendTo(parent).fadeIn("slow");
        });

        $('#loading_gif').html(''); 
        $("#datos_cargados").show("slow");
        tabla_carga_datos.ajax.reload(); 
        tabla_volumen.ajax.reload(); 

      },
      error: function(el){
        var parent = el.find(".jFiler-jProgressBar").parent();
        el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
          $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
        });
      },
      statusCode: null,
      onProgress: null,
      onComplete: null
    },
    files: null,
    addMore: false,
    allowDuplicates: null,
    clipBoardPaste: true,
    excludeName: null,
    beforeRender: null,
    afterRender: null,
    beforeShow: null,
    beforeSelect: null,
    onSelect: null,
    afterShow: null,
    onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
      var filerKit = inputEl.prop("jFiler"),
            file_name = filerKit.files_list[id].name;

        $.post('./flotas_index.php');

    },
    onEmpty: null,
    options: null,
    dialogs: {
      alert: function(text) {
        return alert(text);
      },
      confirm: function (text, callback) {
        confirm(text) ? callback() : null;
      }
    },
    captions: {
            button: "<i class='fas fa-upload'>",
            feedback: "Elija el archivo a subir (.csv)",
            feedback2: "archivo elegido correctamente",
            drop: "Arrastre aquí para subir",
            removeConfirmation: "Esta seguro que desea eliminar este archivo?",
            errors: {
                filesLimit: "Solo se permite cargar {{fi-limit}} archivo.",
                filesType: "Solo se permite cargar archivo en formato CSV.",
                filesSize: "{{fi-name}} supera el peso maximo! Por favor subir archivo hasta 10 MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
                folderUpload: "You are not allowed to upload folders."
            }
    }
  });
///////////////// FIN funcion para subir el documento en CSV /////////////////

function siniestro_pass() {
  //un prompt
  alertify.prompt( 'Siniestro', 'Introduzca la contraseña del administrador:', ''
  , function(evt, value) { 
      if (evt && value != "") {
        $.ajax({
          type: "GET",
          url: "./ajax/flotas_index.php",
          data: '&siniestro_pass=' + value,
          beforeSend: function (objeto) {
            $('#loading_gif_siniestro').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
          },
          success: function (datos) {
            $("#loading_gif_siniestro").html('');
            response = JSON.parse(datos);
            if (response.success) {
              $("#asignaciones,#Asignacion").hide("slow");
              $("#frmSiniestro").show("slow");
            } else {
              alertify.error('<i class="fa fa-times"></i>&nbsp;&nbsp;' + response.message + '');
            }
          }
        });
      }else alertify.error('No ha introducido ningúna contraseña');
    }
  , function() { 
    alertify.error('Cancelado') }
  ).set('type', 'password');
}

function obtener_ppu_asignacion() {
  var idvehiculo = $('#ppu_asignacion').val();
  $("#estado_ppu_asignacion,#descripcion_ppu_asignacion").val('');
  //console.log('valor: ',idvehiculo);
  if (idvehiculo) {
    $.ajax({
      type: "GET",
      url: './ajax/flotas_index.php?idvehiculo=' + idvehiculo,
      beforeSend: function (objeto) {
        $('#loading_gif_modal_asignaciones').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
        $("#loading_gif_modal_asignaciones").html('');
        response = JSON.parse(datos);
        if (response.success) {
            $("#estado_ppu_asignacion").val(response.descripcion_estado);
            $("#descripcion_ppu_asignacion").val(response.descripcion_vehiculo);
        } else {
          alertify.error('<i class="fa fa-times"></i>' +response.message+'');
        }
      }
    });
  }
}

function btn_asignar_tabla_recorrido(idrecorrido,n_recorrido) {
  document.getElementById("btn_asignar_tabla_recorrido_id").value = idrecorrido;
  
} 

function eliminar_recorrido(idrecorrido) {
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=eliminar_recorrido&idrecorrido='+idrecorrido,
    beforeSend: function (objeto) {
      $('#loading_gif_asignacion').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_asignacion").html('');
      response = JSON.parse(datos);
      if (response.success) {
        tabla_asignacion_recorridos.ajax.reload(); 
        tabla_asignaciones.ajax.reload();
        //alertify.success("<i class='fa fa-check'></i> " + response.message + "");
      } else {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

function obtener_conductor_asistente_asignacion(conductor_asistente_tripulacion_asignacion,rut_conductor_asistente_tripulacion_asignacion) {
  var id = $('#'+conductor_asistente_tripulacion_asignacion+'').val();
  $("#"+rut_conductor_asistente_tripulacion_asignacion+"").val('');
  if (id) {
    $.ajax({
      type: "GET",
      url: './ajax/flotas_index.php?action=conductor_asistente&id=' + id,
      beforeSend: function (objeto) {
        $('#loading_gif_modal_asignaciones').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
      },
      success: function (datos) {
        $("#loading_gif_modal_asignaciones").html('');
        response = JSON.parse(datos);
        if (response.success) {
            $("#"+rut_conductor_asistente_tripulacion_asignacion+"").val(response.message);
        } else {
          alertify.error('<i class="fa fa-times"></i>' +response.message+'');
        }
      }
    }); 
  }
}

function actualizar_ppu() {
  $('#ppu_asignacion').empty();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=actualizar_ppu',
    beforeSend: function (objeto) {
    },
    success: function (datos) {
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        $("#ppu_asignacion").append('<option value="">---- Seleccione ----</option>');
        $.each(array,function(key, ppu) {
          $("#ppu_asignacion").append('<option value='+ppu.idvehiculos+'>'+ppu.patente+'</option>');
        }); 

      } else {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

function actualizar_tripulacion_conductor() {
  $('#conductor_tripulacion_asignacion').empty();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=actualizar_conductor',
    beforeSend: function (objeto) {
    },
    success: function (datos) {
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        $("#conductor_tripulacion_asignacion").append('<option value="">---- Seleccione ----</option>');
        $.each(array,function(key, conductor) {
          $("#conductor_tripulacion_asignacion").append('<option value='+conductor.id+'>'+conductor.nombre+'</option>');
        }); 

      } else {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

function actualizar_tripulacion_asistente() {
  $('#asistente_tripulacion_asignacion').empty();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=actualizar_asistente',
    beforeSend: function (objeto) {
    },
    success: function (datos) {
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        $("#asistente_tripulacion_asignacion").append('<option value="">---- Seleccione ----</option>');
        $.each(array,function(key, asistente) {
          $("#asistente_tripulacion_asignacion").append('<option value='+asistente.id+'>'+asistente.nombre+'</option>');
        }); 

      } else {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

// se llama desde dt/index_flotas/php/dt_volumen.php
function actualizar_volumen(idrecorrido, campo) {
  var valor = $('#'+campo+'-'+idrecorrido+'').val();
  if(valor){
    $.ajax({
      type: "GET",
      url: './ajax/flotas_index.php?action=actualizar_volumen&valor='+valor+'&idrecorrido='+idrecorrido+'&campo='+campo,
      beforeSend: function (objeto) {
      },
      success: function (datos) {
        response = JSON.parse(datos);
        if (response.success) {
          alertify.success("<i class='fa fa-check'></i> " + response.message + "");
  
        } else {
          alertify.error('<i class="fa fa-times"></i>' +response.message+'');
        }
        tabla_volumen.ajax.reload();
        tabla_asignacion_recorridos.ajax.reload();
      }
    });
  }
}

// se llama desde ajax/flotas_index.php
function select_unidad(id_recorrido) {
  $('#unidad_ppu_asignacion').empty();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=select_unidad&id_recorrido='+id_recorrido,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_eva_vehiculo').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_eva_vehiculo").html('');
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        //console.log(array[0]);
        if(!array[0]["asignado"]){
          $("#unidad_ppu_asignacion").append('<option value="">- Seleccione -</option>');
        }

        $.each(array,function(key, data) {
          $("#unidad_ppu_asignacion").append('<option value='+data.id+'>'+data.nombre+'</option>');
        });

      } else {
        $("#unidad_ppu_asignacion").append('<option value="">' +response.message+'</option>');
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

// se llama desde ajax/flotas_index.php
function select_presion(id_recorrido) {
  $('#presion_ppu_asignacion').empty();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=select_presion&id_recorrido='+id_recorrido,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_eva_vehiculo').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_eva_vehiculo").html('');
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        //console.log(array[0]);
        if(!array[0]["asignado"]){
          $("#presion_ppu_asignacion").append('<option value="">- Seleccione -</option>');
        }

        $.each(array,function(key, data) {
          $("#presion_ppu_asignacion").append('<option value='+data.id+'>'+data.nombre+'</option>');
        });

      } else {
        $("#presion_ppu_asignacion").append('<option value="">' +response.message+'</option>');
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

// se llama desde ajax/flotas_index.php
function select_agua(id_recorrido) {
  $('#agua_ppu_asignacion').empty();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=select_agua&id_recorrido='+id_recorrido,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_eva_vehiculo').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      response = JSON.parse(datos);
      if (response.success) {
        $("#loading_gif_modal_eva_vehiculo").html('');
        var array = response.message;
        //console.log(array[0]);
        if(!array[0]["asignado"]){
          $("#agua_ppu_asignacion").append('<option value="">- Seleccione -</option>');
        }

        $.each(array,function(key, data) {
          $("#agua_ppu_asignacion").append('<option value='+data.id+'>'+data.nombre+'</option>');
        });

      } else {
        $("#agua_ppu_asignacion").append('<option value="">' +response.message+'</option>');
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

// se llama desde ajax/flotas_index.php
function select_aceite(id_recorrido) {
  $('#aceite_ppu_asignacion').empty();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=select_aceite&id_recorrido='+id_recorrido,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_eva_vehiculo').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_eva_vehiculo").html('');
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        //console.log(array[0]);
        if(!array[0]["asignado"]){
          $("#aceite_ppu_asignacion").append('<option value="">- Seleccione -</option>');
        }

        $.each(array,function(key, data) {
          $("#aceite_ppu_asignacion").append('<option value='+data.id+'>'+data.nombre+'</option>');
        });

      } else {
        $("#aceite_ppu_asignacion").append('<option value="">' +response.message+'</option>');
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

// se llama desde ajax/flotas_index.php
function select_combustible(id_recorrido) {
  $('#combustible_ppu_asignacion').empty();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=select_combustible&id_recorrido='+id_recorrido,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_eva_vehiculo').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_eva_vehiculo").html('');
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        //console.log(array[0]);
        if(!array[0]["asignado"]){
          $("#combustible_ppu_asignacion").append('<option value="">- Seleccione -</option>');
        }

        $.each(array,function(key, data) {
          $("#combustible_ppu_asignacion").append('<option value='+data.id+'>'+data.nombre+'</option>');
        });

      } else {
        $("#combustible_ppu_asignacion").append('<option value="">' +response.message+'</option>');
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

// se llama desde ajax/flotas_index.php
function input_number_eva_vehiculo(id_recorrido) {
  $("#kilometraje_ppu_asignacion,#dinero_ppu_asignacion,#litros_cargados_ppu_asignacion").val('');
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=input_number_eva_vehiculo&id_recorrido='+id_recorrido,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_eva_vehiculo').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_eva_vehiculo").html('');
      response = JSON.parse(datos);
      if (response.success) {
        $("#kilometraje_ppu_asignacion").val(response.message[0]["kilometraje"].replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, "."));
        $("#dinero_ppu_asignacion").val(response.message[0]["asignacion_dinero"].replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, "."));
        $("#litros_cargados_ppu_asignacion").val(response.message[0]["litros_cargados"].replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, "."));

      } else {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}


// se llama desde dt/index_flotas/php/dt_asignaciones.php
function eva_vehiculo(id_recorrido,n_recorrido,ppu,id_vehiculo) {
  $("#eva_vehiculo_id_recorrido").val(id_recorrido);
  $("#eva_vehiculo_recorrido").text(n_recorrido);
  $("#eva_vehiculo_ppu").text(ppu);
  $("#eva_vehiculo_id_ppu").val(id_vehiculo);
  select_unidad(id_recorrido);
  select_presion(id_recorrido);
  select_agua(id_recorrido);
  select_aceite(id_recorrido);
  select_combustible(id_recorrido);
  input_number_eva_vehiculo(id_recorrido);
}

// se llama desde ajax/flotas_index.php
function select_hora_llegada(id_recorrido, id_tripulacion) {
  $('#hora_tripulacion_asignacion').empty();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=select_hora_llegada&id_recorrido='+id_recorrido+'&id_tripulacion='+id_tripulacion,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_eva_tripulacion').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_eva_tripulacion").html('');
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        //console.log(array[0]);
        if(!array[0]["asignado"]){
          $("#hora_tripulacion_asignacion").append('<option value="">- Seleccione -</option>');
        }

        $.each(array,function(key, data) {
          $("#hora_tripulacion_asignacion").append('<option value='+data.id+'>'+data.nombre+'</option>');
        });

      } else {
        $("#hora_tripulacion_asignacion").append('<option value="">' +response.message+'</option>');
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

// se llama desde ajax/flotas_index.php
function check_trabajador(id_recorrido, id_tripulacion) {
  $('#pantalon_tripulacion_asignacion,#camisa_tripulacion_asignacion,#higiene_tripulacion_asignacion,#casco_tripulacion_asignacion,#chaleco_tripulacion_asignacion,#zapatos_tripulacion_asignacion').prop('checked', false);
  
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=check_trabajador&id_recorrido='+id_recorrido+'&id_tripulacion='+id_tripulacion,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_eva_tripulacion').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_eva_tripulacion").html('');
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        //console.log(array[0]);
        if(array[0]["pantalon"]==1){
          $('#pantalon_tripulacion_asignacion').prop('checked', true);
        }
        if(array[0]["camisa"]==1){
          $('#camisa_tripulacion_asignacion').prop('checked', true);
        }
        if(array[0]["higiene"]==1){
          $('#higiene_tripulacion_asignacion').prop('checked', true);
        }
        if(array[0]["casco"]==1){
          $('#casco_tripulacion_asignacion').prop('checked', true);
        }
        if(array[0]["chaleco"]==1){
          $('#chaleco_tripulacion_asignacion').prop('checked', true);
        }
        if(array[0]["zapatos"]==1){
          $('#zapatos_tripulacion_asignacion').prop('checked', true);
        }

      } else {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

// se llama desde dt/index_flotas/php/dt_asignaciones.php
function eva_tripulacion(id_recorrido,n_recorrido,nombre_tripulacion,id_tripulacion) {
  $("#eva_tripulacion_id_recorrido").val(id_recorrido);
  $("#eva_tripulacion_id_tripulacion").val(id_tripulacion);
  $("#eva_tripulacion_recorrido").text(n_recorrido);
  $("#eva_tripulacion").text(nombre_tripulacion);
  select_hora_llegada(id_recorrido,id_tripulacion);
  check_trabajador(id_recorrido,id_tripulacion);
}

// se llama desde btn_modal_asignar_recorrido - flotas_index.js
function insertar_evaluacion_vehiculo(){
  var frm = $('#frmAsignarRecorrido').serialize();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=insertar_evaluacion_vehiculo&'+frm,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_asignaciones').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_asignaciones").html('');
      response = JSON.parse(datos);
      if (response.success == false) {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

function actualizar_evaluacion_vehiculo(){
  var frm = $('#frmAsignarRecorrido').serialize();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=actualizar_evaluacion_vehiculo&'+frm,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_asignaciones').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_asignaciones").html('');
    }
  });
}

// se llama desde btn_modal_asignar_recorrido - flotas_index.js
function insertar_evaluacion_trabajador(token_asistente){
  var frm = $('#frmAsignarRecorrido').serialize();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=insertar_evaluacion_trabajador&'+frm+'&token_asistente='+token_asistente,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_asignaciones').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_asignaciones").html('');
      response = JSON.parse(datos);
      if (response.success == false) {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

function actualizar_evaluacion_trabajador(token_asistente){
  var frm = $('#frmAsignarRecorrido').serialize();
  $.ajax({
    type: "GET",
    url: './ajax/flotas_index.php?action=actualizar_evaluacion_trabajador&'+frm+'&token_asistente='+token_asistente,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_asignaciones').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_asignaciones").html('');
    }
  });
}