$(document).ready(function(){
  
  select_ambiente();
  select_kilos();
  select_metros();

});

function select_ambiente() {
  $('#ppu_ambiente').empty();
  $.ajax({
    type: "GET",
    url: './ajax/ppu.php?action=select_ambiente',
    beforeSend: function (objeto) {
    },
    success: function (datos) {
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        $("#ppu_ambiente").append('<option value="">AMBIENTE</option>');
        $("#ppu_ambiente").append(' <optgroup label="-----">');
      
        $.each(array,function(key, ppu) {
          $("#ppu_ambiente").append('<option value='+ppu.nombre+'>'+ppu.nombre+'</option>');
        });

        $("#ppu_ambiente").append('</optgroup>');

      } else {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

function select_kilos() {
  $('#ppu_kilos').empty();
  $.ajax({
    type: "GET",
    url: './ajax/ppu.php?action=select_kilos',
    beforeSend: function (objeto) {
    },
    success: function (datos) {
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        $("#ppu_kilos").append('<option value="">KILOS</option>');
        $("#ppu_kilos").append(' <optgroup label="-----">');

        $.each(array,function(key, ppu) {
          $("#ppu_kilos").append('<option value='+ppu.nombre+'>'+ppu.nombre+'</option>');
        });

        $("#ppu_kilos").append('</optgroup>');

      } else {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

function select_metros() {
  $('#ppu_metros').empty();
  $.ajax({
    type: "GET",
    url: './ajax/ppu.php?action=select_metros',
    beforeSend: function (objeto) {
    },
    success: function (datos) {
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        $("#ppu_metros").append('<option value="">METROS</option>');
        $("#ppu_metros").append(' <optgroup label="-----">');

        $.each(array,function(key, ppu) {
          $("#ppu_metros").append('<option value='+ppu.nombre+'>'+ppu.nombre+'</option>');
        });

        $("#ppu_metros").append('</optgroup>');

      } else {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

function filtro() {
  var frm = $('#frmFiltros').serialize();
	var url = './dt/ppu/php/dt_ppu.php?action=filtro&'+frm;
  tabla_ppu.ajax.url(url).load();

}

function obtener_recorrido(){
  var id_recorrido = $('#btn_asignar_tabla_recorrido_id').val();
  var url = './dt/ppu/php/dt_select_recorrido.php?action=recorrido&id_recorrido='+id_recorrido;
  tabla_select_recorrido.ajax.url(url).load();
  $('#tabla_select_recorrido').show("fast");
}

function select_recorrido() {
  $('#btn_asignar_tabla_recorrido_id').empty();
  $.ajax({
    type: "GET",
    url: './ajax/ppu.php?action=select_recorrido',
    beforeSend: function (objeto) {
      $('#loading_gif_modal_asignacion_ppu').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_asignacion_ppu").html('');
      response = JSON.parse(datos);
      if (response.success) {
        var array = response.message;
        $("#btn_asignar_tabla_recorrido_id").append('<option value="">- Seleccione -</option>');
        $("#btn_asignar_tabla_recorrido_id").append(' <optgroup label="-----">');

        $.each(array,function(key, recorrido) {
          $("#btn_asignar_tabla_recorrido_id").append('<option value='+recorrido.id+'>'+recorrido.nombre+'</option>');
        });

        $("#btn_asignar_tabla_recorrido_id").append('</optgroup>');

      } else {
        alertify.error('<i class="fa fa-times"></i>' +response.message+'');
      }
    }
  });
}

function asignar_recorrido(id_ppu,patente) {
  $("#ppu_asignacion").val(id_ppu);
  $("#asignacion_ppu").text(patente);
  select_recorrido();
	var url = './dt/ppu/php/dt_asignaciones.php?action=ppu&id_ppu='+id_ppu;
  tabla_asignaciones.ajax.url(url).load();
  $('#tabla_select_recorrido').hide();
}

$('#btn_modal_asignar_ppu').click(function(){

  token_asistente = 0;

  if ($('#btn_asignar_tabla_recorrido_id').val()=='') {
    alertify.error('<center><i class="fas fa-road"></i>&nbsp;&nbsp;No definido</center>');
    return false;
  }
  if ($('#conductor_tripulacion_asignacion').val()=='') {
    alertify.error('<center><i class="fas fa-user-cog"></i>&nbsp;&nbsp;No definido</center>');
    return false;
  }

  if($('#switch_asistente_tripulacion_asignacion').css('display') != 'none'){

    token_asistente = 1;

    if ($('#asistente_tripulacion_asignacion').val()=='') {
      alertify.error('<center><i class="fas fa-users-cog"></i>&nbsp;&nbsp;No definido</center>');
      return false;
    }
  }

  document.getElementById('btn_modal_asignar_ppu').disabled = true; 
  var frm = $('#frmAsignarRecorrido').serialize();

  $.ajax({
    type: "GET",
    url: "./ajax/flotas_index.php",
    data: '&action=btn_modal_asignar_recorrido&'+frm+'&token_asistente='+token_asistente,
    beforeSend: function (objeto) {
      $('#loading_gif_modal_asignacion_ppu').html('<img src="./images/ajax-loader_control_flotas.gif" width="50" height="50"> Cargando...');
    },
    success: function (datos) {
      $("#loading_gif_modal_asignacion_ppu").html('');
      response = JSON.parse(datos);
      if (response.success) {
        insertar_evaluacion_vehiculo();
        actualizar_evaluacion_vehiculo();
        insertar_evaluacion_trabajador(token_asistente);
        actualizar_evaluacion_trabajador(token_asistente);
        alertify.success("<i class='fa fa-check'></i> " + response.message + "");
        document.getElementById("btn_modal_cancelar_asignar_ppu").click();
        $("#frmAsignarRecorrido").trigger('reset');
        document.getElementById("switch_asistente_tripulacion_asignacion").style.display = "none";

      } else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
      document.getElementById('btn_modal_asignar_ppu').disabled = false;
    }
  });

});