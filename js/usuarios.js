$(document).ready(function(){
		
//----------------------------------------------
// Agregar Usuario
	$('#btn_agregar_usuario').on('click',function(){

		if ($('#username').val()=='') {
			alertify.error('<center><i class="fa fa-user"></i>&nbsp;&nbsp;No definido</center>');
			return false;
		}

		if ($('#rut').val()=='') {
			alertify.error('<center><i class="fa fa-credit-card"></i>&nbsp;&nbsp;No definido</center>');
			return false;
		}else {
			//Validar R.U.T ------------------------------------------------
			var rut = $('#rut').val();
			    // Despejar Puntos
			    var valor = rut.replace('.','');
			    // Despejar Guión
			    valor = valor.replace('-','');
			    
			    // Aislar Cuerpo y Dígito Verificador
			    cuerpo = valor.slice(0,-1);
			    dv = valor.slice(-1).toUpperCase();
			    
			    // Formatear RUN
			    rut.value = cuerpo + '-'+ dv
			    
			    // Si no cumple con el mínimo ej. (n.nnn.nnn)
			    if(cuerpo.length < 7) { alertify.error('<center><i class="fa fa-credit-card"></i>&nbsp;&nbsp;R.U.T incompleto</center>'); return false;}
			    
			    // Calcular Dígito Verificador
			    suma = 0;
			    multiplo = 2;
			    
			    // Para cada dígito del Cuerpo
			    for(i=1;i<=cuerpo.length;i++) {
			    
			        // Obtener su Producto con el Múltiplo Correspondiente
			        index = multiplo * valor.charAt(cuerpo.length - i);
			        
			        // Sumar al Contador General
			        suma = suma + index;
			        
			        // Consolidar Múltiplo dentro del rango [2,7]
			        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
			  
			    }
			    
			    // Calcular Dígito Verificador en base al Módulo 11
			    dvEsperado = 11 - (suma % 11);
			    
			    // Casos Especiales (0 y K)
			    dv = (dv == 'K')?10:dv;
			    dv = (dv == 0)?11:dv;
			    
			    // Validar que el Cuerpo coincide con su Dígito Verificador
			    if(dvEsperado != dv) { alertify.error('<center><i class="fa fa-credit-card"></i>&nbsp;&nbsp;R.U.T inválido</center>'); return false; }
			    
			    // Si todo sale bien, eliminar errores (decretar que es válido)
			    //rut.setCustomValidity('');	
		}

		if ($('#email').val()=='') {
			alertify.error('<center><i class="fa fa-envelope"></i>&nbsp;&nbsp;No definido</center>');
			return false;
		}
		if ($('#password').val()=='') {
			alertify.error('<center><i class="fa fa-asterisk"></i>&nbsp;&nbsp;No definido</center>');
			return false;
		}
		if ($('#empresa').val()=='') {
			alertify.error('<center><i class="fa fa-briefcase"></i>&nbsp;&nbsp;No definida</center>');
			return false;
		}

      //var url = "ajax/usuarios.php?"+$('#frmUsuario').serialize();
      var url = $('#frmUsuario').serialize();

			alertify.confirm('Agregar Usuario', '¿Está seguro?.', function(){ 
				$.ajax({
					type: "GET",
					url: "./ajax/usuarios.php",
					data: url,
					beforeSend: function (objeto) {
						//$('#loader').html('<img src="./images/ajax-loader.gif"> Cargando...');
					},
					success: function (datos) {
						//$('#loader').html('');
						response = JSON.parse(datos);
						if (response.success) {
							$('#username').val("");
							$('#rut').val("");
							$('#email').val("");
							$('#password').val("");
							$('#empresa').val("");
    					//tabla_usuarios.ajax.url(url).load();
    					tabla_usuarios.ajax.reload();	
							alertify.success("<i class='fa fa-check'></i> " + response.message + "");
						} else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
					}
				}); 
			}
			, function(){ alertify.error('Cancelado')});
   });

});

//----------------------------------------------
// Eliminar Usuario
function eliminar_usuario(idusuarios) {

	alertify.confirm('Eliminar Usuario', '¿Está seguro?.', function(){ 
		$.ajax({
			type: "GET",
			url: "./ajax/usuarios.php?idusuarios="+ idusuarios,
			beforeSend: function (objeto) {
				//$('#loader').html('<img src="./images/ajax-loader.gif"> Cargando...');
			},
			success: function (datos) {
				//$('#loader').html('');
				response = JSON.parse(datos);
				if (response.success) {
    			//tabla_usuarios.ajax.url(url).load();
    			tabla_usuarios.ajax.reload();	
					alertify.success("<i class='fa fa-check'></i> " + response.message + "");
				} else alertify.error("<i class='fa fa-times'></i>  " + response.message + "");
			}
		});
	}
	, function(){ alertify.error('Cancelado')});

}
