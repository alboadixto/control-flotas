 var datepicker = "";
 $(document).ready(function(){
  
  filtro_fecha();

});

 function filtro_fecha() {
    $("#datepicker_total").text("0");
    datepicker = $("#date").val();
    tabla_dashboard.ajax.url('./dt/dashboard/php/dt_dashboard.php?datepicker='+datepicker).load();
    $.ajax({
    type: "GET",
    url: "./variables.php",
    data: '&datepicker='+datepicker+'&valor_total',
    beforeSend: function (objeto) {
    },
    success: function (datos) {
      response = JSON.parse(datos);
      if (response.success) {
        
        $("#datepicker_total").text(response.valor.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, "."));
        
        $("#date").val(response.fecha);

      }
    }
  });

 }