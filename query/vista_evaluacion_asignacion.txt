CREATE OR REPLACE VIEW 
vista_evaluacion_asignacion AS 

SELECT
	RE.idrecorrido as id_recorrido,
	RE.n_recorrido as n_recorrido,
	
	AR.id_vehiculo as id_vehiculo,
	(select patente from vehiculos where idvehiculos = AR.id_vehiculo) AS patente,

	-- % de efectividad evaluación vehiculo
	ROUND(
		(
			(
				(
				(select puntaje_unidad_limpia from UNIDAD_LIMPIA_PPU WHERE id_unidad_limpia = EV.id_unidad_limpia) +
				(select puntaje_nivel_aceite from NIVEL_ACEITE WHERE id_nivel_aceite = EV.id_nivel_aceite) +
				(select puntaje_nivel_agua from NIVEL_AGUA WHERE id_nivel_agua = EV.id_nivel_agua) +
				(select puntaje_nivel_combustible from NIVEL_COMBUSTIBLE_PPU WHERE id_nivel_combustible = EV.id_nivel_combustible) +
				(select puntaje_presion_neumatico from PRESION_NEUMATICO_PPU WHERE id_presion_neumatico = EV.id_presion_neumatico)
				) 
			* 100)/ 
			(
				(select MAX(puntaje_unidad_limpia) from UNIDAD_LIMPIA_PPU) + 
				(select MAX(puntaje_nivel_aceite) from NIVEL_ACEITE) +
				(select MAX(puntaje_nivel_agua) from NIVEL_AGUA) +
				(select MAX(puntaje_nivel_combustible) from NIVEL_COMBUSTIBLE_PPU) +
				(select MAX(puntaje_presion_neumatico) from PRESION_NEUMATICO_PPU)
			)
		)
	, 0) AS efectividad_evaluacion_vehiculo,
	
	AR.id_conductor AS id_conductor,
	(select nombre from trabajadores where id = AR.id_conductor) AS conductor,
	
	-- %evaluacion conductor ET
	ROUND(
		(
			(
				(
				(select puntaje_hora_llegada from HORA_LLEGADA WHERE id_hora_llegada = ET.id_hora_llegada) +
				IF(ET.pantalon = 1 , 1, 0) +
				IF(ET.camisa = 1 , 1, 0) +
				IF(ET.higiene = 1 , 1, 0) +
				IF(ET.casco = 1 , 1, 0) +
				IF(ET.chaleco = 1 , 1, 0) +
				IF(ET.zapatos= 1 , 1, 0) 
				) 
			* 100)/ 
			(
				(select MAX(puntaje_hora_llegada) from HORA_LLEGADA) + 6
			)
		)
	, 0) AS efectividad_evaluacion_conductor,
	
	AR.id_asistente AS id_asistente,
	(select nombre from trabajadores where id = AR.id_asistente) AS asistente,
	
	-- %evaluacion asistente ET
	ROUND(
		(
			(
				(
				(select puntaje_hora_llegada from HORA_LLEGADA WHERE id_hora_llegada = ET_asis.id_hora_llegada) +
				IF(ET_asis.pantalon = 1 , 1, 0) +
				IF(ET_asis.camisa = 1 , 1, 0) +
				IF(ET_asis.higiene = 1 , 1, 0) +
				IF(ET_asis.casco = 1 , 1, 0) +
				IF(ET_asis.chaleco = 1 , 1, 0) +
				IF(ET_asis.zapatos= 1 , 1, 0) 
				) 
			* 100)/ 
			(
				(select MAX(puntaje_hora_llegada) from HORA_LLEGADA) + 6
			)
		)
	, 0) AS efectividad_evaluacion_asistente,
	
	RE.fecha_import as fecha_import
	
FROM recorrido RE

LEFT JOIN asignacion_recorrido AR
ON RE.idrecorrido = AR.id_recorrido

LEFT JOIN evaluacion_vehiculo EV
ON RE.idrecorrido = EV.id_recorrido

LEFT JOIN evaluacion_trabajador ET
ON AR.id_conductor = ET.id_trabajador AND RE.idrecorrido = ET.id_recorrido

LEFT JOIN evaluacion_trabajador ET_asis
ON AR.id_asistente = ET_asis.id_trabajador AND RE.idrecorrido = ET_asis.id_recorrido

WHERE AR.id_vehiculo is not null

ORDER BY AR.hora desc;