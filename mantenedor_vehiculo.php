<?php

  session_start();
  include "config/config.php";

  if (!isset($_SESSION['user_id']) && $_SESSION['user_id']==null || $_SESSION['user_id']!=1) {
      header("location: config/index.php");
  }

  include "head.php";

// MANTENEDOR VEHICULO--------------------------------------
  $query_form_mantenedor_vehiculo=mysqli_query($con,"SELECT * FROM FORM_MANTENEDOR_VEHICULO WHERE ESTADO=1 ORDER BY 4");

  $class_col=' class="col-xs-12 col-sm-12 col-md-12 col-lg-4"';
  $count_separacion=3; 
  $item_vehiculo="";
  $item_accesorio="";
  $item_documento="";
  $item_compra="";
  $count_vehiculo=0;
  $count_accesorio=0;
  $count_documento=0;
  $count_compra=0;
  
   while ($row=mysqli_fetch_array($query_form_mantenedor_vehiculo)) {
   
      switch ($row["CATEGORIA"]) {
         case 1:
            $count_vehiculo++;
            $form_group_ini_vehiculo='';
            $form_group_fin_vehiculo='';
            
            if($count_vehiculo == 1){
               $form_group_ini_vehiculo='<div class="row form-group">';
            }
      
            if($count_vehiculo == $count_separacion){
               $form_group_fin_vehiculo='</div>';
               $count_vehiculo = 0;
            }
            
            $item_vehiculo.=$form_group_ini_vehiculo.' 
                                    <div'.$class_col.'>
                                       <div class="input-group">
                                          <div class="input-group-addon">
                                             <i class="'.$row["CLASS"].'">'.$row["DESCRIPCION"].'</i>
                                          </div>
                                          '.$row["INPUT"].'
                                       </div>
                                    </div'.'>'.$form_group_fin_vehiculo;
            break;
         case 2:
            $count_accesorio++;
            $form_group_ini_accesorio='';
            $form_group_fin_accesorio='';
            
            if($count_accesorio == 1){
               $form_group_ini_accesorio='<div class="row form-group">';
            }
      
            if($count_accesorio == $count_separacion){
               $form_group_fin_accesorio='</div>';
               $count_accesorio = 0;
            }
            
            $item_accesorio.=$form_group_ini_accesorio.' 
                                    <div'.$class_col.'>
                                       <div class="input-group">
                                          <div class="input-group-addon">
                                             <i class="'.$row["CLASS"].'">'.$row["DESCRIPCION"].'</i>
                                          </div>
                                          '.$row["INPUT"].'
                                       </div>
                                    </div>'.$form_group_fin_accesorio;
            break;
         case 3:
            $count_documento++;
            $form_group_ini_documento='';
            $form_group_fin_documento='';
            
            if($count_documento == 1){
               $form_group_ini_documento='<div class="row form-group">';
            }
      
            if($count_documento == $count_separacion){
               $form_group_fin_documento='</div>';
               $count_documento = 0;
            }
            
            $item_documento.=$form_group_ini_documento.' 
                                    <div'.$class_col.'>
                                       <div class="input-group">
                                          <div class="input-group-addon">
                                             <i class="'.$row["CLASS"].'">'.$row["DESCRIPCION"].'</i>
                                          </div>
                                          '.$row["INPUT"].'
                                       </div>
                                    </div>'.$form_group_fin_documento;
            break;
         case 4:
            $count_compra++;
            $form_group_ini_compra='';
            $form_group_fin_compra='';
            
            if($count_compra == 1){
               $form_group_ini_compra='<div class="row form-group">';
            }
      
            if($count_compra == $count_separacion){
               $form_group_fin_compra='</div>';
               $count_compra = 0;
            }
            
            $item_compra.=$form_group_ini_compra.' 
                                    <div'.$class_col.'>
                                       <div class="input-group">
                                          <div class="input-group-addon">
                                             <i class="'.$row["CLASS"].'">'.$row["DESCRIPCION"].'</i>
                                          </div>
                                          '.$row["INPUT"].'
                                       </div>
                                    </div>'.$form_group_fin_compra;
            break;
      }
  
   }

  if($form_group_fin_vehiculo == ''){
   $form_group_fin_vehiculo='</div>';
  } else $form_group_fin_vehiculo='';

  $item_vehiculo.=$form_group_fin_vehiculo;

  if($form_group_fin_accesorio == ''){
   $form_group_fin_accesorio='</div>';
  } else $form_group_fin_accesorio='';

  $item_accesorio.=$form_group_fin_accesorio;

  if($form_group_fin_documento == ''){
   $form_group_fin_documento='</div>';
  } else $form_group_fin_documento='';

  $item_documento.=$form_group_fin_documento;

  if($form_group_fin_compra == ''){
   $form_group_fin_compra='</div>';
  } else $form_group_fin_compra='';

  $item_compra.=$form_group_fin_compra;
// FIN MANTENEDOR VEHICULO--------------------------------------

?>

<section class="p-t-20">
  
  <!-- CONTAINER -->
  <div class="container">

   <!-- Formulario ingresar vehiculo-->
   <div id="div_ingresar_vehiculo" class="col-lg-12">
      <div class="card">
      
         <div class="card-header">
            <i class="fas fa-truck"></i>
            <strong> Ingreso Vehículo</strong>
            <span id="loading_gif_mantenedor_vehiculo" aria-hidden="true" class="pull-right"></span>
         </div>
         
         <div class="card-body card-block">
            <form id="frm_mantenedor_vehiculo" name="frm_mantenedor_vehiculo">

               <div class="custom-tab">                 
                  <nav>
                     <div class="nav nav-tabs" id="nav-tab" role="tablist">
                     
                        <a class="nav-item nav-link active show" id="nav-vehiculo-tab" data-toggle="tab" href="#nav-vehiculo" role="tab" aria-controls="nav-vehiculo" aria-selected="true">Vehículo</a>
                        
                        <a class="nav-item nav-link" id="nav-accesorio-tab" data-toggle="tab" href="#nav-accesorio" role="tab" aria-controls="nav-accesorio" aria-selected="false">Accesorio</a>
                        
                        <a class="nav-item nav-link" id="nav-documento-tab" data-toggle="tab" href="#nav-documento" role="tab" aria-controls="nav-documento" aria-selected="false">Documento</a>

                        <a class="nav-item nav-link" id="nav-compra-tab" data-toggle="tab" href="#nav-compra" role="tab" aria-controls="nav-compra" aria-selected="false">Compra</a>
                     
                     </div>
                  </nav>

                  <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                     <div class="tab-pane fade active show" id="nav-vehiculo" role="tabpanel" aria-labelledby="nav-vehiculo-tab">
                        
                        <?php echo $item_vehiculo; ?>
                     
                     </div>

                     <div class="tab-pane fade" id="nav-accesorio" role="tabpanel" aria-labelledby="nav-accesorio-tab">
                        
                        <?php echo $item_accesorio; ?>
                     
                     </div>
                     
                     <div class="tab-pane fade" id="nav-documento" role="tabpanel" aria-labelledby="nav-documento-tab">
                        
                        <?php echo $item_documento; ?>
                     
                     </div>

                     <div class="tab-pane fade" id="nav-compra" role="tabpanel" aria-labelledby="nav-compra-tab">

                        <?php echo $item_compra; ?>

                     </div>
                  
                  </div>
               </div>

            </form>
         </div>

         <div class="card-footer">
               <button type="button" id="btn_limpiar_mantenedor_vehiculo" class="btn btn-info" title="Limpiar formulario vehículo">
                  <i class="fa fa-eraser"></i>
               </button>
               <button type="button" id="btn_mantenedor_vehiculo" class="btn btn-success pull-right" title="Agregar vehículo">
                  <i class="fas fa-check"></i>
               </button>
         </div>

      </div>
   </div>
   <!-- Fin Formulario ingresar vehiculo-->

   <!-- TABLA VEHÍCULO-->
   <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> Vehículo</h3>
   <div class="row m-t-30">
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="table-responsive m-b-40">

         <?php 

            include "dt/mantenedor/view/dt_mantenedor_vehiculo.php" 

         ?>

         </div>
      </div>
   </div>
   <!-- TABLA VEHÍCULO-->

  </div>
  <!-- FIN CONTAINER -->

</section>

<?php 
  
  include "footer.php" 

?>

<script type="text/javascript" src="js/mantenedor/vehiculo.js"></script>
<script type="text/javascript" src="dt/mantenedor/dt.js"></script>

