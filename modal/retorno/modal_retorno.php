<!-- modal carga datos -->
<div class="modal fade" id="modal_retorno" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
               <h5 class="modal-title"><i class="fas fa-tasks"></i>&nbsp;&nbsp;Evaluar Retorno</h5>
               <div id="loading_gif_modal_retorno"></div>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
               </button> 
         </div>

         <div class="modal-body">
            <form id="frmRetorno" >
               
               <input type="text" class="form-control" id="id_vehiculo_retorno" name="id_vehiculo_retorno" style="display:none">

               <div class="container" >
                  <div class="alert alert-light" role="alert">
                     <h4 class="alert-heading"><i class="fas fa-truck"></i>&nbsp;&nbsp;<span id="vehiculo_retorno" name="vehiculo_retorno"></span></h4>
                     <br>

                     <div class="row form-group">

                        <div class="col col-md-2">
                           <label for="rendimiento_retorno" class=" form-control-label">Rendimiento</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-4">
                           <div class="input-group-addon">
                              <i class="fas fa-balance-scale"></i>
                           </div>
                           <input type="" id="rendimiento_retorno" name="rendimiento_retorno" class="form-control">
                        </div>

                        <div class="col col-md-2">
                           <label for="kilometraje_retorno" class=" form-control-label">Kilometraje</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-4">
                           <div class="input-group-addon">
                              <i class="fas fa-route"></i>
                           </div>
                           <input type="" id="kilometraje_retorno" name="kilometraje_retorno" class="form-control">
                        </div>
                     
                     </div>

                     <div class="row form-group">

                        <div class="col col-md-2">
                           <label for="unidad_ppu_retorno" class=" form-control-label">Unidad</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-4">
                           <div class="input-group-addon">
                              <i class="fas fa-shower"></i>
                           </div>
                           <select id="unidad_ppu_retorno" name="unidad_ppu_retorno" class="form-control">
                           </select>
                        </div>

                        <div class="col col-md-2">
                           <label for="combustible_ppu_retorno" class=" form-control-label">Combustible</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-4">
                           <div class="input-group-addon">
                              <i class="fas fa-gas-pump"></i>
                           </div>
                           <select id="combustible_ppu_retorno" name="combustible_ppu_retorno" class="form-control">
                           </select>
                        </div>

                     
                     </div>

                     <div class="row form-group">

                        <div class="col col-md-2">
                           <label for="rechazos_retorno" class=" form-control-label">Rechazos</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-4">
                           <div class="input-group-addon">
                              <i class="fas fa-thumbs-down"></i>
                           </div>
                           <input type="" id="rechazos_retorno" name="rechazos_retorno" class="form-control">
                        </div>

                        <div class="col col-md-2">
                           <label for="porcentaje_entrega_retorno" class=" form-control-label">% entrega</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-4">
                           <div class="input-group-addon">
                              <i class="fas fa-thumbs-up"></i>
                           </div>
                           <input type="number" id="porcentaje_entrega_retorno" name="porcentaje_entrega_retorno" class="form-control">
                        </div>
                     
                     </div>

                     <div class="row form-group">

                        <div class="col col-md-2">
                           <label for="dinero_retorno" class=" form-control-label">Dinero</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-4">
                           <div class="input-group-addon">
                              <i class="fas fa-dollar-sign"></i>
                           </div>
                           <input type="" id="dinero_retorno" name="dinero_retorno" class="form-control">
                        </div>

                        <div class="col col-md-4">
                           <label class=" form-control-label">¿Mercaderia sobrante?</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-2">
                           <div class="form-check-inline form-check">
                              <label for="mercaderia_retorno_si" class="form-check-label ">
                                 <input type="radio" id="mercaderia_retorno_si" name="inline-radios" value="1" class="form-check-input">Si
                              </label>
                              <label for="mercaderia_retorno_no" class="form-check-label ">
                                 <input type="radio" id="mercaderia_retorno_no" name="inline-radios" value="0" class="form-check-input">No
                              </label>
                           </div>
                        </div>
                     
                     </div>

                  </div>

                  <div class="alert alert-secondary" role="alert">
                     <h4 class="alert-heading"><i class="fas fa-search-dollar"></i>&nbsp;&nbsp;Rendición de Gastos</h4>
                     <br>

                     <div class="row form-group" align="justify">

                        <div class="col col-md-9">
                           <label for="total_dinero_retorno" class=" form-control-label"><h6>TOTAL DINERO ASIGNADO</h6></label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-3">
                           <div class="input-group-addon">
                              <i class="fas fa-dollar-sign"></i>
                           </div>
                           <input type="" id="total_dinero_retorno" name="total_dinero_retorno" class="form-control" readonly>
                        </div>
                     
                     </div>

                     <div class="row form-group" align="justify">

                        <div class="col col-md-9">
                           <label for="estacionamiento_retorno" class=" form-control-label">Estacionamiento</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-3">
                           <div class="input-group-addon">
                              <i class="fas fa-dollar-sign"></i>
                           </div>
                           <input type="" id="estacionamiento_retorno" name="estacionamiento_retorno" class="form-control">
                        </div>

                     </div>

                     <div class="row form-group" align="justify">

                        <div class="col col-md-9">
                           <label for="peaje_retorno" class=" form-control-label">Peaje</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-3">
                           <div class="input-group-addon">
                              <i class="fas fa-dollar-sign"></i>
                           </div>
                           <input type="" id="peaje_retorno" name="peaje_retorno" class="form-control">
                        </div>

                     </div>

                     <div class="row form-group" align="justify">

                        <div class="col col-md-9">
                           <label for="otros_retorno" class=" form-control-label">Otros</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-3">
                           <div class="input-group-addon">
                              <i class="fas fa-dollar-sign"></i>
                           </div>
                           <input type="" id="otros_retorno" name="otros_retorno" class="form-control">
                        </div>

                     </div>

                     <div class="row form-group" align="justify">

                        <div class="col col-md-9">
                           <label for="diferencia_dinero_retorno" class=" form-control-label">Diferencia</label>
                        </div>
                        <div class="input-group col-12 col-md-12 col-lg-3">
                           <div class="input-group-addon">
                              <i class="fas fa-dollar-sign"></i>
                           </div>
                           <input type="" id="diferencia_dinero_retorno" name="diferencia_dinero_retorno" class="form-control" readonly>
                        </div>

                     </div>

                  </div>

               </div>

            </form>
         </div>

         <div class="modal-footer">
            <button id="btn_modal_retorno" type="button" class="btn btn-success"><i class="fas fa-save"></i> Guardar
            </button>
            <button id="btn_modal_cancelar_retorno" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fas fa-times"></i> Cancelar
            </button> 
         </div>

      </div>
   </div>
</div>
<!-- end modal retorno -->