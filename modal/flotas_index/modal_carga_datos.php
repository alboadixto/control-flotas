<!-- modal carga datos -->
<div class="modal fade" id="modal_carga_datos" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
               <h5 class="modal-title"><i class="fas fa-file-import"></i>&nbsp;&nbsp;Planilla de Carga</h5>
               <h5 class="modal-title"><i class="far fa-calendar-alt"></i>&nbsp;&nbsp;<?php echo date("d-m-Y");?></h5>  
         </div>

         <div class="modal-body">

            <div id="filer" class="row">
            <div class="col-md-6 offset-md-3 mr-auto ml-auto">
               <div class="input-group">
                  <input type="file" name="files[]" id="filer_input" accept=".csv">
               </div>
            </div>
            </div>

            <!-- TABLA CARGA DATOS-->
            <div class="container" >
               <center><div id="loading_gif"></div></center>
               <div id="datos_cargados" class="row">
                  <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> Datos Importados</h3>
                  <div class="table-responsive m-b-40">
                  
                  <?php   

                     include "dt/index_flotas/view/dt_carga_datos.php";
                  
                  ?>

                  </div>
               </div>
            </div>
            <!-- TABLA CARGA DATOS-->

         </div>

         <div class="modal-footer">
            <button id="btn_cerrar_modal" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fas fa-times"></i> Cerrar</button>
         </div>
      </div>
   </div>
</div>
<!-- end modal carga datos -->