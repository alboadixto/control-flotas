   <!-- MODAL EVA VEHICULO ASIGNACIONES -->
   <div class="modal fade" id="modal_asignaciones_eva_vehiculo" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">

            <div class="modal-header">
               <h5 class="modal-title"><i class="fas fa-tasks"></i>&nbsp;&nbsp;Evaluación PPU: <span class="badge badge-pill badge-secondary" id="eva_vehiculo_ppu" name="eva_vehiculo_ppu"></span></h5>
               <div id="loading_gif_modal_eva_vehiculo"></div>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
               </button> 
            </div>

            <div class="modal-body">
               <form id="frmEvaVehiculo" >
               
                  <input type="text" class="form-control" id="eva_vehiculo_id_recorrido" name="eva_vehiculo_id_recorrido" style="display:none">
                  
                  <input type="text" class="form-control" id="eva_vehiculo_id_ppu" name="eva_vehiculo_id_ppu" style="display:none">

                  <div class="container" >
                     <div class="alert alert-light" role="alert">
                        <h4 class="alert-heading">RECORRIDO: <span id="eva_vehiculo_recorrido" name="eva_vehiculo_recorrido"></span></h4>
                        <br>

                        <div class="row form-group">

                           <div class="col col-md-2">
                              <label for="unidad_ppu_asignacion" class=" form-control-label">Unidad</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-4">
                              <div class="input-group-addon">
                                 <i class="fas fa-shower"></i>
                              </div>
                              <select id="unidad_ppu_asignacion" name="unidad_ppu_asignacion" class="form-control">
                              </select>
                           </div>

                           <div class="col col-md-2">
                              <label for="presion_ppu_asignacion" class=" form-control-label">Neumático</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-4">
                              <div class="input-group-addon">
                                 <i class="fas fa-truck-monster"></i>
                              </div>
                              <select id="presion_ppu_asignacion" name="presion_ppu_asignacion" class="form-control">
                              </select>
                           </div>
                        
                        </div>

                        <div class="row form-group">

                           <div class="col col-md-2">
                              <label for="agua_ppu_asignacion" class=" form-control-label">Agua</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-4">
                              <div class="input-group-addon">
                                 <i class="fas fa-water"></i>
                              </div>
                              <select id="agua_ppu_asignacion" name="agua_ppu_asignacion" class="form-control">
                              </select>
                           </div>

                           <div class="col col-md-2">
                              <label for="aceite_ppu_asignacion" class=" form-control-label">Aceite</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-4">
                              <div class="input-group-addon">
                                 <i class="fas fa-oil-can"></i>
                              </div>
                              <select id="aceite_ppu_asignacion" name="aceite_ppu_asignacion" class="form-control">
                              </select>
                           </div>

                        </div>
                           
                        <div class="row form-group">

                           <div class="col col-md-2">
                              <label for="combustible_ppu_asignacion" class=" form-control-label">Combustible</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-4">
                              <div class="input-group-addon">
                                 <i class="fas fa-gas-pump"></i>
                              </div>
                              <select id="combustible_ppu_asignacion" name="combustible_ppu_asignacion" class="form-control">
                              </select>
                           </div>
                           <div class="col col-md-2">
                              <label for="kilometraje_ppu_asignacion" class=" form-control-label">Kilometraje</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-4">
                              <div class="input-group-addon">
                                 <i class="fas fa-route"></i>
                              </div>
                              <input type="" id="kilometraje_ppu_asignacion" name="kilometraje_ppu_asignacion" class="form-control">
                           </div>
                        
                        </div>

                        <div class="row form-group">

                           <div class="col col-md-2">
                              <label for="dinero_ppu_asignacion" class=" form-control-label">Dinero</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-4">
                              <div class="input-group-addon">
                                 <i class="fas fa-dollar-sign"></i>
                              </div>
                              <input type="" id="dinero_ppu_asignacion" name="dinero_ppu_asignacion" class="form-control">
                           </div>
                           <div class="col col-md-2">
                              <label for="litros_cargados_ppu_asignacion" class=" form-control-label">Litros</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-4">
                              <div class="input-group-addon">
                                 <i class="fas fa-gas-pump"></i>
                              </div>
                              <input type="" id="litros_cargados_ppu_asignacion" name="litros_cargados_ppu_asignacion" class="form-control">
                           </div>
                        
                        </div>

                     </div>

                  </div>

               </form>
            </div>

            <div class="modal-footer">
                  <button id="btn_modal_eva_vehiculo" type="button" class="btn btn-success"><i class="fas fa-save"></i> Guardar
                  </button>
                  <button id="btn_modal_cancelar_eva_vehiculo" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fas fa-times"></i> Cancelar
                  </button> 
            </div>

         </div>
      </div>
   </div>
   <!-- END MODAL EVA VEHICULO ASIGNACIONES -->

      <!-- MODAL EVA TRIPULACIÓN ASIGNACIONES -->
   <div class="modal fade" id="modal_asignaciones_eva_tripulacion" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">

            <div class="modal-header">
               <h5 class="modal-title"><i class="fas fa-tasks"></i>&nbsp;&nbsp;Evaluación Tripulación: <span class="badge badge-pill badge-secondary" id="eva_tripulacion" name="eva_tripulacion"></span></h5>
               <div id="loading_gif_modal_eva_tripulacion"></div>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
               </button> 
            </div>

            <div class="modal-body">
               <form id="frmEvaTripulacion" >
               
                  <input type="text" class="form-control" id="eva_tripulacion_id_recorrido" name="eva_tripulacion_id_recorrido" style="display:none">

                  <input type="text" class="form-control" id="eva_tripulacion_id_tripulacion" name="eva_tripulacion_id_tripulacion" style="display:none">

                  <div class="container" >
                     <div class="alert alert-light" role="alert">
                        <h4 class="alert-heading">RECORRIDO: <span id="eva_tripulacion_recorrido" name="eva_tripulacion_recorrido"></span></h4>
                        <br>

                        <div class="row form-group">

                           <div class="col col-md-2">
                              <label class=" form-control-label">Ropa</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-10">
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="checkbox" id="pantalon_tripulacion_asignacion" name="pantalon_tripulacion_asignacion">
                                 <label class="form-check-label" for="pantalon_tripulacion_asignacion">Pantalón</label>
                              </div>
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="checkbox" id="camisa_tripulacion_asignacion" name="camisa_tripulacion_asignacion">
                                 <label class="form-check-label" for="camisa_tripulacion_asignacion">Camisa</label>
                              </div>
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="checkbox" id="higiene_tripulacion_asignacion" name="higiene_tripulacion_asignacion">
                                 <label class="form-check-label" for="higiene_tripulacion_asignacion">Higiene</label>
                              </div>
                           </div>
                        
                        </div>

                        <div class="row form-group">

                           <div class="col col-md-2">
                              <label class=" form-control-label">Seguridad</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-10">
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="checkbox" id="casco_tripulacion_asignacion" name="casco_tripulacion_asignacion">
                                 <label class="form-check-label" for="casco_tripulacion_asignacion">Casco</label>
                              </div>
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="checkbox" id="chaleco_tripulacion_asignacion" name="chaleco_tripulacion_asignacion">
                                 <label class="form-check-label" for="chaleco_tripulacion_asignacion">Chaleco</label>
                              </div>
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="checkbox" id="zapatos_tripulacion_asignacion" name="zapatos_tripulacion_asignacion">
                                 <label class="form-check-label" for="zapatos_tripulacion_asignacion">Zapatos</label>
                              </div>
                           </div>

                        </div>
                           
                        <div class="row form-group">

                           <div class="col col-md-2">
                              <label for="hora_tripulacion_asignacion" class=" form-control-label">Llegada</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-6">
                              <div class="input-group-addon">
                                 <i class="fas fa-clock"></i>
                              </div>
                              <select id="hora_tripulacion_asignacion" name="hora_tripulacion_asignacion" class="form-control">
                              </select>
                           </div>
                        
                        </div>

                     </div>

                  </div>

               </form>
            </div>

            <div class="modal-footer">
                  <button id="btn_modal_eva_tripulacion" type="button" class="btn btn-success"><i class="fas fa-save"></i> Guardar
                  </button>
                  <button id="btn_modal_cancelar_eva_tripulacion" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fas fa-times"></i> Cancelar
                  </button> 
            </div>

         </div>
      </div>
   </div>
   <!-- END MODAL EVA TRIPULACIÓN ASIGNACIONES -->