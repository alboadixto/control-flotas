   <!-- MODAL ASIGNACIONES -->
   <div class="modal fade" id="modal_asignacion_recorrido" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                     <h5 class="modal-title"><i class="fas fa-cart-plus"></i>&nbsp;&nbsp;Asignar a recorrido</h5>
                     <div id="loading_gif_modal_asignaciones"></div>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                     </button> 
               </div>

               <div class="modal-body">
                     <form id="frmAsignarRecorrido" >
                     
                     <input type="text" class="form-control" id="btn_asignar_tabla_recorrido_id" name="btn_asignar_tabla_recorrido_id" style="display:none">

                        <div class="container" >
                           <div class="alert alert-light" role="alert">
                                 <h4 class="alert-heading">PPU</h4>
                                 <br>

                                 <div class="row form-group">

                                    <div class="input-group col-12 col-md-12 col-lg-6">
                                       <div class="input-group-addon">
                                             <i class="fas fa-truck"></i>
                                       </div>
                                       <select id="ppu_asignacion" name="ppu_asignacion" class="form-control" onchange="obtener_ppu_asignacion()">
                                       </select>
                                    </div>

                                    <div class="input-group col-12 col-md-12 col-lg-6">
                                       <div class="input-group-addon">
                                             <i class="fas fa-thumbs-up"></i>
                                       </div>
                                       <input type="text" id="estado_ppu_asignacion" name="estado_ppu_asignacion" class="form-control" readonly>
                                    </div>
                                 </div>

                                 <div class="row form-group">

                                    <div class="input-group col-12 col-md-12 col-lg-12">
                                       <div class="input-group-addon">
                                             <i class="fas fa-pen"></i>
                                       </div>
                                       <input type="text" id="descripcion_ppu_asignacion" name="descripcion_ppu_asignacion" class="form-control" readonly>
                                    </div>

                                 </div>

                           </div>

                           <div class="alert alert-light" role="alert">
                                 <h4 class="alert-heading">TRIPULACIÓN</h4>
                                 <br>

                                 <div class="row form-group">

                                    <div class="input-group col-12 col-md-12 col-lg-8">
                                       <div class="input-group-addon">
                                             <i class="fas fa-user-cog"></i>
                                       </div>
                                       <select id="conductor_tripulacion_asignacion" name="conductor_tripulacion_asignacion" class="form-control" onchange="obtener_conductor_asistente_asignacion('conductor_tripulacion_asignacion','rut_conductor_tripulacion_asignacion')">
                                       </select>
                                    </div>

                                    <div class="input-group col-12 col-md-12 col-lg-4">
                                       <div class="input-group-addon">
                                             <i class="fas fa-id-card"></i>
                                       </div>
                                       <input type="text" id="rut_conductor_tripulacion_asignacion" name="rut_conductor_tripulacion_asignacion" class="form-control" readonly>
                                    </div>
                                    
                                 </div>

                                 <div class="row form-group">

                                    <div class="col col-md-2">
                                       <label for="switch_asistente_asignacion" class=" form-control-label">¿Asistente?</label>
                                    </div>
                                    <div class="input-group col-12 col-md-12 col-lg-10">   
                                       <label class="switch switch-text switch-success switch-pill">
                                             <input id="switch_asistente_asignacion" type="checkbox" class="switch-input">
                                             <span data-on="Si" data-off="No" class="switch-label"></span>
                                             <span class="switch-handle"></span>
                                       </label>
                                    </div>

                                 </div>

                                 <div id="switch_asistente_tripulacion_asignacion" style="display:none">
                                    <div class="row form-group">

                                       <div class="input-group col-12 col-md-12 col-lg-8">
                                             <div class="input-group-addon">
                                                <i class="fas fa-users-cog"></i>
                                             </div>
                                             <select id="asistente_tripulacion_asignacion" name="asistente_tripulacion_asignacion" class="form-control" onchange="obtener_conductor_asistente_asignacion('asistente_tripulacion_asignacion','rut_asistente_tripulacion_asignacion')">
                                             </select>
                                       </div>

                                       <div class="input-group col-12 col-md-12 col-lg-4">
                                             <div class="input-group-addon">
                                                <i class="fas fa-id-card"></i>
                                             </div>
                                             <input type="text" id="rut_asistente_tripulacion_asignacion" name="rut_asistente_tripulacion_asignacion" class="form-control" readonly>
                                       </div>

                                    </div>
                                 </div>

                           </div>

                        </div>

                     </form>
               </div>

               <div class="modal-footer">
                     <button id="btn_modal_asignar_recorrido" type="button" class="btn btn-success"><i class="fas fa-check"></i> Asignar
                     </button>
                     <button id="btn_modal_cancelar_recorrido" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fas fa-times"></i> Cancelar
                     </button> 
               </div>

            </div>
         </div>
   </div>
   <!-- END MODAL ASIGNACIONES -->