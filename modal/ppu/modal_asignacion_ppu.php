   <!-- MODAL ASIGNACION PPU -->
   <div class="modal fade" id="modal_asignacion_ppu" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">

            <div class="modal-header">
            <h5 class="modal-title"><i class="fas fa-cart-plus"></i>&nbsp;&nbsp;Asignar recorrido a PPU</h5>
               <div id="loading_gif_modal_asignacion_ppu"></div>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
               </button> 
            </div>

            <div class="modal-body">
               <form id="frmAsignarRecorrido" >
               
                  <input type="text" class="form-control" id="ppu_asignacion" name="ppu_asignacion" style="display:none">

                  <div class="container" >

                     <div class="alert alert-light" role="alert">
                        <h4 class="alert-heading"><i class="fas fa-truck"></i>&nbsp;&nbsp;<span id="asignacion_ppu" name="asignacion_ppu"></span></h4>
                        <br>

                        <div class="row form-group">

                           <div class="input-group col-12 col-md-12 col-lg-6">
                              <div class="input-group-addon">
                                 <i class="fas fa-road"></i>
                              </div>
                              <select id="btn_asignar_tabla_recorrido_id" name="btn_asignar_tabla_recorrido_id" class="form-control" onchange="obtener_recorrido()">
                              </select>
                           </div>
                           
                        </div>

                        <div class="row form-group">

                           <div class="table-responsive table-responsive-data2">
                              <?php

                                 include "dt/ppu/view/dt_select_recorrido.php"

                              ?>
                           </div>

                        </div>

                     </div>

                     <div class="alert alert-light" role="alert">
                        <h4 class="alert-heading">TRIPULACIÓN</h4>
                        <br>

                        <div class="row form-group">

                           <div class="input-group col-12 col-md-12 col-lg-8">
                              <div class="input-group-addon">
                                    <i class="fas fa-user-cog"></i>
                              </div>
                              <select id="conductor_tripulacion_asignacion" name="conductor_tripulacion_asignacion" class="form-control" onchange="obtener_conductor_asistente_asignacion('conductor_tripulacion_asignacion','rut_conductor_tripulacion_asignacion')">
                              </select>
                           </div>

                           <div class="input-group col-12 col-md-12 col-lg-4">
                              <div class="input-group-addon">
                                    <i class="fas fa-id-card"></i>
                              </div>
                              <input type="text" id="rut_conductor_tripulacion_asignacion" name="rut_conductor_tripulacion_asignacion" class="form-control" readonly>
                           </div>
                           
                        </div>

                        <div class="row form-group">

                           <div class="col col-md-2">
                              <label for="switch_asistente_asignacion" class=" form-control-label">¿Asistente?</label>
                           </div>
                           <div class="input-group col-12 col-md-12 col-lg-10">   
                              <label class="switch switch-text switch-success switch-pill">
                                 <input id="switch_asistente_asignacion" type="checkbox" class="switch-input">
                                 <span data-on="Si" data-off="No" class="switch-label"></span>
                                 <span class="switch-handle"></span>
                              </label>
                           </div>

                        </div>

                        <div id="switch_asistente_tripulacion_asignacion" style="display:none">
                           <div class="row form-group">

                              <div class="input-group col-12 col-md-12 col-lg-8">
                                 <div class="input-group-addon">
                                    <i class="fas fa-users-cog"></i>
                                 </div>
                                 <select id="asistente_tripulacion_asignacion" name="asistente_tripulacion_asignacion" class="form-control" onchange="obtener_conductor_asistente_asignacion('asistente_tripulacion_asignacion','rut_asistente_tripulacion_asignacion')">
                                 </select>
                              </div>

                              <div class="input-group col-12 col-md-12 col-lg-4">
                                 <div class="input-group-addon">
                                    <i class="fas fa-id-card"></i>
                                 </div>
                                 <input type="text" id="rut_asistente_tripulacion_asignacion" name="rut_asistente_tripulacion_asignacion" class="form-control" readonly>
                              </div>

                           </div>
                        </div>

                     </div>

                     <div class="alert alert-secondary" role="alert">
                        <h4 class="alert-heading"><i class="fas fa-table"></i> Recorridos asignados</h4>
                        <br>

                        <div class="table-responsive m-b-40">
                           <?php

                              include "dt/ppu/view/dt_asignaciones.php"

                           ?>
                        </div>

                     </div>

                  </div>

               </form>
            </div>

            <div class="modal-footer">
               <button id="btn_modal_asignar_ppu" type="button" class="btn btn-success"><i class="fas fa-save"></i> Guardar
               </button>
               <button id="btn_modal_cancelar_asignar_ppu" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fas fa-times"></i> Cancelar
               </button> 
            </div>

         </div>
      </div>
   </div>
   <!-- END MODAL ASIGNACION PPU -->