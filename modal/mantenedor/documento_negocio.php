<!-- MODAL DOC NEGOCIO -->
<div class="modal fade" id="modal_documento_negocio" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title"><i class="fas fa-cart-plus"></i>&nbsp;&nbsp;Asignar documentos a negocio</h5>
                  <div id="loading_gif_modal_documento_negocio"></div>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                  </button> 
               </div>

               <div class="modal-body">

              <!-- SUBIR DOCUMENTO -->
                  <!-- <div id="filer" class="row">
                     <div class="col-md-6 offset-md-3 mr-auto ml-auto">
                        <div class="input-group">
                           <input type="file" name="files[]" id="filer_input">
                        </div>
                     </div>
                     </div> -->
                  <div class="col-md-6 offset-md-3 mr-auto ml-auto">
                     <div class="input-group">
                     
                     <form id="frm_documento_negocio">
                        <input type="text" class="form-control" id="input_id_negocio" name="input_id_negocio" style="display:none">
                        <input type="file" name="archivo" id="archivo" class="form-control"/>   
                     </form>
                     <br><br>
                     <button id="btn_documento_negocio" name="btn_documento_negocio" type="button" class="btn btn-success btn-block" title="Subir documento"><i class="fa fa-upload"></i>
                     </button> 

                     </div>
                  </div>
              <!-- FIN SUBIR DOCUMENTO -->

                <!-- TABLA DOCUMENTO NEGOCIO -->
                <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> DOCUMENTOS</h3>
                  <div class="row m-t-30">
                     <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="table-responsive m-b-40">

                        <?php 

                           include "dt/mantenedor/view/dt_documento_negocio.php" 

                        ?>

                        </div>
                     </div>
                  </div>
                  <!-- TABLA DOCUMENTO NEGOCIO -->
               </div>

               <div class="modal-footer">
                     <button id="btn_modal_cancelar_documento_negocio" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fas fa-times"></i>
                     </button> 
               </div>

            </div>
         </div>
   </div>
<!-- END MODAL DOC NEGOCIO -->