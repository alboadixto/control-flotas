<!-- MODAL ASIGNACIONES -->
   <div class="modal fade" id="modal_asignacion_ppu_negocio" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                     <h5 class="modal-title"><i class="fas fa-cart-plus"></i>&nbsp;&nbsp;Asignar a negocio</h5>
                     <div id="loading_gif_modal_asignacion_ppu"></div>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                     </button> 
               </div>

               <div class="modal-body">
                <!-- TABLA PPU NO ASIGNACIÓN -->
                <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> PPU DISPONIBLES</h3>
                  <div class="row m-t-30">
                     <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="table-responsive m-b-40">

                        <?php 

                           include "dt/mantenedor/view/dt_asignacion_no_ppu_negocio.php" 

                        ?>

                        </div>
                     </div>
                  </div>
                  <!-- TABLA PPU NO ASIGNACIÓN -->

                  <!-- TABLA PPU ASIGNACIÓN -->
                  <h3 class="title-5 m-b-35"><i class="fas fa-table"></i> PPU ASIGNADAS</h3>
                  <div class="row m-t-30">
                     <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="table-responsive m-b-40">

                        <?php 

                           include "dt/mantenedor/view/dt_asignacion_ppu_negocio.php" 

                        ?>

                        </div>
                     </div>
                  </div>
                  <!-- TABLA PPU ASIGNACIÓN -->
               </div>

               <div class="modal-footer">
                     <button id="btn_modal_cancelar_asignacion_ppu_negocio" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fas fa-times"></i>
                     </button> 
               </div>

            </div>
         </div>
   </div>
   <!-- END MODAL ASIGNACIONES -->